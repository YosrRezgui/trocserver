package Admin;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import com.alee.laf.WebLookAndFeel;

import delegate.AdminDelegate;
import domain.Administrator;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.SystemColor;
import javax.swing.ImageIcon;

public class Authentification extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtLogin;
	private JPasswordField txtPassword;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					Authentification frame = new Authentification();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Authentification() {
		setTitle("Troc-Tn Administration");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 477, 231);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.text);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setBounds(167, 51, 46, 14);
		contentPane.add(lblLogin);
		
		JLabel Password = new JLabel("Password");
		Password.setBounds(163, 103, 89, 14);
		contentPane.add(Password);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(244, 48, 176, 20);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		JButton txtSign = new JButton("  Sign In");
		txtSign.setIcon(new ImageIcon(Authentification.class.getResource("/Ressources/ImgFromMaster/Register-icon.png")));
		txtSign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Administrator admin = AdminDelegate.authentification(
						txtLogin.getText(), txtPassword.getText());
				String message = "Successful Authentication";
				String message1 = "Authentication Failed";
				if (admin!= null) {
					JOptionPane.showMessageDialog(new JFrame(), message,
							"Alert", JOptionPane.INFORMATION_MESSAGE);	
					
					MenuAdmin frame = new MenuAdmin();
					frame.setVisible(true);
				}
				else{ 
					JOptionPane.showMessageDialog(new JFrame(), message1,
							"Alerte", JOptionPane.ERROR_MESSAGE);	
				}
				
			}
		});
		txtSign.setBounds(302, 145, 118, 36);
		contentPane.add(txtSign);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(249, 100, 171, 20);
		contentPane.add(txtPassword);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\osa_user_blue_sysadmin.png"));
		lblNewLabel.setBounds(10, 26, 147, 129);
		contentPane.add(lblNewLabel);
	}
}
