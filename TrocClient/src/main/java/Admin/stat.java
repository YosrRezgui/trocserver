/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Admin;


import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import delegate.AdminDelegate;


/**
 *
 * @author 
 */
public class stat {
int NbrExchangeValide ;//-Nombre d'echanges valides
int NbrExchangeNonValide ;//-Nombre d'echanges non valides
int NbrExchangeSwapper1NonValide; 
int nbrSwapperValide; 
int nbrSwapperNonValide;


//------------Swapper----------------------------------------------------------------------------------------------------------------------------
    public DefaultPieDataset SwapperPie() {
        /**/
        DefaultPieDataset data = new DefaultPieDataset();
        
         
        /*---Recuperation du  Nombre des swapper confirmés & non confirmés dans la  base---*/
        nbrSwapperValide = AdminDelegate.nbrSwapperConfirme().size();
        nbrSwapperNonValide = AdminDelegate.nbrSwapperNonConfirme().size();     //----------------------------------------------------
        data.setValue("Number of Valide Swapper", nbrSwapperValide);
        data.setValue("Number of Invalid Swapper", nbrSwapperNonValide);
        //----------------------------------------------------
        return data;

    }
    
      public DefaultCategoryDataset SwapperBar() {
      
        /*---Recuperation du  Nombre des swapper confirmés & non confirmés dans la  base---*/
    	  nbrSwapperValide = AdminDelegate.nbrSwapperConfirme().size();
          nbrSwapperNonValide = AdminDelegate.nbrSwapperNonConfirme().size();  
        /**/
        DefaultCategoryDataset datasetBar = new DefaultCategoryDataset();
        datasetBar.setValue(nbrSwapperValide, "Valide Swapper", "Valide Swapper");
        datasetBar.setValue(nbrSwapperNonValide, "Invalide Swapper", "Invalide Swapper");
        return datasetBar;

    }
//------------------------------------------------------------------------------
   
    //---------------------Exchange-------------------------------------------------------------------------------------------------------------------
      public DefaultPieDataset ExchangePie() {
          /**/
          DefaultPieDataset data = new DefaultPieDataset();
          
           
          /*---Recuperation du  Nombre des Exchage confirmés & non confirmés dans la  base---*/
          NbrExchangeValide = AdminDelegate.NbrExchangeValide().size();
          NbrExchangeNonValide = AdminDelegate.NbrExchangeNonValide().size();
          NbrExchangeSwapper1NonValide = AdminDelegate.NbrExchangeSwapper1NonValide().size(); //----------------------------------------------------
          data.setValue("Number of Valide Exchage ", NbrExchangeValide);
          data.setValue("Number of Invalid Exchage", NbrExchangeNonValide);
          data.setValue("Number of Invalid Exchage with swapper not validate", NbrExchangeSwapper1NonValide);
          //----------------------------------------------------
          return data;

      }
      
        public DefaultCategoryDataset ExchangeBar() {
        
          /*---Recuperation du  Nombre des swapper confirmés & non confirmés dans la  base---*/
        	NbrExchangeValide = AdminDelegate.NbrExchangeValide().size();
            NbrExchangeNonValide = AdminDelegate.NbrExchangeNonValide().size();
            NbrExchangeSwapper1NonValide = AdminDelegate.NbrExchangeSwapper1NonValide().size();  
          /**/
          DefaultCategoryDataset datasetBar = new DefaultCategoryDataset();
          datasetBar.setValue(NbrExchangeValide, "Number of Valide Exchage", "Number of Valide Exchage");
          datasetBar.setValue(NbrExchangeNonValide, "Number of Invalid Exchage", "Number of Invalid Exchage");
          datasetBar.setValue(NbrExchangeSwapper1NonValide, "Number of Invalid Exchage with swapper not validate", "Number of Invalid Exchage with swapper not validate");
          return datasetBar;

      }
  //------------------------------------------------------------------------------
    
      
      
}
