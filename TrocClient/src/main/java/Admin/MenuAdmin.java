package Admin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuAdmin extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuAdmin frame = new MenuAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuAdmin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Statistics");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				statistiques frame = new statistiques();
				frame.setVisible(true);
			}
		});
		btnNewButton.setBounds(20, 104, 155, 51);
		contentPane.add(btnNewButton);
		
		JButton btnAccountManagement = new JButton("Account Management");
		btnAccountManagement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AccountManagement frame = new AccountManagement();
				//frame.setVisible(true);
			}
		});
		btnAccountManagement.setBounds(175, 104, 155, 51);
		contentPane.add(btnAccountManagement);
	}

}
