package Admin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JButton;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import com.alee.laf.WebLookAndFeel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;

import java.awt.Frame;

import javax.swing.JToolBar;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import javax.swing.SwingConstants;

public class statistiques extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					statistiques frame = new statistiques();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public statistiques() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 746, 483);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JPanel jPanel = new JPanel();
		jPanel.setBackground(SystemColor.controlHighlight);
		jPanel.setBounds(434, 190, 564, 311);
		contentPane.add(jPanel);
		jPanel.setLayout(null);
		
		final JComboBox jtype = new JComboBox();
		jtype.setModel(new DefaultComboBoxModel(new String[] {"Pie", "Bar"}));
		jtype.setBounds(823, 155, 112, 20);
		contentPane.add(jtype);
		
		JLabel lblRepere = new JLabel("Repere");
		lblRepere.setForeground(new Color(255, 255, 255));
		lblRepere.setBounds(143, 117, 46, 14);
		contentPane.add(lblRepere);
		
		JButton jButton1 = new JButton("");
		jButton1.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/Groups-Meeting-Dark-icon.png")));
		jButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stat stat = new stat();
				  switch (jtype.getSelectedIndex()) {
		            case 0:
		                DefaultPieDataset pieDataset = new DefaultPieDataset(); //Dataset qui va contenir les Donn�es 
		                //remplissage du  dataset du pie
		                pieDataset = stat.SwapperPie();
		                System.out.println("" + pieDataset);
		                // Creation du fromage 
		                JFreeChart chartPie = ChartFactory.createPieChart3D("SwapperConfirm/SwapperNoConfirm", pieDataset, true, true, true);            //Graphe
		                //saveChart(chartPie);
		                //affichage
		                ChartPanel frame = new ChartPanel(chartPie);
		                jPanel.add(frame);
		                frame.setSize(508, 232);
		                // TODO add your handling code here:
		                break;
		               
		                     case 1:
		                // Create a simple Bar chart       
		                DefaultCategoryDataset datasetBar = new DefaultCategoryDataset();
		                //remplissage du  dataset du pie
		                datasetBar = stat.SwapperBar();
		                System.out.println("" + datasetBar.toString());
		                JFreeChart chartBar = ChartFactory.createBarChart3D("SwapperConfirm/SwapperNoConfirm", "SwapperConfirm/SwapperNoConfirm", "Number Of SwapperConfirm and SwapperNoConfirm", datasetBar, PlotOrientation.VERTICAL, true, true, true);
		                //saveChart(chartBar);
		                //affichage
		                ChartPanel frameBar = new ChartPanel(chartBar);
		                jPanel.add(frameBar);
		                frameBar.setSize(508, 232);
		                
		                break;
		       }
				
			}
		});
		jButton1.setBounds(164, 289, 152, 146);
		contentPane.add(jButton1);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/peer-to-peer-icon (1).png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				stat stat = new stat();
				  switch (jtype.getSelectedIndex()) {
		            case 0:
		                DefaultPieDataset pieDataset = new DefaultPieDataset(); //Dataset qui va contenir les Donn�es 
		                //remplissage du  dataset du pie
		                pieDataset = stat.ExchangePie();
		                System.out.println("" + pieDataset);
		                // Creation du fromage 
		                JFreeChart chartPie = ChartFactory.createPieChart3D("ExchangrConfirm/ExchangrNoConfirm", pieDataset, true, true, true);            //Graphe
		                //saveChart(chartPie);
		                //affichage
		                ChartPanel frame = new ChartPanel(chartPie);
		                jPanel.add(frame);
		                frame.setSize(508, 232);
		                // TODO add your handling code here:
		                break;
		               
		                     case 1:
		                // Create a simple Bar chart       
		                DefaultCategoryDataset datasetBar = new DefaultCategoryDataset();
		                //remplissage du  dataset du pie
		                datasetBar = stat.ExchangeBar();
		                System.out.println("" + datasetBar.toString());
		                JFreeChart chartBar = ChartFactory.createBarChart3D("ExchangrConfirm/ExchangrNoConfirm", "ExchangrConfirm/ExchangrNoConfirm", "Number Of ExchangeConfirm and ExchangeNoConfirm", datasetBar, PlotOrientation.VERTICAL, true, true, true);
		                //saveChart(chartBar);
		                //affichage
		                ChartPanel frameBar = new ChartPanel(chartBar);
		                jPanel.add(frameBar);
		                frameBar.setSize(508, 232);
		                
		                break;
		       }
				
			}
		});
		btnNewButton.setBounds(1043, 289, 144, 136);
		contentPane.add(btnNewButton);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(SystemColor.menu);
		panel.setBounds(0, 609, 1354, 96);
		contentPane.add(panel);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setBounds(626, 11, 79, 74);
		panel.add(button);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		button_1.setBounds(750, 26, 70, 59);
		panel.add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		button_2.setBounds(870, 26, 70, 59);
		panel.add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		button_3.setBounds(991, 26, 70, 59);
		panel.add(button_3);
		
		JButton button_4 = new JButton("");
		button_4.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		button_4.setBackground(Color.WHITE);
		button_4.setBounds(1120, 26, 70, 59);
		panel.add(button_4);
		
		JButton button_5 = new JButton("");
		button_5.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\right208.png"));
		button_5.setBounds(1257, 33, 62, 52);
		panel.add(button_5);
		
		JButton button_6 = new JButton("");
		button_6.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		button_6.setBounds(497, 26, 70, 59);
		panel.add(button_6);
		
		JButton button_7 = new JButton("");
		button_7.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		button_7.setBounds(372, 26, 70, 59);
		panel.add(button_7);
		
		JButton button_8 = new JButton("");
		button_8.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		button_8.setBounds(257, 26, 70, 59);
		panel.add(button_8);
		
		JButton button_9 = new JButton("");
		button_9.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		button_9.setBounds(118, 26, 70, 59);
		panel.add(button_9);
		
		JButton button_10 = new JButton("");
		button_10.setIcon(new ImageIcon(statistiques.class.getResource("/Ressources/ImgFromMaster/left224.png")));
		button_10.setBorder(null);
		button_10.setBounds(10, 33, 62, 52);
		panel.add(button_10);
		
		JLabel lblStatistic = new JLabel("Statistics");
		lblStatistic.setHorizontalTextPosition(SwingConstants.CENTER);
		lblStatistic.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatistic.setFont(new Font("Tahoma", Font.BOLD, 58));
		lblStatistic.setBounds(391, 11, 548, 96);
		contentPane.add(lblStatistic);
		
		JLabel label_1 = new JLabel("Rafik Riahi");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		label_1.setBounds(1180, 43, 78, 14);
		contentPane.add(label_1);
		
		JButton button_11 = new JButton("");
		button_11.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\repair14.png"));
		button_11.setBounds(1257, 22, 45, 35);
		contentPane.add(button_11);
		
		JButton button_12 = new JButton("");
		button_12.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\logout13.png"));
		button_12.setBackground(Color.WHITE);
		button_12.setBounds(1309, 22, 45, 35);
		contentPane.add(button_12);
		
		JLabel lblSwapper = new JLabel("Swapper");
		lblSwapper.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSwapper.setBounds(183, 248, 101, 30);
		contentPane.add(lblSwapper);
		
		JLabel lblNewLabel = new JLabel("Exchanges");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(1053, 247, 125, 32);
		contentPane.add(lblNewLabel);
		
		JLabel lblPleaseChooseA = new JLabel(" Please Choose a type of statistics :");
		lblPleaseChooseA.setBounds(614, 158, 205, 14);
		contentPane.add(lblPleaseChooseA);
	}
}
