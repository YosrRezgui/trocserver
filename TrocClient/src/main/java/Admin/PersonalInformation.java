package Admin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;

import com.alee.laf.WebLookAndFeel;

import delegate.AdminDelegate;
import domain.Administrator;
import javax.swing.ImageIcon;

public class PersonalInformation extends JFrame {

	private JPanel contentPane;
	private JTextField txtFname;
	private JTextField txtSname;
	private JTextField txtemail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					PersonalInformation frame = new PersonalInformation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PersonalInformation() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 683, 431);
		contentPane = new JPanel();
		contentPane.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
				Administrator admin=AdminDelegate.afficherAdmin();
				txtFname.setText(admin.getFirstName());
				txtSname.setText(admin.getSecondName());
				txtemail.setText(admin.getMail());
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("First Name");
		
		lblNewLabel.setBounds(100, 29, 177, 39);
		contentPane.add(lblNewLabel);
		
		JLabel lblSecondName = new JLabel("Second Name");
		
		lblSecondName.setBounds(100, 87, 177, 39);
		contentPane.add(lblSecondName);
		
		JLabel lblEmail = new JLabel("Email");
		
		lblEmail.setBounds(100, 148, 177, 39);
		contentPane.add(lblEmail);
		
		txtFname = new JTextField();
		txtFname.setBounds(238, 37, 204, 26);
		contentPane.add(txtFname);
		txtFname.setColumns(10);
		
		txtSname = new JTextField();
		txtSname.setColumns(10);
		txtSname.setBounds(238, 95, 204, 26);
		contentPane.add(txtSname);
		
		txtemail = new JTextField();
		txtemail.setColumns(10);
		txtemail.setBounds(238, 156, 204, 26);
		contentPane.add(txtemail);
		
		JButton btnUpdate = new JButton("       Update");
		btnUpdate.setIcon(new ImageIcon(PersonalInformation.class.getResource("/Ressources/ImgFromMaster/pencil-icon.png")));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator admin=AdminDelegate.afficherAdmin();
				admin.setFirstName(txtFname.getText());
				admin.setSecondName(txtSname.getText());
				admin.setMail(txtemail.getText());
				AdminDelegate.modifierAdmin(admin);
				String message = "the account was modified with success";
				JOptionPane.showMessageDialog(new JFrame(), message,
						"Alert", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		btnUpdate.setBounds(130, 243, 142, 39);
		contentPane.add(btnUpdate);
		
		JButton btnCancel = new JButton("       Cancel");
		btnCancel.setIcon(new ImageIcon(PersonalInformation.class.getResource("/Ressources/ImgFromMaster/Actions-dialog-cancel-icon.png")));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
			}
		});
		
		btnCancel.setBounds(300, 243, 147, 39);
		contentPane.add(btnCancel);
	}

}
