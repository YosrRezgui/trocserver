package Admin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import delegate.AdminDelegate;
import domain.Administrator;

import java.awt.Color;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import com.alee.laf.WebLookAndFeel;

public class InformationSecurity extends JFrame {

	private JPanel contentPane;
	private JTextField txtlogin;
	private JPasswordField txtpwd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					InformationSecurity frame = new InformationSecurity();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InformationSecurity() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
				Administrator admin = AdminDelegate.afficherAdmin();
				txtlogin.setText(admin.getLogin());
				txtpwd.setText(admin.getPassword());
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		contentPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLogin = new JLabel("Login");
		
		lblLogin.setBounds(100, 71, 91, 30);
		contentPane.add(lblLogin);
		
		JLabel lblPassword = new JLabel("Password");
		
		lblPassword.setBounds(100, 126, 91, 30);
		contentPane.add(lblPassword);
		
		txtlogin = new JTextField();
		txtlogin.setBounds(220, 71, 133, 28);
		contentPane.add(txtlogin);
		txtlogin.setColumns(10);
		
		txtpwd = new JPasswordField();
		txtpwd.setBounds(220, 129, 133, 30);
		contentPane.add(txtpwd);
		
		JButton btnNewButton = new JButton("          Update");
		btnNewButton.setIcon(new ImageIcon(InformationSecurity.class.getResource("/Ressources/ImgFromMaster/pencil-icon.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator admin = AdminDelegate.afficherAdmin();
				admin.setLogin(txtlogin.getText());
				admin.setPassword(txtpwd.getText());
				AdminDelegate.modifierAdmin(admin);
				String message = "the account was modified with success";
				JOptionPane.showMessageDialog(new JFrame(), message,
						"Alert", JOptionPane.INFORMATION_MESSAGE);
			
				
				
			}
		});
		
		btnNewButton.setBounds(10, 189, 148, 39);
		contentPane.add(btnNewButton);
		
		JButton btnCancel = new JButton("        Cancel");
		btnCancel.setIcon(new ImageIcon(InformationSecurity.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnCancel.setBounds(183, 189, 170, 39);
		contentPane.add(btnCancel);
	}
}
