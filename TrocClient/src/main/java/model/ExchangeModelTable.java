package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import delegate.OfferDelegate;
import domain.Exchange;
import domain.Offer;

public class ExchangeModelTable extends AbstractTableModel{


	List<Exchange> exchanges;
	String []headers ={"N°","Offer1","Offer2","swapper1","swapper2","date"};
	
	public ExchangeModelTable(List<Exchange> e) {
	this.exchanges=e;
		 }
	
	

	


	@Override
	public int getRowCount() {
		return exchanges.size();
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}
	
	

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
            return exchanges.get(rowIndex).getIdExchangePk();
        case 1:
        	return exchanges.get(rowIndex).getRequest().getOffer1().getNameOffer();
        case 2:
        	return exchanges.get(rowIndex).getRequest().getOffer2().getNameOffer();
        case 3:
        	return exchanges.get(rowIndex).getRequest().getOffer1().getSwapper().getFirstName();
        case 4:
        	return exchanges.get(rowIndex).getRequest().getOffer2().getSwapper().getFirstName();
        case 5:
        	return exchanges.get(rowIndex).getDateExchange();
        default:
		throw new IllegalArgumentException();
	}
		
	}
	  public String getColumnName(int column) {
	        return headers[column];
	    }
	     
	    }
	    


	 




