package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;



import delegate.SubCategoryDelegate;
import domain.SubCategory;

public class SubCategoryModel extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	String[] entetes ={"Num","Name"};
	 List<SubCategory> subCategorys;
	 
	 public SubCategoryModel(int id){
		 
		 subCategorys=SubCategoryDelegate.findSub(id);
	 }
	 
	 
	@Override
	public int getRowCount() {
		
		return subCategorys.size();
	}
	@Override
	public int getColumnCount() {
		
		return entetes.length;
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return subCategorys.get(rowIndex).getIdSubCat();
		case 1:
			return subCategorys.get(rowIndex).getNameSubCat();
			
		default:
			throw new IllegalArgumentException();
		}
		
	}
	public String getColumnName(int column){
		return entetes[column];
    
	}
	
	@Override
	   public void setValueAt(Object aValue, int rowIndex, int columnIndex){
			subCategorys.get(rowIndex).setNameSubCat((String) aValue);
		
	}
	
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return columnIndex == 1; //Or whatever column index you want to be editable
		}
	

}
