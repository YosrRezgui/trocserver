package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;
import delegate.NoteDelegate;
import domain.Note;
import domain.Swapper;

public class NotesModel extends AbstractTableModel {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
List <Note> notes;
String[] entetes ={"ID","Noted by","Note"};

public NotesModel(int id) {
	notes= NoteDelegate.findNoteBySwapper(id);
}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return notes.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0: 
        	return notes.get(rowIndex).getIdNote();
        case 1:
        	Swapper s= notes.get(rowIndex).getNoteSender();
        	return s.getFirstName();
        case 2:
        	return notes.get(rowIndex).getNote();
        default:
        	throw new IllegalArgumentException();
        	
		}
	}
	@Override
	 public String getColumnName(int column) {
			return entetes[column];
	    
		}

}
