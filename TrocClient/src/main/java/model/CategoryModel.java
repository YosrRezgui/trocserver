package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import delegate.CategoryDelegate;
import domain.Category;

public class CategoryModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1L;
	String[] entetes ={"Num","Name"};
	 List<Category> categorys;
	 
	 public CategoryModel(){
		 
		 categorys=CategoryDelegate.displayAllCategory();
	 }
	 
	 
	@Override
	public int getRowCount() {
		
		return categorys.size();
	}
	@Override
	public int getColumnCount() {
		
		return entetes.length;
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return categorys.get(rowIndex).getIdCategory();
		case 1:
			return categorys.get(rowIndex).getNameCategory();
			
		default:
			throw new IllegalArgumentException();
		}
		
	}
	
	
	@Override
	   public void setValueAt(Object aValue, int rowIndex, int columnIndex){
			categorys.get(rowIndex).setNameCategory((String) aValue);
		
	}
	
	
	public String getColumnName(int column){
		return entetes[column];
    
	}
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return columnIndex == 1; 
		}
	
	

}
