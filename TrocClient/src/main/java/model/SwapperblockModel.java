package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import delegate.SwapperDelegate;
import domain.Swapper;

public class SwapperblockModel extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Swapper> swappers ;
	String[] entetes ={"ID","First_Name","Second_Name","Mail","NIC"};
	public SwapperblockModel() {
		super();
		swappers= SwapperDelegate.findALLSwapperblock();
	}

	@Override
	public int getRowCount() {
		return swappers.size();
	}

	@Override
	public int getColumnCount() {
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		  case 0:
	            return swappers.get(rowIndex).getIdSwapperPk();
	        case 1:
	        	return swappers.get(rowIndex).getFirstName();
	        case 2:
	        	return swappers.get(rowIndex).getSecondName();
	        case 3:
	        	return swappers.get(rowIndex).getMail();
	        case 4:
	        	return swappers.get(rowIndex).getNic();
	        default:
			throw new IllegalArgumentException();
	}
	}
	public String getColumnName(int column) {
		return entetes[column];
    
	}

}
