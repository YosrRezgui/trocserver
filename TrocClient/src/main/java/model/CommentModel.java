package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import domain.Comment;
import domain.Event;


public class CommentModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	List<Comment> Comments;
	String[] entetes ={"User","Content","Date","ID"};
	
	

	
	public CommentModel(Event e) {
		
		Comments=e.getComments();
		
	}

	@Override
	public int getRowCount() {
		return Comments.size();
				
	}

	@Override
	public int getColumnCount() {
		
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return Comments.get(rowIndex).getSwapper().getLogin();
		case 1:
			return Comments.get(rowIndex).getContents();
		case 2:
			return Comments.get(rowIndex).getDateComment();
		case 3:
			return Comments.get(rowIndex).getIdCommentPk();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public String getColumnName(int column){
		return entetes[column];
    
	}

}
