package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import domain.Event;
import domain.Participant;


public class ParticipantModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	List<Participant> participants;
	String[] entetes ={"First Name","Last Name","Mail"};
	
	public ParticipantModel(Event event){
		
		participants=event.getParticipants();
	}


	@Override
	public int getRowCount() {
		
		return participants.size();
	}

	@Override
	public int getColumnCount() {
		
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return participants.get(rowIndex).getSwapper().getFirstName();
		case 1:
			return participants.get(rowIndex).getSwapper().getSecondName();
		case 2:
			return participants.get(rowIndex).getSwapper().getMail();
		default:
			throw new IllegalArgumentException();
		}
		
	}
	
	public String getColumnName(int column){
		return entetes[column];
    
	}

}
