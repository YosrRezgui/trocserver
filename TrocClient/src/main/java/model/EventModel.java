package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import domain.Event;
import delegate.EventDelegate;

public class EventModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 1L;
	 String[] entetes ={"Name","Theme"," place","Date"};
	 
	 List<Event> events;
	
	
	/**
	 * 
	 */
	public EventModel() {
		events=EventDelegate.findAllEvent();
		
	}
	public EventModel(List<Event> ev) {
		events=ev;
		
	}

	

	@Override
	public int getRowCount() {
		
		return events.size();
	}

	@Override
	public int getColumnCount() {
		
		return entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
            return events.get(rowIndex).getNameEvent();
        case 1:
        	return events.get(rowIndex).getThemeEvent();
        case 2:
        	return events.get(rowIndex).getPlaceEvent();
        case 3:
        	return events.get(rowIndex).getDateEvent();
        default:
		throw new IllegalArgumentException();
	}
	}
		public String getColumnName(int column){
			return entetes[column];
	    
		}
	
	
	
	

}
