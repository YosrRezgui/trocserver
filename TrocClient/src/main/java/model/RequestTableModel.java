package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import delegate.OfferDelegate;
import delegate.RequestDelegate;
import domain.Offer;
import domain.Request;

public class RequestTableModel extends AbstractTableModel {
	
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		List<Request> requests;
		String []headers ={"N°","NameOffer","NameSwapper","Date"};
		
		public RequestTableModel(int o) {
			requests=RequestDelegate.findAllRequestByOffer(o);
		
		}
		

		


		@Override
		public int getRowCount() {
			return requests.size();
		}

		@Override
		public int getColumnCount() {
			return headers.length;
		}
		
		

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch(columnIndex){
	        case 0:
	            return requests.get(rowIndex).getOffer2().getIdOfferPk();
	        case 1:
	        	return requests.get(rowIndex).getOffer2().getNameOffer();
	        case 2:
	        	return requests.get(rowIndex).getOffer2().getSwapper().getFirstName();
	        case 3:
	        	return requests.get(rowIndex).getDateRequest();
	        default:
			throw new IllegalArgumentException();
		}
			
		}
		  public String getColumnName(int column) {
		        return headers[column];
		    }
}