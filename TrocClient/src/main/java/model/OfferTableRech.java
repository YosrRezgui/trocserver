package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import delegate.OfferDelegate;
import domain.Offer;

public class OfferTableRech extends AbstractTableModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Offer> offers;
	String []headers ={"N°","Name","Date","Desc","Sawpper","Supprimer"};
	Boolean rowlist[][] = new Boolean[50][50];
	public OfferTableRech(List<Offer> offers) {
	this.offers=offers;
		 
	}
	

	


	@Override
	public int getRowCount() {
		return offers.size();
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}
	
	

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0:
            return offers.get(rowIndex).getIdOfferPk();
        case 1:
        	return offers.get(rowIndex).getNameOffer();
        case 2:
        	return offers.get(rowIndex).getDateOffer();
        case 3:
        	return offers.get(rowIndex).getDescriptionOffer();
        case 4:
        	return offers.get(rowIndex).getSwapper().getFirstName();
        case 5:
        	 return rowlist[rowIndex][5];
        default:
		throw new IllegalArgumentException();
	}
		
	}
	  public String getColumnName(int column) {
	        return headers[column];
	    }
	     @Override
	  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	      boolean b = (Boolean) aValue;
	            if(columnIndex==5)
	                rowlist[rowIndex][5]=b;
	           
	        fireTableCellUpdated(rowIndex, columnIndex);
	      
	  }
	  
	 
	    @Override
	    public Class<?> getColumnClass(int columnIndex) {
	        if(columnIndex==5 ){
	            return Boolean.class;
	        }
	        return super.getColumnClass(columnIndex); //To change body of generated methods, choose Tools | Templates.
	    }
	    
	    
	    @Override
	    public boolean isCellEditable(int row, int column) {
	    return (column > 4);
	  }
	      
	    }
	    


	 



