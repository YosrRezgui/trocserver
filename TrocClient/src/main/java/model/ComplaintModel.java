package model;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import delegate.ComplaintDelegate;
import domain.Complaint;

public class ComplaintModel extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Complaint> complaints;
	String[] entetes ={"ID","From","Subject"};
	public ComplaintModel() {
		complaints= ComplaintDelegate.findAllComplaint();
	}
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return complaints.size();
	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return entetes.length;
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
        case 0: 
        	return complaints.get(rowIndex).getIdComplaintPk();
        case 1:
        	return complaints.get(rowIndex).getSwapperAj().getFirstName();
        case 2:
        	return complaints.get(rowIndex).getSubject();
        /*case 3:
        {
        	if (complaints.get(rowIndex).getOffer().equals(null))
        		return "Swapper: "+complaints.get(rowIndex).getSwapperRe().getFirstName();
        	else
        		return "Offer: "+complaints.get(rowIndex).getOffer().getNameOffer();
        }*/

        default :
        	throw new IllegalArgumentException();
		}
        	
	}
	 public String getColumnName(int column) {
			return entetes[column];
	    
		}
}
