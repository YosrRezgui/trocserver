package service;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
public class ServiceLocator {
	Context context;
	Map<String, Object> cache;
	static ServiceLocator instance;
	
	private ServiceLocator() {
		cache = new HashMap<String, Object>();
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			
			e.printStackTrace();
		}
		
	}

public static ServiceLocator getInstance(){
	if(instance==null){
		instance= new ServiceLocator();
		
	}
	return instance;
} 
/**
 * 
 * @param jndi
 * @object
 */
public synchronized Object getProxy(String jndi){
	Object object= null;
	if(cache.get(jndi)!=null)
	{
		return cache.get(jndi);
	}
	else{
		try {
			object = context.lookup(jndi);
			if(object!=null)
			{
				cache.put(jndi, object);
			}
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	return object;
}

}