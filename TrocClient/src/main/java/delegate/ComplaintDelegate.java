package delegate;

import java.util.List;

import domain.Complaint;
import service.ServiceLocator;
import services.ComplaintServiceEJBRemote;


public class ComplaintDelegate {
	private static ComplaintServiceEJBRemote getRemote()
	{
		return (ComplaintServiceEJBRemote) ServiceLocator.getInstance().getProxy("/trocEJB/ComplaintServiceEJB!services.ComplaintServiceEJBRemote");	
	}
	public static void addComplaint(Complaint complaint)
	{
		getRemote().addComplaint(complaint);
	}
	public static void removeComplaint(Complaint complaint)
	{
		getRemote().delete(complaint);
	}
	public static void updateComplaint(Complaint complaint)
	{
		getRemote().updateComplaint(complaint);
	}
	public static Complaint findComplaintById(int id){
		
		return getRemote().findComplaintById(id);
	}
	public static List<Complaint> findAllComplaint()
	{
		
		return getRemote().findAllComplaint();
	
	}
}
