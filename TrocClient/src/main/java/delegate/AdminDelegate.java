package delegate;



import java.util.List;

import org.omg.CORBA.PRIVATE_MEMBER;

import domain.Administrator;
import domain.Exchange;
import domain.Swapper;
import service.ServiceLocator;
import services.administrateurServiceRemote;


public class AdminDelegate {
	private static final String jndiName ="/trocEJB/administrateurService!services.administrateurServiceRemote";
	private static administrateurServiceRemote getRemote()
	{
		return (administrateurServiceRemote) service.ServiceLocator.getInstance().getProxy(jndiName);	
	}
	
	public static Administrator authentification(String login , String password){
	
		return getRemote().authentification(login, password);
}
	
	public static void modifierAdmin(Administrator administrator) {
		
		getRemote().modifierAdmin(administrator);
	}
	public static Administrator afficherAdmin() {
		return getRemote().afficherAdmin();
		
	}
	
	public static List<Exchange> NbrExchangeValide() {
		
	return getRemote().NbrExchangeValide();
	}
	
public static List<Exchange> NbrExchangeNonValide() {
		
	return getRemote().NbrExchangeNonValide();
	}

public static List<Exchange> NbrExchangeSwapper1NonValide() {
	
	return getRemote().NbrExchangeSwapper1NonValide();
}

public static List<Swapper> nbrSwapperConfirme() {
	
	return getRemote().nbrSwapperConfirme();
}

public static List<Swapper> nbrSwapperNonConfirme() {
	
	return getRemote().nbrSwapperNonConfirme();
}
	
}



