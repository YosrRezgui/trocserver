package delegate;


import java.util.List;

import service.ServiceLocator;
import services.CategoryServicesRemote;
import domain.Category;


public class CategoryDelegate {
	
	private static CategoryServicesRemote getRemote()
	{
		return (CategoryServicesRemote) ServiceLocator.getInstance().getProxy("/trocEJB/CategoryServices!services.CategoryServicesRemote");	
	}
	public static void addCategory(Category category)
	{
		getRemote().addCategory(category);
	}
	public static void removeCategory(Category category)
	{
		getRemote().removeCategory(category);
	}
	public static void updateCategory(Category category)
	{
		getRemote().updateCategory(category);
	}
	public static Category findCategoryById(int id){
	return getRemote().findCategoryById(id);
		
	}
	public static List<Category> displayAllCategory(){
		List<Category> categories =getRemote().displayAllCategory();
		return categories;
		
	}
}
