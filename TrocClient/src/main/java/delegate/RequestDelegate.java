package delegate;

import java.util.List;

import service.ServiceLocator;
import services.RequestServiceRemote;
import domain.Offer;
import domain.Request;

public class RequestDelegate {
	private static RequestServiceRemote getRemote()
	{
		return (RequestServiceRemote) ServiceLocator.getInstance().getProxy("/trocEJB/RequestService!services.RequestServiceRemote");	
	}
	public static void addRequest(Request request)
	{
		getRemote().addRequest(request);
	}
	public static void removeRequest(Request request)
	{
		getRemote().deleteRequest(request);
	}
	public static void updateRequest(Request request)
	{
		getRemote().updateRequest(request);
	}
	public static Request findRequestById(int id){
		
		return getRemote().findRequestById(id);
	}
	public static List<Request> findAllRequest()
	{
		
		return getRemote().findAllRequest();
	
	}
	public static List<Request> findAllRequestByOffer(int i)
	{
		return getRemote().findAllRequestByOffer(i);
	}
	public static Request findAllRequestByOffer2(int id){
		
		return getRemote().findAllRequestByOffer2(id);
	}
	public static List<Request> SearchByName(String name){
		List<Request> offers =getRemote().SearchByName(name);
		return offers;
		
		
		
	}
	public static List<Request> SearchBySwapper(String name){
		List<Request> offers =getRemote().SearchBySwapper(name);
		return offers;

}
}
