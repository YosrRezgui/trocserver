package delegate;

import java.util.List;

import domain.Event;
import domain.Swapper;
import service.ServiceLocator;
import services.EventServiceRemote;

public class EventDelegate {
	private static EventServiceRemote getRemote()
	{
		return (EventServiceRemote) ServiceLocator.getInstance().getProxy("/trocEJB/EventService!services.EventServiceRemote");	
	}
	
	
	
	
	public static void addEvent(Event event)
	{
		getRemote().addEvent(event);
	}
	public static void removeEvent(Event event)
	{
		getRemote().deleteEvent(event);
	}
	public static void updateEvent(Event event)
	{
		getRemote().updateEvent(event);
	}
	public static Event findEventById(int id){
		Event  o=getRemote().findByIdEvent(id);
		return o;
	}
	public static List<Event> findAllEvent(){
		List<Event> events =getRemote().findAllEvent();
		return events;
			}
	public static Event findEventByName(String name){
		
		return getRemote().findEventByName(name);
	}
	public static List<Event> findEventsByName(String name){
		return getRemote().findEventsByName(name);
	}
	public static List<Event> findThemsByName(String theme) {
		return getRemote().findThemsByName(theme);
		
	}
	public static void sendMail(List<Swapper> s, Event e){
		getRemote().sendMail(s, e);
	}
	

}
