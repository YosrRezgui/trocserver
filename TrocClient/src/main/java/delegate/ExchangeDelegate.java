package delegate;

import java.util.List;

import service.ServiceLocator;
import services.ExchangeserviceRemote;

import domain.Exchange;


public class ExchangeDelegate {
	
	private static ExchangeserviceRemote getRemote()
	{
		return (ExchangeserviceRemote) ServiceLocator.getInstance().getProxy("/trocEJB/Exchangeservice!services.ExchangeserviceRemote");	
	}
	public static void addExchange(Exchange exhange)
	{
		getRemote().addExchange(exhange);
	}
	public static void removeExchange(Exchange exhange)
	{
		getRemote().deleteExchange(exhange);
	}
	public static void updateExchange(Exchange exhange)
	{
		getRemote().updateExchange(exhange);
	}
	public static Exchange findEchangeById(int id){
		Exchange  o=getRemote().findExchangeById(id);
		return o;
	}
	public static List<Exchange> findAllExchangeV(){
		List<Exchange> exhanges =getRemote().findAllExchangeV();
		return exhanges;
		
			
	}
	public static List<Exchange> findAllExchangeF(){
		List<Exchange> exhanges =getRemote().findAllExchangeF();
		return exhanges;
		
			
	}
	public static List<Exchange> findAllExchange(){
		List<Exchange> exhanges =getRemote().findAllExchange();
		return exhanges;
		
			
	}
	
	
	

}
