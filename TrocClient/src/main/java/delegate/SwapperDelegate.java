package delegate;

import java.util.List;

import domain.Swapper;
import service.ServiceLocator;
import services.SwapperServiceEJBRemote;




public class SwapperDelegate {

	
		private static SwapperServiceEJBRemote getRemote()
		{
			return (SwapperServiceEJBRemote) ServiceLocator.getInstance().getProxy("/trocEJB/SwapperServiceEJB!services.SwapperServiceEJBRemote");	
		}
		public static void addSwapper(Swapper swapper)
		{
			getRemote().addSwapper(swapper);
		}
		public static void removeSwapper(Swapper swapper)
		{
			getRemote().delete(swapper);
		}
		public static void updateSwapper(Swapper swapper)
		{
			getRemote().updateSwapper(swapper);
		}
		public static Swapper findSwapperById(int id){
			
			return getRemote().findSwapperById(id);
		}
		public static List<Swapper> findAllSwapper()
		{
			
			return getRemote().findAllSwapper();
		
		}
		public static List<Swapper> findALLSwapperActifs(){
			return getRemote().findALLSwapperActifs();
		}
		public static List<Swapper> findALLSwapperPassifs(){
			return getRemote().findALLSwapperPassifs();
			
		}
		public static List<Swapper> findALLSwapperblock(){
			return getRemote().findALLSwapperblock();
			
		}
		public static void blockSwapper(Swapper swapper) {
		 getRemote().blockSwapper(swapper);
		}
		public  static void unblockSwapper(Swapper swapper) {
			getRemote().unblockSwapper(swapper);
		}
		public  static void verificationSwapper(Swapper swapper) {
			getRemote().verificationSwapper(swapper);
		}
		public  static int nbrSwapperNotVerified() {
			return getRemote().nbrSwapperNotVerified();
		}
		public  static  List<Swapper> findSwapperByFirstName(String firstName) {
			return getRemote().findSwapperByFirstName(firstName);
		}
		public  static List<Swapper> findSwapperBySecondName(String secondName) {
			return getRemote().findSwapperBySecondName(secondName);
		}
		public  static  List<Swapper> findSwapperByCIN(int cin) {
			return getRemote().findSwapperByCIN(cin);
		}
		public  static  List<Swapper> findSwapperByMail(String mail) {
			return getRemote().findSwapperByMail(mail);
		}
		public  static  List<Swapper> findSwapperBlockByFirstName(String firstName) {
			return getRemote().findSwapperBlockByFirstName(firstName);
		}
		public  static List<Swapper> findSwapperBlockBySecondName(String secondName) {
			return getRemote().findSwapperBlockBySecondName(secondName);
		}
	
		public  static  List<Swapper> findSwapperBlockByMail(String mail) {
			return getRemote().findSwapperBlockByMail(mail);
		}
		}

