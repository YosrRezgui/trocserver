package delegate;

import java.util.List;

import service.ServiceLocator;
import services.CategoryServicesRemote;
import services.SubCategoryServicesRemote;
import domain.Category;
import domain.SubCategory;

public class SubCategoryDelegate {
	private static SubCategoryServicesRemote getRemote()
	{
		return (SubCategoryServicesRemote) ServiceLocator.getInstance().getProxy("/trocEJB/SubCategoryServices!services.SubCategoryServicesRemote");	
	}
	public static void addSubCategory(SubCategory subCategory)
	{
		getRemote().addSubCategory(subCategory);
	}
	public static void removeSubCategory(SubCategory subCategory)
	{
		getRemote().removeSubCategory(subCategory);
	}
	public static void updateSubCategory(SubCategory subCategory)
	{
		getRemote().updateSubCategory(subCategory);
	}
	public static SubCategory findSubCategoryById(int id){
		return getRemote().findSubCategoryById(id);
			
		}
	public static List<SubCategory> displayAllCategory(){
		List<SubCategory> subCategories =getRemote().displayAllSubCategory();
		return subCategories;
		
	}
	
	public static List<SubCategory> findSub(int id){
		List<SubCategory> subCategories =getRemote().findSub(id);
		return subCategories;
		
	}
	
	
}
