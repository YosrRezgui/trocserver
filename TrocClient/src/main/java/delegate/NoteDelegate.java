package delegate;

import java.util.List;

import domain.Note;
import services.NoteServiceRemote;

public class NoteDelegate {
	private static final String jndiName ="/trocEJB/NoteService!services.NoteServiceRemote";
	private static NoteServiceRemote getRemote(){
		return (NoteServiceRemote) service.ServiceLocator.getInstance().getProxy(jndiName);
	}
	public static void addNote(Note note){
		getRemote().addNote(note);
	}
	public static void removeNote(Note note){
		getRemote().removeNote(note);
	}
	public static void updateNote(Note note){
		getRemote().updateNote(note);
	}
	public static Note findNoteById(int id){
		return getRemote().findNoteById(id);
	}
	public static List<Note> displayAllNotes(){
		return getRemote().displayAllNotes();
	}
	public static List<Note> findNoteBySwapper(int id) {
		return getRemote().findNoteBySwapper(id);
	}
}
