package delegate;

import java.util.Date;
import java.util.List;

import service.ServiceLocator;
import services.OfferServicesRemote;
import domain.Offer;

public class OfferDelegate {
	private static OfferServicesRemote getRemote()
	{
		return (OfferServicesRemote) ServiceLocator.getInstance().getProxy("/trocEJB/OfferServices!services.OfferServicesRemote");	
	}
	public static void addOffer(Offer offer)
	{
		getRemote().addOffer(offer);
	}
	public static void removeOffer(Offer offer)
	{
		getRemote().removeOffer(offer);
	}
	public static void updateOffer(Offer offer)
	{
		getRemote().updateOffer(offer);
	}
	public static Offer findOfferById(int id){
		Offer  o=getRemote().findOfferById(id);
		return o;
	}
	public static List<Offer> displayAllOffers(){
		List<Offer> offers =getRemote().displayAllOffers();
		return offers;
		
	}
	public static List<Offer> displayAllOffersV(){
		List<Offer> offers =getRemote().displayAllOffersV();
		return offers;
		
	}
	public static List<Offer> displayAllOffersC(){
		List<Offer> offers =getRemote().displayAllOffersC();
		return offers;
	}
	public static List<Offer> displayAllOffersVC(){
		List<Offer> offers =getRemote().displayAllOffersVC();
		return offers;
	}
	public static int nbrOfferOfSwapper(int id) {
		return getRemote().nbrOfferOfSwapper(id);

	}
	public static List<Offer> SearchByName(String name){
		List<Offer> offers =getRemote().SearchByName(name);
		return offers;
		
		
		
	}
	public static List<Offer> SearchBySwapper(String name){
		List<Offer> offers =getRemote().SearchBySwapper(name);
		return offers;
		
		
		
	}public static List<Offer> SearchByDate(Date name){
		List<Offer> offers =getRemote().SearchByDate(name);
		return offers;
		
		
		
	}
}



