package delegate;

import java.util.List;

import service.ServiceLocator;
import services.CommentServiceEJBRemote;
import domain.Comment;

public class CommentDelegate {
	private static CommentServiceEJBRemote getRemote()
	{
		return (CommentServiceEJBRemote) ServiceLocator.getInstance().getProxy("/trocEJB/CommentServiceEJB!services.CommentServiceEJBRemote");	
	}
	public static void addComment(Comment comment)
	{
		getRemote().addComment(comment);
	}
	public static void removeComment(Comment comment)
	{
		getRemote().delete(comment);
	}
	public static void updateComment(Comment comment)
	{
		getRemote().updateComment(comment);
	}
	public static Comment findCommentById(int id){
		
		return getRemote().findCommentById(id);
	}
	public static List<Comment> findAllComment()
	{
		
		return getRemote().findAllComment();
	
	}

}
