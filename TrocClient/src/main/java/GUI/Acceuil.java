package GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;

import model.OfferModelfiltre;
import model.OfferTableModel;
import model.OfferTableRech;
import model.RequestTableModel;
import model.RequestTableModel1;

import org.apache.commons.io.FileUtils;
import org.eclipse.wb.swing.FocusTraversalOnArray;

import com.alee.laf.WebLookAndFeel;

import delegate.OfferDelegate;
import delegate.RequestDelegate;
import delegate.SwapperDelegate;
import domain.Offer;
import domain.Request;
import domain.Swapper;

public class Acceuil extends JFrame  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private OfferTableModel modelOffer;
	private RequestTableModel modelRequest;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnNewButton_1;
	private JTable table_1;
	private JTable table_2;
	JScrollPane scrollPane_1;
	JLabel offerLabel;
	JLabel j1;
	JLabel date;
	private JLabel lblDescription;
	private JLabel lblInteresstedby;
	private JPanel panel;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JButton btnNewButton_3;
	private JButton btnNewButton_12;
	private JButton btnNewButton_13;
	private JButton btnNewButton_14;
	private JPanel panel_2;
	private JLabel lblOffersManagement;
	private JButton btnNewButton_15;
	private JButton btnNewButton_16;
	private JLabel lblNewLabel_6;
	JLayeredPane layeredPane;
	private JTextField textField;
	JCheckBox jCheckBox;
	JComboBox comboBox;
	 JComboBox comboBox_1;
	List<Offer> allOffers;
	JOptionPane op;
	private JLabel lblNewLabel_7;
	JTextArea textArea ;
	private JLabel lblNewLabel_8;
	private JLabel lblNewLabel_9;
	private JLabel lblNewLabel_10;
	private JLabel lblNewLabel_11;
	private JTextField textField_2;
	Date date1;
	private JLabel lblNewLabel_12;
	private JLabel lblNewLabel_13;
	private JLabel lblNewLabel_14;
	private JLabel lblNewLabel_15;
	private JLabel lblNewLabel_17;
	private JLabel lblNewLabel_18;
	 JLabel lblNewLabel_19;
	 OfferModelfiltre omf;
	 private JTextField textField_1;
	 private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					Acceuil frame = new Acceuil();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	
	public Acceuil() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		
		setBackground(Color.WHITE);
		setTitle("Offers Management");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//allOffers=OfferDelegate.displayAllOffers();
		modelOffer=new OfferTableModel();
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.control);
		panel_1.setBounds(0, 598, 1354, 96);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		btnNewButton_12 = new JButton("");
		btnNewButton_12.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNewButton_12.setBounds(626, 11, 79, 74);
		panel_1.add(btnNewButton_12);
		btnNewButton_12.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setBounds(750, 26, 70, 59);
		panel_1.add(btnNewButton_8);
		btnNewButton_8.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		
		JButton btnNewButton_11 = new JButton("");
		btnNewButton_11.setBounds(870, 26, 70, 59);
		panel_1.add(btnNewButton_11);
		btnNewButton_11.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setBounds(991, 26, 70, 59);
		panel_1.add(btnNewButton_6);
		btnNewButton_6.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setBounds(1120, 26, 70, 59);
		panel_1.add(btnNewButton_4);
		btnNewButton_4.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		btnNewButton_4.setBackground(Color.WHITE);
		
		btnNewButton_14 = new JButton("");
		btnNewButton_14.setBounds(1257, 33, 62, 52);
		panel_1.add(btnNewButton_14);
		btnNewButton_14.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/right208.png")));
		
		JButton btnNewButton_9 = new JButton("");
		btnNewButton_9.setBounds(497, 26, 70, 59);
		panel_1.add(btnNewButton_9);
		btnNewButton_9.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		
		JButton btnNewButton_10 = new JButton("");
		btnNewButton_10.setBounds(372, 26, 70, 59);
		panel_1.add(btnNewButton_10);
		btnNewButton_10.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setBounds(257, 26, 70, 59);
		panel_1.add(btnNewButton_7);
		btnNewButton_7.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setBounds(118, 26, 70, 59);
		panel_1.add(btnNewButton_5);
		btnNewButton_5.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		
		btnNewButton_13 = new JButton("");
		btnNewButton_13.setBounds(10, 33, 62, 52);
		panel_1.add(btnNewButton_13);
		btnNewButton_13.setBorder(null);
		btnNewButton_13.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/left224.png")));
		
		panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(0, 62, 1354, 536);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 78, 521, 158);
		panel_2.add(scrollPane);
		List<Offer> off=OfferDelegate.displayAllOffersVC();
		omf=new OfferModelfiltre(off);
		table = new JTable(omf);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int ido=table.getValueAt(table.getSelectedRow(), 0).hashCode();
				System.out.println(ido);
				scrollPane_1.setVisible(true);
				modelRequest=new RequestTableModel(ido);
				table_2.setModel(modelRequest);
				String a=table.getValueAt(table.getSelectedRow(), 5).toString();
				System.out.println(a+table_2.getRowCount());
				if(table_2.getRowCount()==0 && a.equals("valide"))	
				{					
					scrollPane_1.setVisible(false);
					
					 op.showMessageDialog(null, "offre valid");
					 
								}
				if(table_2.getRowCount()==0 && a.equals("supprime"))	
				{					
					
					
					 op.showMessageDialog(null, "offer supprime");
					 
								}
				
				int idR=table.getValueAt(table.getSelectedRow(), 0).hashCode();
				Offer offer=OfferDelegate.findOfferById(idR);
				offerLabel.setText(offer.getNameOffer());
lblNewLabel_19.setText(offer.getSwapper().getFirstName());
				date.setText(offer.getDateOffer().toString());
				textArea.setText(offer.getDescriptionOffer());
				lblNewLabel_7.setText(offer.getInterestedBy());
						
				
				int idR1=table.getValueAt(table.getSelectedRow(), 0).hashCode();
				Offer offer1=new Offer();
				offer1=OfferDelegate.findOfferById(idR1);
				int s1=offer1.getSwapper().getIdSwapperPk();
				Swapper swapper1=new Swapper();
				swapper1=SwapperDelegate.findSwapperById(s1);
				
				lblNewLabel_1.setText(swapper1.getFirstName());
				lblNewLabel_3.setText(swapper1.getSecondName());
				lblNewLabel_5.setText(swapper1.getMail());
				lblNewLabel_13.setText(swapper1.getDateOfBrith().toString());
				lblNewLabel_14.setText(swapper1.getAddress());
				/*byte[] b = offer.getImage();
				File file = new File(offer.getNameOffer());
				try {
					FileUtils.writeByteArrayToFile(file, b);
					 BufferedImage image = ImageIO.read(file);
					 ImageIcon imageIcon = new ImageIcon(image);
					 lblNewLabel_18.setIcon(imageIcon);
					 
					
			
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				byte[] b1 = swapper1.getImage();
				File file1 = new File(swapper1.getFirstName());
				try {
					FileUtils.writeByteArrayToFile(file1, b1);
					 BufferedImage image1 = ImageIO.read(file1);
					 ImageIcon imageIcon1 = new ImageIcon(image1);
					 lblNewLabel_18.setIcon(imageIcon1);
					 
					
			
				} catch (IOException e1) {
					e1.printStackTrace();
				}*/
				
				
			}
			
		});
		
		
		scrollPane.setViewportView(table);
		table.setPreferredScrollableViewportSize(scrollPane.getPreferredSize());
		
		panel = new JPanel();
		panel.setBorder(new SoftBevelBorder(BevelBorder.RAISED, SystemColor.control, SystemColor.control, new Color(180, 180, 180), new Color(180, 180, 180)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(23, 314, 521, 211);
		panel_2.add(panel);
		panel.setLayout(null);
		
		lblNewLabel = new JLabel("First Name");
		lblNewLabel.setBounds(26, 22, 88, 14);
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(167, 22, 88, 14);
		panel.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("Second Name");
		lblNewLabel_2.setBounds(26, 61, 104, 14);
		panel.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(167, 61, 88, 14);
		panel.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Mail");
		lblNewLabel_4.setBounds(26, 100, 88, 14);
		panel.add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setBounds(167, 100, 88, 14);
		panel.add(lblNewLabel_5);
		
		lblNewLabel_12 = new JLabel("Date");
		lblNewLabel_12.setBounds(26, 145, 88, 14);
		panel.add(lblNewLabel_12);
		
		lblNewLabel_13 = new JLabel("");
		lblNewLabel_13.setBounds(167, 145, 132, 14);
		panel.add(lblNewLabel_13);
		
		lblNewLabel_14 = new JLabel("");
		lblNewLabel_14.setBounds(167, 186, 132, 14);
		panel.add(lblNewLabel_14);
		
		lblNewLabel_15 = new JLabel("Adress");
		lblNewLabel_15.setBounds(26, 186, 88, 14);
		panel.add(lblNewLabel_15);
		
		lblNewLabel_17 = new JLabel("");
		lblNewLabel_17.setBounds(327, 22, 171, 77);
		panel.add(lblNewLabel_17);
		
		btnNewButton_3 = new JButton("Delete");
		btnNewButton_3.setVisible(false);
		btnNewButton_3.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\Actions-edit-delete-icon.png"));
		btnNewButton_3.setBounds(437, 247, 107, 23);
		panel_2.add(btnNewButton_3);
		
		btnNewButton_1 = new JButton("detailOffre");
		btnNewButton_1.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/Eye-icon.png")));
		btnNewButton_1.setBounds(833, 259, 113, 33);
		panel_2.add(btnNewButton_1);
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(823, 78, 521, 170);
		panel_2.add(scrollPane_1);
		
		
		
		
		
		
		table_2 = new JTable();
		
		scrollPane_1.setViewportView(table_2);
		
		layeredPane = new JLayeredPane();
		layeredPane.setBounds(833, 308, 544, 217);
		panel_2.add(layeredPane);
		layeredPane.setBorder(new SoftBevelBorder(BevelBorder.RAISED, new Color(180, 180, 180), new Color(180, 180, 180), new Color(180, 180, 180), new Color(180, 180, 180)));
		
			layeredPane.setToolTipText("");
			layeredPane.setBackground(Color.GRAY);
			
			JLabel lblName = new JLabel("Name :");
			lblName.setBounds(10, 21, 46, 14);
			layeredPane.add(lblName);
			
			JLabel lblSwapper = new JLabel("Swapper :");
			lblSwapper.setBounds(10, 56, 71, 14);
			layeredPane.add(lblSwapper);
			
			JLabel lblDate = new JLabel("Date :");
			lblDate.setBounds(10, 92, 46, 14);
			layeredPane.add(lblDate);
			
			 offerLabel = new JLabel("");
			 offerLabel.setBounds(163, 21, 46, 14);
			 layeredPane.add(offerLabel);
			 
			  
			  
			   date = new JLabel("");
			   date.setBounds(163, 92, 142, 14);
			   layeredPane.add(date);
			   
			   lblDescription = new JLabel("Description :");
			   lblDescription.setBounds(10, 164, 81, 14);
			   layeredPane.add(lblDescription);
			   
			   lblInteresstedby = new JLabel("InteresstedBy :");
			   lblInteresstedby.setBounds(10, 130, 71, 14);
			   layeredPane.add(lblInteresstedby);
			   
			   textArea = new JTextArea();
			   textArea.setBackground(SystemColor.control);
			   textArea.setBounds(163, 146, 206, 60);
			   layeredPane.add(textArea);
			   
			   lblNewLabel_7 = new JLabel("");
			   lblNewLabel_7.setBounds(163, 130, 94, 14);
			   layeredPane.add(lblNewLabel_7);
			   
			   lblNewLabel_18 = new JLabel("");
			   lblNewLabel_18.setBounds(324, 21, 181, 98);
			   layeredPane.add(lblNewLabel_18);
			   
			 lblNewLabel_19 = new JLabel("");
			   lblNewLabel_19.setBounds(173, 56, 46, 14);
			   layeredPane.add(lblNewLabel_19);
			   layeredPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblName, lblSwapper, lblDate, offerLabel, lblNewLabel_19, date}));
			   
			   textField = new JTextField();
			   textField.setBounds(357, 47, 86, 20);
			   panel_2.add(textField);
			   
			   textField.addKeyListener(new KeyAdapter() {
			   	@Override
			   	public void keyReleased(KeyEvent e) {
			   		
			   		if(comboBox.getSelectedItem().toString().equals("Name"))
			   		{
			   			List<Offer> offers=new ArrayList<Offer>();
			   			offers=OfferDelegate.SearchByName(textField.getText());
			   			OfferTableRech modelRech = new OfferTableRech(offers);
			   			table.setModel(modelRech);
			   			modelRech.fireTableDataChanged();
			   			
			   			
			   		}
			   		if(comboBox.getSelectedItem().toString().equals("Swapper"))
			   		{
			   			List<Offer> offers1=new ArrayList<Offer>();
			   			offers1=OfferDelegate.SearchBySwapper(textField.getText());
			   			OfferTableRech modelRech = new OfferTableRech(offers1);
			   			table.setModel(modelRech);
			   			modelRech.fireTableDataChanged();
			   			
				   		
			   			
			   		}
			   	}
			   });
			   textField.setColumns(10);
			   
			  
			   
			    comboBox = new JComboBox();
			    comboBox.addMouseListener(new MouseAdapter() {
			    	@Override
			    	public void mouseClicked(MouseEvent e) {
			    		table.setModel(modelOffer);
				   		modelOffer.fireTableDataChanged();
				   		textField.setText("");
			    	}
			    });
			   
			   
			    comboBox.setBounds(466, 47, 78, 20);
			    panel_2.add(comboBox);
			    comboBox.setModel(new DefaultComboBoxModel(new String[] {"Search by :", "Name", "Swapper"}));
			    
			    comboBox_1 = new JComboBox();
			    comboBox_1.addItemListener(new ItemListener() {
			    	public void itemStateChanged(ItemEvent e) {
			    		if(comboBox_1.getSelectedItem().toString().equals("allOffre"))
			    		{
			    			OfferTableModel modelo1=new OfferTableModel();
			    			table.setModel(modelo1);
			    			modelo1.fireTableDataChanged();
			    			
			    		}
			    		if(comboBox_1.getSelectedItem().toString().equals("Valide"))
			    		{
			    			List<Offer> o1=OfferDelegate.displayAllOffersV();
			    			OfferModelfiltre mo=new OfferModelfiltre(o1);
			    			table.setModel(mo);
			    			mo.fireTableDataChanged();
			    			
			    		}
			    		if(comboBox_1.getSelectedItem().toString().equals("encours"))
			    		{
			    			List<Offer> o1=OfferDelegate.displayAllOffersC();
			    			OfferModelfiltre mo=new OfferModelfiltre(o1);
			    			table.setModel(mo);
			    			mo.fireTableDataChanged();
			    			
			    		}
			    	}
			    });
			   
			    comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"allOffre", "Valide", "encours"}));
			    comboBox_1.setBounds(212, 47, 89, 20);
			    panel_2.add(comboBox_1);
			    
			    lblNewLabel_8 = new JLabel("Offer Details");
			    lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 20));
			    lblNewLabel_8.setBounds(1022, 283, 128, 23);
			    panel_2.add(lblNewLabel_8);
			    
			    lblNewLabel_9 = new JLabel("Swapper Details");
			    lblNewLabel_9.setFont(new Font("Tahoma", Font.BOLD, 20));
			    lblNewLabel_9.setBounds(176, 294, 172, 19);
			    panel_2.add(lblNewLabel_9);
			    
			    lblNewLabel_10 = new JLabel("Offer List");
			    lblNewLabel_10.setFont(new Font("Tahoma", Font.BOLD, 24));
			    lblNewLabel_10.setBounds(202, 11, 164, 23);
			    panel_2.add(lblNewLabel_10);
			    
			    lblNewLabel_11 = new JLabel("Request List");
			    lblNewLabel_11.setFont(new Font("Tahoma", Font.BOLD, 24));
			    lblNewLabel_11.setBounds(995, 10, 202, 24);
			    panel_2.add(lblNewLabel_11);
			    
			    JButton btnNewButton_2 = new JButton("Delete");
			    btnNewButton_2.addActionListener(new ActionListener() {
			    	public void actionPerformed(ActionEvent e) {
			    		int idoff=table.getValueAt(table.getSelectedRow(), 0).hashCode();
			    		Offer oof=OfferDelegate.findOfferById(idoff);
			    		oof.setType("aaa");
			    		OfferDelegate.updateOffer(oof);
			    		List<Offer> off=OfferDelegate.displayAllOffersVC();
			    		omf=new OfferModelfiltre(off);
			    		table.setModel(omf);
			    		omf.fireTableDataChanged();
			    		modelRequest=new RequestTableModel(idoff);
			    		table_2.setModel(modelRequest);
			    		modelRequest.fireTableDataChanged();
			    		
			    		
			    		
			    	}
			    });
			    btnNewButton_2.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
			    btnNewButton_2.setBounds(23, 247, 107, 23);
			    panel_2.add(btnNewButton_2);
			    
			    JLabel lblNewLabel_16 = new JLabel("");
			    lblNewLabel_16.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/search-icon.png")));
			    lblNewLabel_16.setBounds(320, 44, 46, 23);
			    panel_2.add(lblNewLabel_16);
			    
			    JComboBox comboBox_2 = new JComboBox();
			    comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Search by :", "Name", "Swapper"}));
			    comboBox_2.setBounds(1258, 47, 86, 20);
			    panel_2.add(comboBox_2);
			    
			    textField_1 = new JTextField();
			    textField_1.addKeyListener(new KeyAdapter() {
			    	@Override
			    	public void keyReleased(KeyEvent e) {
			    		if(comboBox.getSelectedItem().toString().equals("Name"))
				   		{
				   			List<Request> requests=new ArrayList<Request>();
				   			requests=RequestDelegate.SearchByName(textField_1.getText());
				   			RequestTableModel1 modelRe = new RequestTableModel1(requests);
				   			table_2.setModel(modelRe);
				   			modelRe.fireTableDataChanged();
				   			
				   			
				   		}
				   		if(comboBox.getSelectedItem().toString().equals("Swapper"))
				   		{
				   			List<Request> requests=new ArrayList<Request>();
				   			requests=RequestDelegate.SearchBySwapper(textField_1.getText());
				   			RequestTableModel1 modelRe = new RequestTableModel1(requests);
				   			table_2.setModel(modelRe);
				   			modelRe.fireTableDataChanged();
				   			
					   		
				   			
				   		}
			    		
			    	}
			    });
			    textField_1.setBounds(1116, 47, 107, 20);
			    panel_2.add(textField_1);
			    textField_1.setColumns(10);
			    
			   
			   
			   lblOffersManagement = new JLabel("Offers Management");
			   lblOffersManagement.setHorizontalAlignment(SwingConstants.CENTER);
			   lblOffersManagement.setHorizontalTextPosition(SwingConstants.CENTER);
			   lblOffersManagement.setFont(new Font("Tahoma", Font.BOLD, 46));
			   lblOffersManagement.setBounds(378, 11, 548, 53);
			   contentPane.add(lblOffersManagement);
			   
			   btnNewButton_15 = new JButton("");
			   btnNewButton_15.addActionListener(new ActionListener() {
			   	public void actionPerformed(ActionEvent e) {
			   	}
			   });
			   btnNewButton_15.setBackground(Color.WHITE);
			   btnNewButton_15.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/logout13.png")));
			   btnNewButton_15.setBounds(1296, 22, 45, 35);
			   contentPane.add(btnNewButton_15);
			   
			   btnNewButton_16 = new JButton("");
			   btnNewButton_16.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/repair14.png")));
			   btnNewButton_16.setBounds(1244, 22, 45, 35);
			   contentPane.add(btnNewButton_16);
			   
			   lblNewLabel_6 = new JLabel("Rafik Riahi");
			   lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 12));
			   lblNewLabel_6.setBounds(1113, 43, 78, 14);
			   contentPane.add(lblNewLabel_6);
			   
			   btnNewButton = new JButton("");
			   btnNewButton.setBounds(1188, 22, 46, 35);
			   contentPane.add(btnNewButton);
			   btnNewButton.setIcon(new ImageIcon(Acceuil.class.getResource("/Ressources/ImgFromMaster/locked59.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int idR=table_2.getValueAt(table_2.getSelectedRow(), 0).hashCode();
				Offer offer=OfferDelegate.findOfferById(idR);
				offerLabel.setText(offer.getNameOffer());
				lblNewLabel_19.setText(offer.getSwapper().getFirstName());
				date.setText(offer.getDateOffer().toString());
				textArea.setText(offer.getDescriptionOffer());
				lblNewLabel_7.setText(offer.getInterestedBy());
				offer.getCategory().getNameCategory();
				offer.getsubcategory().getNameSubCat();
				
				int idR1=table_2.getValueAt(table_2.getSelectedRow(), 0).hashCode();
				Offer offer1=new Offer();
				offer1=OfferDelegate.findOfferById(idR1);
				int s1=offer1.getSwapper().getIdSwapperPk();
				Swapper swapper1=new Swapper();
				swapper1=SwapperDelegate.findSwapperById(s1);
				lblNewLabel_1.setText(swapper1.getFirstName());
				
				
			}
		});
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int	idOF=table.getValueAt(table.getSelectedRow(), 0).hashCode();
			Offer offerFok=OfferDelegate.findOfferById(idOF);
			
			
			OfferDelegate.removeOffer(offerFok);
			modelOffer=new OfferTableModel();
			table.setModel(modelOffer);
			modelOffer.fireTableDataChanged();
			
			
				
			}
		});
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		
		
		
		
		
		
		
		//pane=new JScrollPane(table);
	}
}
