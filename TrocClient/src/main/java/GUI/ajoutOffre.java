package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

import org.apache.commons.io.FileUtils;

import delegate.OfferDelegate;
import domain.Offer;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class ajoutOffre extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ajoutOffre frame = new ajoutOffre();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ajoutOffre() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 432, 253);
		contentPane.add(panel);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(121, 34, 116, 22);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(121, 75, 116, 22);
		panel.add(textField_1);
		textField_1.setColumns(10);
		final JFileChooser chooser = new JFileChooser();
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// intitulé du bouton
				chooser.setApproveButtonText("Choix du fichier");
				// affiche la boite de dialogue
				chooser.showOpenDialog(null);
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					textField.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnNewButton.setBounds(257, 33, 97, 25);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Offer o = new Offer();
				o= OfferDelegate.findOfferById(Integer.parseInt(textField_1.getText()));
				File f = chooser.getSelectedFile();
				try {

					o.setImage(FileUtils.readFileToByteArray(f));
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			OfferDelegate.updateOffer(o);
			}
			
		});
		btnNewButton_1.setBounds(289, 200, 97, 25);
		panel.add(btnNewButton_1);
	}
}
