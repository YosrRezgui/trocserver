package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.CategoryModel;
import model.SubCategoryModel;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import delegate.CategoryDelegate;
import delegate.SubCategoryDelegate;
import domain.Category;
import domain.SubCategory;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.border.LineBorder;

import com.alee.laf.WebLookAndFeel;

import java.awt.Color;
import java.awt.Frame;

import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.SystemColor;
import javax.swing.ImageIcon;

public class CategoryTest extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JPanel panel_1;
	private JTextField textField;
	private JTable table_1;
	private JButton btnDelete_1;
	private JPanel panel_2;
	private JTextField textField_1;
	private JLabel lblSubcategory;
	JLabel labCategory;
	JLabel labSub ;
	JLabel labOffer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install ();
					CategoryTest frame = new CategoryTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public int id1;
	private JButton btnAdd;
	private JButton btnAdd_1;
	private JPanel panel_4;
	private JButton button;
	private JButton button_1;
	private JButton button_2;
	private JButton button_3;
	private JButton button_4;
	private JButton button_5;
	private JButton button_6;
	private JButton button_7;
	private JButton button_8;
	private JButton button_9;
	private JButton button_10;

	/**
	 * Create the frame.
	 */
	public CategoryTest() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 669, 391);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setToolTipText("");
		panel.setBounds(0, 62, 1352, 536);
		contentPane.add(panel);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("");
		scrollPane.setBounds(115, 83, 391, 159);
		panel.add(scrollPane);
		
		table = new JTable();
		table.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				labCategory.setText("");
				labSub.setText("");
				labOffer.setText("");
			}
		});
		table.setModel(new CategoryModel());
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(e.getClickCount()!=0){
					int id = (int)table.getValueAt(table.getSelectedRow(),0);
					id1=id;
					
					table_1.setModel(new SubCategoryModel(id));
					table_1.repaint();
					Category cat=CategoryDelegate.findCategoryById(id);
					labCategory.setText(cat.getNameCategory());
					labSub.setText(""+cat.getSubCategories().size());
					labOffer.setText(""+cat.getOffers().size());
					
					
					
				}
			}
			
		});
		
		
		
		scrollPane.setViewportView(table);
		
		panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel_1.setBounds(840, 83, 352, 152);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(194, 11, 148, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setBounds(10, 14, 75, 14);
		panel_1.add(lblCategory);
		
		JButton btnDelete = new JButton("delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = (int)table.getValueAt(table.getSelectedRow(),0);
				CategoryDelegate.removeCategory(CategoryDelegate.findCategoryById(id));
				table.setModel(new CategoryModel());
				table_1.setModel(new SubCategoryModel(0));
				table_1.repaint();
				table.repaint();
				
				
			}
		});
		btnDelete.setBounds(417, 261, 89, 23);
		panel.add(btnDelete);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setToolTipText("");
		scrollPane_1.setBounds(115, 350, 391, 136);
		panel.add(scrollPane_1);
		
		table_1 = new JTable();
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Category cat=CategoryDelegate.findCategoryById(id1);
				labCategory.setText(cat.getNameCategory());
				labSub.setText(""+cat.getSubCategories().size());
				labOffer.setText(""+cat.getOffers().size());
			}
		});
		
		scrollPane_1.setViewportView(table_1);
		
		btnDelete_1 = new JButton("delete");
		btnDelete_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int id = (int)table_1.getValueAt(table_1.getSelectedRow(),0);
				SubCategory sub=SubCategoryDelegate.findSubCategoryById(id);
				sub.setCategory(null);
				
				SubCategoryDelegate.updateSubCategory(sub);
				SubCategoryDelegate.removeSubCategory(sub);
				table_1.setModel(new SubCategoryModel(id1));
				
				table_1.repaint();
				Category cat=CategoryDelegate.findCategoryById(id1);
				labCategory.setText(cat.getNameCategory());
				labSub.setText(""+cat.getSubCategories().size());
				labOffer.setText(""+cat.getOffers().size());
				
				
				
				
				
			}
		});
		btnDelete_1.setBounds(417, 497, 89, 23);
		panel.add(btnDelete_1);
		
		panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel_2.setBounds(840, 350, 352, 136);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(215, 11, 127, 20);
		panel_2.add(textField_1);
		textField_1.setColumns(10);
		
		lblSubcategory = new JLabel("Subcategory");
		lblSubcategory.setBounds(10, 14, 93, 14);
		panel_2.add(lblSubcategory);
		
		btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals("")){
					JOptionPane.showMessageDialog(null, "You must type a name");
				}
				else{
				Category c = new Category();
				c.setNameCategory(textField.getText());
				CategoryDelegate.addCategory(c);
				table.setModel(new CategoryModel());
				table_1.setModel(new SubCategoryModel(0));
				table.repaint();
				table_1.repaint();
				textField.setText("");
			}}
		});
		btnAdd.setBounds(1103, 246, 89, 23);
		panel.add(btnAdd);
		
		btnAdd_1 = new JButton("add");
		btnAdd_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField_1.getText().equals("")){
					JOptionPane.showMessageDialog(null, "You must type a name");
				}
				else{
				SubCategory sub = new SubCategory();
				sub.setNameSubCat(textField_1.getText());
				sub.setCategory(CategoryDelegate.findCategoryById(id1));
				SubCategoryDelegate.addSubCategory(sub);
				table_1.setModel(new SubCategoryModel(id1));
				table_1.repaint();
				textField_1.setText("");
				Category cat=CategoryDelegate.findCategoryById(id1);
				labCategory.setText(cat.getNameCategory());
				labSub.setText(""+cat.getSubCategories().size());
				labOffer.setText(""+cat.getOffers().size());
				
				
				}
				
			}
		});
		btnAdd_1.setBounds(1103, 497, 89, 23);
		panel.add(btnAdd_1);
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String id = (String)table.getValueAt(table.getSelectedRow(),1);
				Category c=CategoryDelegate.findCategoryById(id1);
				c.setNameCategory(id);
				CategoryDelegate.updateCategory(c);
				
				
			}
		});
		btnSave.setBounds(316, 261, 89, 23);
		panel.add(btnSave);
		
		JButton btnSave_1 = new JButton("save");
		btnSave_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = (int)table_1.getValueAt(table_1.getSelectedRow(),0);
				String name = (String)table_1.getValueAt(table_1.getSelectedRow(),1);
				SubCategory sub =SubCategoryDelegate.findSubCategoryById(id);
				sub.setNameSubCat(name);
				SubCategoryDelegate.updateSubCategory(sub);
				
				
				
			}
		});
		btnSave_1.setBounds(316, 497, 89, 23);
		panel.add(btnSave_1);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel_3.setBounds(556, 210, 249, 188);
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblCategory_1 = new JLabel("Category");
		lblCategory_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCategory_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblCategory_1.setBounds(0, 0, 95, 23);
		panel_3.add(lblCategory_1);
		
		labCategory = new JLabel("");
		labCategory.setForeground(Color.BLUE);
		labCategory.setFont(new Font("Tahoma", Font.BOLD, 13));
		labCategory.setHorizontalAlignment(SwingConstants.CENTER);
		labCategory.setBounds(102, 26, 95, 23);
		panel_3.add(labCategory);
		
		JLabel lblSubCategory = new JLabel("Sub Category");
		lblSubCategory.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubCategory.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSubCategory.setBounds(0, 55, 95, 23);
		panel_3.add(lblSubCategory);
		
		labSub = new JLabel("");
		labSub.setForeground(Color.BLUE);
		labSub.setHorizontalAlignment(SwingConstants.CENTER);
		labSub.setFont(new Font("Tahoma", Font.BOLD, 13));
		labSub.setBounds(102, 85, 95, 23);
		panel_3.add(labSub);
		
		JLabel lblOffers = new JLabel("Offers");
		lblOffers.setHorizontalAlignment(SwingConstants.CENTER);
		lblOffers.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblOffers.setBounds(0, 113, 95, 23);
		panel_3.add(lblOffers);
		
		 labOffer = new JLabel("");
		labOffer.setForeground(Color.BLUE);
		labOffer.setHorizontalAlignment(SwingConstants.CENTER);
		labOffer.setFont(new Font("Tahoma", Font.BOLD, 13));
		labOffer.setBounds(102, 140, 95, 23);
		panel_3.add(labOffer);
		
		panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBackground(SystemColor.menu);
		panel_4.setBounds(0, 598, 1354, 96);
		contentPane.add(panel_4);
		
		button = new JButton("");
		button.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setBounds(626, 11, 79, 74);
		panel_4.add(button);
		
		button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		button_1.setBounds(750, 26, 70, 59);
		panel_4.add(button_1);
		
		button_2 = new JButton("");
		button_2.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		button_2.setBounds(870, 26, 70, 59);
		panel_4.add(button_2);
		
		button_3 = new JButton("");
		button_3.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		button_3.setBounds(991, 26, 70, 59);
		panel_4.add(button_3);
		
		button_4 = new JButton("");
		button_4.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		button_4.setBackground(Color.WHITE);
		button_4.setBounds(1120, 26, 70, 59);
		panel_4.add(button_4);
		
		button_5 = new JButton("");
		button_5.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/right208.png")));
		button_5.setBounds(1257, 33, 62, 52);
		panel_4.add(button_5);
		
		button_6 = new JButton("");
		button_6.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		button_6.setBounds(497, 26, 70, 59);
		panel_4.add(button_6);
		
		button_7 = new JButton("");
		button_7.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		button_7.setBounds(372, 26, 70, 59);
		panel_4.add(button_7);
		
		button_8 = new JButton("");
		button_8.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		button_8.setBounds(257, 26, 70, 59);
		panel_4.add(button_8);
		
		button_9 = new JButton("");
		button_9.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		button_9.setBounds(118, 26, 70, 59);
		panel_4.add(button_9);
		
		button_10 = new JButton("");
		button_10.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/left224.png")));
		button_10.setBorder(null);
		button_10.setBounds(10, 33, 62, 52);
		panel_4.add(button_10);
		
		JLabel labelTitre = new JLabel("Category Management");
		labelTitre.setHorizontalTextPosition(SwingConstants.CENTER);
		labelTitre.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitre.setFont(new Font("Tahoma", Font.BOLD, 46));
		labelTitre.setBounds(378, 11, 548, 53);
		contentPane.add(labelTitre);
		
		JButton button_11 = new JButton("");
		button_11.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/logout13.png")));
		button_11.setBackground(Color.WHITE);
		button_11.setBounds(1307, 26, 45, 35);
		contentPane.add(button_11);
		
		JButton button_12 = new JButton("");
		button_12.setIcon(new ImageIcon(CategoryTest.class.getResource("/Ressources/ImgFromMaster/repair14.png")));
		button_12.setBounds(1255, 26, 45, 35);
		contentPane.add(button_12);
		
	}
}
