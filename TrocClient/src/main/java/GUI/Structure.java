package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Admin.InformationSecurity;
import Admin.PersonalInformation;
import Admin.statistiques;

import com.alee.laf.WebLookAndFeel;
import com.alee.utils.TextUtils;

import delegate.OfferDelegate;
import delegate.RequestDelegate;
import delegate.SwapperDelegate;
import domain.Offer;
import domain.Request;
import domain.Swapper;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.JScrollPane;

import model.OfferModelfiltre;
import model.OfferTableModel;
import model.OfferTableRech;
import model.RequestTableModel;

import java.awt.Frame;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JList;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.w3c.dom.ls.LSInput;

import java.awt.Component;
import java.awt.ComponentOrientation;

import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.JDesktopPane;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JSeparator;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import java.awt.Font;

import javax.swing.border.MatteBorder;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.Dimension;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JTextArea;

public class Structure extends JFrame  {

	private JPanel contentPane;
	private OfferTableModel modelOffer;
	private RequestTableModel modelRequest;
	private JTable table_1;
	private JLabel lblOffersManagement;
	JCheckBox jCheckBox;
	List<Offer> allOffers;
	JOptionPane op;
	private JTextField textField_2;
	Date date1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					Structure frame = new Structure();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	
	public Structure() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		
		setBackground(Color.WHITE);
		setTitle("Offers Management");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 769, 597);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setBackground(SystemColor.text);
		setContentPane(contentPane);
		//allOffers=OfferDelegate.displayAllOffers();
		modelOffer=new OfferTableModel();
			   contentPane.setLayout(null);
			    
			   
			   
			   lblOffersManagement = new JLabel("Tn-Troc Administration");
			   lblOffersManagement.setBounds(397, 11, 548, 53);
			   lblOffersManagement.setHorizontalAlignment(SwingConstants.CENTER);
			   lblOffersManagement.setHorizontalTextPosition(SwingConstants.CENTER);
			   lblOffersManagement.setFont(new Font("Tahoma", Font.BOLD, 46));
			   contentPane.add(lblOffersManagement);
			   
			   JButton btnNewButton_8 = new JButton("");
			   btnNewButton_8.addActionListener(new ActionListener() {
			   	public void actionPerformed(ActionEvent e) {
			   		statistiques frame = new statistiques();
					frame.setVisible(true);
			   	}
			   });
			   btnNewButton_8.setBounds(446, 490, 149, 133);
			   contentPane.add(btnNewButton_8);
			   btnNewButton_8.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\business155.png"));
			   
			   JButton btnNewButton_11 = new JButton("");
			   btnNewButton_11.setBounds(1036, 490, 174, 124);
			   contentPane.add(btnNewButton_11);
			   btnNewButton_11.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\calendar146 (1).png"));
			   
			   JButton btnNewButton_6 = new JButton("");
			   btnNewButton_6.setBounds(141, 490, 149, 124);
			   contentPane.add(btnNewButton_6);
			   btnNewButton_6.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\directory (1).png"));
			   
			   JButton btnNewButton_4 = new JButton("");
			   btnNewButton_4.setBounds(732, 315, 161, 124);
			   contentPane.add(btnNewButton_4);
			   btnNewButton_4.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\users6 (2).png"));
			   btnNewButton_4.setBackground(Color.WHITE);
			   
			   JButton btnNewButton_10 = new JButton("");
			   btnNewButton_10.setBounds(141, 315, 149, 113);
			   contentPane.add(btnNewButton_10);
			   btnNewButton_10.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\video189 (1).png"));
			   
			   JButton btnNewButton_9 = new JButton("");
			   btnNewButton_9.setBounds(732, 490, 161, 124);
			   contentPane.add(btnNewButton_9);
			   btnNewButton_9.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\sim2 (1).png"));
			   
			   JButton btnNewButton_7 = new JButton("");
			   btnNewButton_7.setBounds(1036, 319, 174, 124);
			   contentPane.add(btnNewButton_7);
			   btnNewButton_7.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\present3 (1).png"));
			   
			   JButton btnNewButton_5 = new JButton("");
			   btnNewButton_5.setBounds(446, 315, 149, 113);
			   contentPane.add(btnNewButton_5);
			   btnNewButton_5.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\agreement2.png"));
			   
			   JLabel lblNewLabel = new JLabel("");
			   lblNewLabel.setBounds(441, 42, 472, 274);
			   lblNewLabel.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\troc-cuturel-télérama.jpg"));
			   contentPane.add(lblNewLabel);
			   
			   JButton button = new JButton("");
			   button.addActionListener(new ActionListener() {
			   	public void actionPerformed(ActionEvent e) {
			   		PersonalInformation frame = new PersonalInformation();
					frame.setVisible(true);
			   	}
			   });
			   button.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\repair14.png"));
			   button.setBounds(1255, 11, 45, 35);
			   contentPane.add(button);
			   
			   JButton button_1 = new JButton("");
			   button_1.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\logout13.png"));
			   button_1.setBackground(Color.WHITE);
			   button_1.setBounds(1307, 11, 45, 35);
			   contentPane.add(button_1);
			   
			   JLabel label = new JLabel("Rafik Riahi");
			   label.setFont(new Font("Tahoma", Font.BOLD, 12));
			   label.setBounds(1121, 32, 78, 14);
			   contentPane.add(label);
			   
			   JButton btnNewButton = new JButton("");
			   btnNewButton.addActionListener(new ActionListener() {
			   	public void actionPerformed(ActionEvent e) {
			   		InformationSecurity frame = new InformationSecurity();
					frame.setVisible(true);
			   	}
			   });
			   btnNewButton.setIcon(new ImageIcon("C:\\Users\\rafik\\Downloads\\locked59.png"));
			   btnNewButton.setBounds(1199, 11, 45, 35);
			   contentPane.add(btnNewButton);
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		
		
		
		
		
		
		
		//pane=new JScrollPane(table);
	}
}
