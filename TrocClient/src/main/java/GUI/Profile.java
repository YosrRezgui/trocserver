package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import domain.Exchange;
import javax.swing.JLabel;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JComboBox;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Profile extends JFrame {

	private JPanel contentPane;
	Exchange ex1;
	JLabel lblNewLabel;
	JLabel lblNewLabel_1;
	JLabel lblNewLabel_2;
	JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;
	private JLabel lblNewLabel_6;
	private JLabel lblNewLabel_7;
	private JLabel lblNewLabel_8;
	private JLabel lblNewLabel_9;
	private JLabel lblNewLabel_10;
	private JLabel lblNewLabel_11;
	private JLabel lblNewLabel_12;
	private JLabel lblNewLabel_13;
	private JLabel lblNewLabel_14;
	private JLabel lblNewLabel_15;
	private JLabel lblNewLabel_16;
	private JLabel lblNewLabel_17;
	private JLabel lblNewLabel_18;
	private JLabel lblNewLabel_19;
	private JLabel lblExchangeDetail;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Profile frame = new Profile();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
public Profile() {
		
		
		
	}
	public Profile(Exchange exchange) {
		this.ex1=exchange;
		
		setUndecorated(true);
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1005, 477);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new SoftBevelBorder(BevelBorder.RAISED, SystemColor.control, SystemColor.control, SystemColor.control, SystemColor.control));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		 lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(187, 144, 158, 14);
		contentPane.add(lblNewLabel);
		
		 lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(187, 297, 165, 14);
		contentPane.add(lblNewLabel_1);
		
		 lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setBounds(780, 144, 201, 14);
		contentPane.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setBounds(780, 297, 189, 14);
		contentPane.add(lblNewLabel_3);
		lblNewLabel_4 = new JLabel("New label");
		lblNewLabel_4.setBounds(187, 188, 100, 14);
		contentPane.add(lblNewLabel_4);
		
		lblNewLabel_5 = new JLabel("New label");
		lblNewLabel_5.setBounds(187, 242, 100, 14);
		contentPane.add(lblNewLabel_5);
		
		lblNewLabel_6 = new JLabel("New label");
		lblNewLabel_6.setBounds(780, 188, 226, 14);
		contentPane.add(lblNewLabel_6);
		
		lblNewLabel_7 = new JLabel("New label");
		lblNewLabel_7.setBounds(780, 242, 189, 14);
		contentPane.add(lblNewLabel_7);
		
		lblNewLabel_8 = new JLabel("New label");
		lblNewLabel_8.setBounds(187, 360, 100, 14);
		contentPane.add(lblNewLabel_8);
		
		lblNewLabel_9 = new JLabel("New label");
		lblNewLabel_9.setBounds(187, 416, 82, 14);
		contentPane.add(lblNewLabel_9);
		
		lblNewLabel_10 = new JLabel("New label");
		lblNewLabel_10.setBounds(780, 360, 296, 14);
		contentPane.add(lblNewLabel_10);
		
		lblNewLabel_11 = new JLabel("New label");
		lblNewLabel_11.setBounds(780, 416, 149, 14);
		contentPane.add(lblNewLabel_11);
		lblNewLabel.setText(ex1.getRequest().getOffer1().getSwapper().getFirstName());
		lblNewLabel_1.setText(ex1.getRequest().getOffer2().getSwapper().getFirstName());
		lblNewLabel_2.setText(ex1.getRequest().getOffer1().getNameOffer());
		lblNewLabel_3.setText(ex1.getRequest().getOffer2().getNameOffer());
		lblNewLabel_4.setText(ex1.getRequest().getOffer1().getSwapper().getSecondName());
		lblNewLabel_5.setText(ex1.getRequest().getOffer1().getSwapper().getAddress());
		lblNewLabel_6.setText(ex1.getRequest().getOffer2().getSwapper().getSecondName());
	    lblNewLabel_7.setText(ex1.getRequest().getOffer2().getSwapper().getAddress());
		lblNewLabel_8.setText(ex1.getRequest().getOffer1().getDateOffer().toString());
		lblNewLabel_9.setText(ex1.getRequest().getOffer1().getCategory().getNameCategory());
		lblNewLabel_10.setText(ex1.getRequest().getOffer2().getDateOffer().toString());
		lblNewLabel_11.setText(ex1.getRequest().getOffer2().getCategory().getNameCategory());
		
		lblNewLabel_12 = new JLabel("First Name");
		lblNewLabel_12.setBounds(52, 144, 100, 14);
		contentPane.add(lblNewLabel_12);
		
		lblNewLabel_13 = new JLabel("Second Name");
		lblNewLabel_13.setBounds(52, 188, 100, 14);
		contentPane.add(lblNewLabel_13);
		
		lblNewLabel_14 = new JLabel("Adress");
		lblNewLabel_14.setBounds(52, 242, 87, 14);
		contentPane.add(lblNewLabel_14);
		
		lblNewLabel_15 = new JLabel("Name Offer");
		lblNewLabel_15.setBounds(52, 297, 87, 14);
		contentPane.add(lblNewLabel_15);
		
		lblNewLabel_16 = new JLabel("Date");
		lblNewLabel_16.setBounds(52, 360, 100, 14);
		contentPane.add(lblNewLabel_16);
		
		lblNewLabel_17 = new JLabel("Category");
		lblNewLabel_17.setBounds(52, 416, 87, 14);
		contentPane.add(lblNewLabel_17);
		
		lblNewLabel_18 = new JLabel("First Name");
		lblNewLabel_18.setBounds(602, 144, 82, 14);
		contentPane.add(lblNewLabel_18);
		
		lblNewLabel_19 = new JLabel("Second Name");
		lblNewLabel_19.setBounds(602, 188, 82, 14);
		contentPane.add(lblNewLabel_19);
		
		JLabel lblNewLabel_20 = new JLabel("Adress");
		lblNewLabel_20.setBounds(602, 242, 82, 14);
		contentPane.add(lblNewLabel_20);
		
		JLabel lblNewLabel_21 = new JLabel("Name Offer");
		lblNewLabel_21.setBounds(602, 297, 100, 14);
		contentPane.add(lblNewLabel_21);
		
		JLabel lblNewLabel_22 = new JLabel("Date");
		lblNewLabel_22.setBounds(602, 360, 100, 14);
		contentPane.add(lblNewLabel_22);
		
		JLabel lblNewLabel_23 = new JLabel("Category");
		lblNewLabel_23.setBounds(602, 416, 82, 14);
		contentPane.add(lblNewLabel_23);
		
		lblExchangeDetail = new JLabel("Exchange Detail");
		lblExchangeDetail.setBackground(Color.WHITE);
		lblExchangeDetail.setFont(new Font("Tahoma", Font.BOLD, 36));
		lblExchangeDetail.setBounds(353, 21, 326, 44);
		contentPane.add(lblExchangeDetail);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnNewButton.setIcon(new ImageIcon(Profile.class.getResource("/Ressources/ImgFromMaster/cross106 (1).png")));
		btnNewButton.setBounds(971, 0, 34, 23);
		contentPane.add(btnNewButton);
		
		
		
	
	}
}
