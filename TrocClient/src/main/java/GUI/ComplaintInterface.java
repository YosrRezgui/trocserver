package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import model.ComplaintModel;
import delegate.ComplaintDelegate;
import delegate.OfferDelegate;
import delegate.SwapperDelegate;
import domain.Complaint;
import domain.Offer;
import domain.Swapper;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;

public class ComplaintInterface extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private Complaint c= new Complaint();
	private  Swapper s1 = new Swapper();
     
	private  Offer o = new Offer();
    
	private  Swapper s2 = new Swapper();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComplaintInterface frame = new ComplaintInterface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComplaintInterface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1455, 978);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(5, 5, 1904, 998);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new TitledBorder(null, "Complaints", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(56, 155, 807, 212);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBounds(23, 57, 754, 135);
		panel_1.add(scrollPane);
		
		
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Complaint details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(890, 166, 412, 189);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblBy = new JLabel("Subject:");
		lblBy.setBounds(12, 54, 56, 16);
		panel_2.add(lblBy);
		
		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(12, 102, 116, 16);
		panel_2.add(lblDescription);
		
		final JTextPane SubjectText = new JTextPane();
		SubjectText.setBounds(90, 54, 225, 22);
		panel_2.add(SubjectText);
		
		final JTextPane textPane = new JTextPane();
		textPane.setBounds(100, 98, 233, 78);
		panel_2.add(textPane);
		
		final JPanel panel_3 = new JPanel();
		panel_3.setVisible(false);
		panel_3.setBorder(new TitledBorder(null, "Caused by this offer", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBackground(Color.WHITE);
		panel_3.setBounds(558, 406, 395, 243);
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(12, 65, 56, 16);
		panel_3.add(lblName);
		
		JLabel lblDescription_1 = new JLabel("Description:");
		lblDescription_1.setBounds(12, 138, 80, 16);
		panel_3.add(lblDescription_1);
		
		final JTextPane textPane_1 = new JTextPane();
		textPane_1.setBounds(104, 138, 261, 42);
		panel_3.add(textPane_1);
		
		final JLabel label_4 = new JLabel("");
		label_4.setBounds(117, 65, 172, 16);
		panel_3.add(label_4);
		
		final JPanel panel_4 = new JPanel();
		panel_4.setVisible(false);
		panel_4.setBorder(new TitledBorder(null, "Caused by this swapper ", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBackground(Color.WHITE);
		panel_4.setBounds(990, 406, 335, 243);
		panel.add(panel_4);
		panel_4.setLayout(null);
		
	
		
		JLabel lblFirstName_1 = new JLabel("First name:");
		lblFirstName_1.setBounds(12, 40, 86, 16);
		panel_4.add(lblFirstName_1);
		
		JLabel lblSecondName_1 = new JLabel("Second name:");
		lblSecondName_1.setBounds(12, 87, 97, 16);
		panel_4.add(lblSecondName_1);
		
		JLabel lblNic_1 = new JLabel("NIC:");
		lblNic_1.setBounds(12, 131, 56, 16);
		panel_4.add(lblNic_1);
		
		JLabel lblMail_1 = new JLabel("Mail:");
		lblMail_1.setBounds(12, 180, 56, 16);
		panel_4.add(lblMail_1);
		
		final JLabel firstName = new JLabel("");
		firstName.setBounds(147, 40, 152, 16);
		panel_4.add(firstName);
		
		final JLabel SecondName = new JLabel("");
		SecondName.setBounds(149, 87, 152, 16);
		panel_4.add(SecondName);
		
		final JLabel NIC = new JLabel("");
		NIC.setBounds(149, 131, 152, 16);
		panel_4.add(NIC);
		
		final JLabel Mail = new JLabel("");
		Mail.setBounds(149, 180, 152, 16);
		panel_4.add(Mail);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setBorder(new TitledBorder(null, "The sender ", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBounds(47, 406, 431, 227);
		panel.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblFirstName = new JLabel("First name:");
		lblFirstName.setBounds(12, 50, 83, 16);
		panel_5.add(lblFirstName);
		
		JLabel lblSecondName = new JLabel("Second name:");
		lblSecondName.setBounds(12, 94, 83, 16);
		panel_5.add(lblSecondName);
		
		JLabel lblNic = new JLabel("NIC:");
		lblNic.setBounds(12, 141, 56, 16);
		panel_5.add(lblNic);
		
		JLabel lblMail = new JLabel("Mail:");
		lblMail.setBounds(12, 193, 56, 16);
		panel_5.add(lblMail);
		
		final JLabel label = new JLabel("");
		label.setBounds(151, 50, 202, 16);
		panel_5.add(label);
		
		final JLabel label_1 = new JLabel("");
		label_1.setBounds(159, 141, 194, 16);
		panel_5.add(label_1);
		
		final JLabel label_2 = new JLabel("");
		label_2.setBounds(159, 193, 194, 16);
		panel_5.add(label_2);
		
		final JLabel label_3 = new JLabel("");
		label_3.setBounds(151, 94, 202, 16);
		panel_5.add(label_3);
		final JButton btnBlock = new JButton("Block");
		btnBlock.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/Actions-dialog-cancel-icon.png")));
		final JButton btnRemove = new JButton("Remove");
		btnRemove.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnBlock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 label_4.setText(null);
	              	textPane_1.setText(null);
	                  	firstName.setText(null);
	                  	SecondName.setText(null);
	                  	NIC.setText(null);
	                  	Mail.setText(null);
	                  	btnBlock.setVisible(false);
	                  	btnRemove.setVisible(false);
	                  	panel_3.setVisible(false);
	                  	panel_4.setVisible(false);
				if(s1.equals(null))
					{ComplaintDelegate.removeComplaint(c);
					SwapperDelegate.blockSwapper(s1);
				table.setModel(new ComplaintModel());
					}
				else
				{ComplaintDelegate.removeComplaint(c);
					SwapperDelegate.blockSwapper(s2);
				table.setModel(new ComplaintModel());
				}
			}
		});
		btnBlock.setBounds(202, 205, 97, 25);
		panel_4.add(btnBlock);
		
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 label_4.setText(null);
              	textPane_1.setText(null);
                  	firstName.setText(null);
                  	SecondName.setText(null);
                  	NIC.setText(null);
                  	Mail.setText(null);
                  	btnBlock.setVisible(false);
                  	btnRemove.setVisible(false);
                  	panel_3.setVisible(false);
                  	panel_4.setVisible(false);
				ComplaintDelegate.removeComplaint(c);
				OfferDelegate.removeOffer(o);
				table.setModel(new ComplaintModel());
			}
		});
		btnRemove.setBounds(268, 193, 115, 25);
		panel_3.add(btnRemove);
		table = new JTable(new ComplaintModel());
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				label.setText(null);
				label_1.setText(null);
				label_2.setText(null);
                label_3.setText(null);
				 label_4.setText(null);
              	textPane_1.setText(null);
                  	firstName.setText(null);
                  	SecondName.setText(null);
                  	NIC.setText(null);
                  	Mail.setText(null);
                  	btnBlock.setVisible(false);
                  	btnRemove.setVisible(false);
                  	panel_3.setVisible(false);
                  	panel_4.setVisible(false);
				int ide = table.getValueAt(table.getSelectedRow(), 0).hashCode();
				
				c= ComplaintDelegate.findComplaintById(ide);
				SubjectText.setText(c.getSubject());
				textPane.setText(c.getContents());
				Swapper s = new Swapper();
				s= c.getSwapperAj();
				label.setText(s.getFirstName());
				label_1.setText(s.getSecondName());
				label_2.setText(String.valueOf(s.getNic()));
                label_3.setText(s.getMail());	
               
               
                if(c.getSwapperRe()==null && c.getOffer()!=null  )
                	
                {  o= c.getOffer();
                	s2= o.getSwapper();
                	label_4.setText(o.getNameOffer());
                	textPane_1.setText(o.getDescriptionOffer());
                	firstName.setText(s2.getFirstName());
                	SecondName.setText(s2.getSecondName());
                	NIC.setText(String.valueOf(s2.getNic()));
                	Mail.setText(s2.getMail());
                	btnBlock.setVisible(true);
                 	btnRemove.setVisible(true);
                 	panel_3.setVisible(true);
                  	panel_4.setVisible(true);
                }
                
 else if(c.getOffer()==null && c.getSwapperRe()!=null )
	
 {    s1=c.getSwapperRe();
	 label_4.setText(null);
	textPane_1.setText(null);
 	firstName.setText(s1.getFirstName());
 	SecondName.setText(s1.getSecondName());
 	NIC.setText(String.valueOf(s1.getNic()));
 	Mail.setText(s1.getMail());
 	btnBlock.setVisible(true);
  	btnRemove.setVisible(false);
  	panel_3.setVisible(false);
  	panel_4.setVisible(true);
 }
                else{
                
                	 label_4.setText(null);
                 	textPane_1.setText(null);
                     	firstName.setText(null);
                     	SecondName.setText(null);
                     	NIC.setText(null);
                     	Mail.setText(null);
                     	btnBlock.setVisible(false);
                     	btnRemove.setVisible(false);
                     	panel_3.setVisible(false);
                      	panel_4.setVisible(false);
                	
                }
               
              
                
			}
		});
		scrollPane.setViewportView(table);
		
		JButton btnTreat = new JButton("Treat");
		btnTreat.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/Eye-icon.png")));
		btnTreat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComplaintDelegate.removeComplaint(c);
				table.setModel(new ComplaintModel());
				panel_4.setVisible(false);
				panel_3.setVisible(false);
			}
		});
		btnTreat.setBounds(699, 380, 97, 25);
		panel.add(btnTreat);
		
		JLabel lblComplaintsManagement = new JLabel("Complaints management");
		lblComplaintsManagement.setFont(new Font("Tahoma", Font.BOLD, 35));
		lblComplaintsManagement.setBounds(582, 31, 479, 92);
		panel.add(lblComplaintsManagement);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBounds(-4, 690, 1428, 84);
		panel.add(panel_6);
		panel_6.setLayout(null);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/left224.png")));
		btnNewButton.setBounds(12, 13, 97, 58);
		panel_6.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		btnNewButton_1.setBounds(130, 13, 97, 58);
		panel_6.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		btnNewButton_2.setBounds(239, 13, 97, 58);
		panel_6.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		btnNewButton_3.setBounds(358, 13, 97, 58);
		panel_6.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		btnNewButton_4.setBounds(611, 0, 97, 84);
		panel_6.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		btnNewButton_5.setBounds(740, 13, 97, 58);
		panel_6.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		btnNewButton_6.setBounds(876, 13, 97, 58);
		panel_6.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		btnNewButton_7.setBounds(1033, 13, 97, 58);
		panel_6.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		btnNewButton_8.setBounds(1164, 13, 97, 58);
		panel_6.add(btnNewButton_8);
		
		JButton button = new JButton("");
		button.setBounds(1298, 13, 97, 58);
		panel_6.add(button);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(ComplaintInterface.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		button_1.setBounds(483, 13, 97, 58);
		panel_6.add(button_1);
	}
}
