package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Label;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Frame;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JTable;

import org.apache.commons.io.FileUtils;
import org.hibernate.event.spi.RefreshEvent;

import delegate.OfferDelegate;
import delegate.SwapperDelegate;
import domain.Offer;
import domain.Swapper;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ListSelectionModel;

import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class MediaInterface extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private int id=0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MediaInterface frame = new MediaInterface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the frame.
	 */
	public MediaInterface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1049, 619);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 1031, 566);
		contentPane.add(panel);
		panel.setLayout(null);
		
		final JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Images offers", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(12, 74, 1007, 479);
		panel.add(panel_1);
		panel_1.setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		//list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		scrollPane.setBounds(12, 43, 750, 397);
		panel_1.add(scrollPane, BorderLayout.WEST);
		
		JList list = new JList();
		list.setBounds(11, 42, 968, 395);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(list);
		
		JButton btnBlockSwapper = new JButton("Block swapper");
		btnBlockSwapper.setIcon(new ImageIcon(MediaInterface.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnBlockSwapper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (id!=0)
				{
				Offer of = new Offer();
				of = OfferDelegate.findOfferById(id);
				of.setImage(null);
				Swapper s = new Swapper();
				int IDs= OfferDelegate.findOfferById(id).getSwapper().getIdSwapperPk();
				System.out.println(IDs);
				if(IDs!=0)
				{
				s= SwapperDelegate.findSwapperById(IDs);
		
				SwapperDelegate.blockSwapper(s);
				}
				OfferDelegate.removeOffer(of);
				}
				
				setVisible(false);
				 new MediaInterface().setVisible(true);
			}
		});
		btnBlockSwapper.setBounds(814, 225, 169, 36);
		panel_1.add(btnBlockSwapper);
		
		JButton btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnQuit.setIcon(new ImageIcon(MediaInterface.class.getResource("/Ressources/ImgFromMaster/Actions-dialog-cancel-icon.png")));
		btnQuit.setBounds(886, 441, 97, 25);
		panel_1.add(btnQuit);
		
		JLabel lblMedia = new JLabel("Media");
		lblMedia.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblMedia.setBounds(407, 28, 245, 33);
		panel.add(lblMedia);
		int x=2;
		int y= 2;
		List <Offer> o;
		o= OfferDelegate.displayAllOffers();
		int i=0;
		for (final Offer offer : o) {
			
				
			
			File file = new File(offer.getNameOffer());
			try {
				if (offer.getImage()!=null)
				{
				FileUtils.writeByteArrayToFile(file, offer.getImage());
				 BufferedImage image = ImageIO.read(file);
				 ImageIcon imageIcon = new ImageIcon(image);
				 //lblNewLabel.setIcon(imageIcon);
				 JButton Label = new JButton();
				 Label.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							id= offer.getIdOfferPk();
							System.out.println(id);
						}
					});
				 Label.setIcon(imageIcon);
				 Label.setBounds(x, y, 120, 120);
				 if(i<5)
				 {
				 x+=122;
				 i++;
				 }
				 else
				 {y+=122;
					 x=2;
					 i=0;
				 }
					Label.setName("label"+offer.getIdOfferPk());
				  list.add(Label);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
		
		
		
	}
}

	
