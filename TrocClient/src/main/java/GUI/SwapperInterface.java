package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import model.RechercheSwapperModel;
import model.SwapperAllModel;
import model.SwapperblockModel;
import model.SwappersPassifsModel;

import org.apache.commons.io.FileUtils;

import delegate.NoteDelegate;
import delegate.OfferDelegate;
import delegate.SwapperDelegate;
import domain.Note;
import domain.Swapper;

public class SwapperInterface extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFileChooser chooser = new JFileChooser();
	private JPanel contentPane;
	private SwapperAllModel modele;
	private SwapperblockModel modele1;
	private SwappersPassifsModel modele2;
	private JTextField firstname;
	private JTextField secondname;
	private JTextField nic;
	private JTextField address;
	private JTextField mail;
	private JTextField dateofbirth;
	private JTextField phoneNumber;
	private JTextField Login;
	private JPasswordField passwordField;
	private JTextField picture;
	private JTable tableswapperblocked;
	private JTable tableswappersnotverified;
	private JTable table;
	private JTable table_1;
	int ide;
	private JTextField txtSearch;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					JFrame frame = new SwapperInterface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SwapperInterface() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1490, 705);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(928, 76, 388, 364);
		panel_2.setBorder(new TitledBorder(null, "Add swapper",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBackground(Color.WHITE);
		panel.add(panel_2);
		panel_2.setLayout(null);
		JLabel lblName = new JLabel("First name:");
		lblName.setBounds(12, 39, 76, 16);
		panel_2.add(lblName);

		JLabel lblNewLabel = new JLabel("Second name:");
		lblNewLabel.setBounds(12, 70, 88, 16);
		panel_2.add(lblNewLabel);

		JLabel lblNic = new JLabel("NIC:");
		lblNic.setBounds(12, 99, 56, 16);
		panel_2.add(lblNic);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setBounds(12, 128, 56, 16);
		panel_2.add(lblAddress);

		JLabel lblMail = new JLabel("Mail:");
		lblMail.setBounds(12, 168, 56, 16);
		panel_2.add(lblMail);

		JLabel lblPhoneNumber = new JLabel("Phone number:");
		lblPhoneNumber.setBounds(0, 205, 88, 16);
		panel_2.add(lblPhoneNumber);

		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setBounds(12, 251, 56, 16);
		panel_2.add(lblLogin);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(12, 296, 76, 16);
		panel_2.add(lblPassword);

		JLabel lblPicture = new JLabel("Picture:");
		lblPicture.setBounds(12, 332, 56, 16);
		panel_2.add(lblPicture);

		firstname = new JTextField();
		firstname.setBounds(100, 36, 116, 22);
		panel_2.add(firstname);
		firstname.setColumns(10);

		secondname = new JTextField();
		secondname.setBounds(100, 67, 116, 22);
		panel_2.add(secondname);
		secondname.setColumns(10);

		nic = new JTextField();
		nic.setBounds(100, 99, 116, 22);
		panel_2.add(nic);
		nic.setColumns(10);

		address = new JTextField();
		address.setBounds(100, 128, 116, 22);
		panel_2.add(address);
		address.setColumns(10);

		mail = new JTextField();
		mail.setBounds(100, 165, 116, 22);
		panel_2.add(mail);
		mail.setColumns(10);
		/*
		 * JCalendar j =new JCalendar(); j.setBounds(100, 224, 205, 125);
		 * j.getYearChooser().getSpinner().setBounds(0, 0, 7, 22);
		 * panel_2.add(j);
		 * 
		 * j.getYearChooser().setLayout(null); //panel_2.add(dateofbirth);
		 * //j.setColumns(10);
		 */

		phoneNumber = new JTextField();
		phoneNumber.setBounds(100, 202, 116, 22);
		panel_2.add(phoneNumber);
		phoneNumber.setColumns(10);

		Login = new JTextField();
		Login.setBounds(100, 248, 116, 22);
		panel_2.add(Login);
		Login.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(100, 293, 116, 22);
		panel_2.add(passwordField);

		picture = new JTextField();
		picture.setBounds(100, 329, 116, 22);
		panel_2.add(picture);
		picture.setColumns(10);

		JButton btnP = new JButton("");
		btnP.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/search-icon.png")));
		btnP.setBounds(241, 328, 76, 25);
		btnP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// intitulé du bouton
				chooser.setApproveButtonText("Choix du fichier");
				// affiche la boite de dialogue
				chooser.showOpenDialog(null);
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					picture.setText(chooser.getSelectedFile().getAbsolutePath());
				}

			}

		});
		panel_2.add(btnP);

		JButton btnBlock = new JButton("Block");
		btnBlock.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnBlock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ide = table_1.getValueAt(table_1.getSelectedRow(), 0)
						.hashCode();
				Swapper S = SwapperDelegate.findSwapperById(ide);
				SwapperDelegate.blockSwapper(S);
				modele = new SwapperAllModel();
				modele1 = new SwapperblockModel();
				table_1.setModel(modele);
				tableswapperblocked.setModel(modele1);
				modele.fireTableDataChanged();
				modele1.fireTableDataChanged();

			}
		});
		btnBlock.setBounds(340, 295, 97, 25);
		panel.add(btnBlock);

		JLabel lblThereAre = new JLabel("There are ");
		lblThereAre.setBounds(1075, 23, 67, 16);
		panel.add(lblThereAre);
		final JLabel LabelVerification = new JLabel(
				String.valueOf(SwapperDelegate.nbrSwapperNotVerified()));
		LabelVerification.setForeground(Color.RED);
		LabelVerification.setBackground(Color.RED);
		LabelVerification.setBounds(1154, 23, 14, 16);
		panel.add(LabelVerification);

		JLabel lblNotVerifed = new JLabel("account not verifed");
		lblNotVerifed.setBounds(1168, 23, 135, 16);
		panel.add(lblNotVerifed);

		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBorder(new TitledBorder(null, "Swapper's profile",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(498, 76, 407, 610);
		panel.add(panel_3);
		panel_3.setLayout(null);

		JLabel lblFirstName = new JLabel("First name:");
		lblFirstName.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblFirstName.setBounds(30, 276, 90, 16);
		panel_3.add(lblFirstName);

		JLabel lblSecondName = new JLabel("Second name:");
		lblSecondName.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblSecondName.setBounds(27, 314, 110, 16);
		panel_3.add(lblSecondName);

		JLabel lblNic_1 = new JLabel("NIC:");
		lblNic_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNic_1.setBounds(30, 363, 56, 16);
		panel_3.add(lblNic_1);

		JLabel lblAddress_1 = new JLabel("Address:");
		lblAddress_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblAddress_1.setBounds(30, 392, 75, 16);
		panel_3.add(lblAddress_1);

		JLabel lblMail_1 = new JLabel("Mail:");
		lblMail_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblMail_1.setBounds(30, 435, 56, 16);
		panel_3.add(lblMail_1);

		JLabel lblPhoneNumber_1 = new JLabel("Phone number:");
		lblPhoneNumber_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblPhoneNumber_1.setBounds(27, 465, 110, 16);
		panel_3.add(lblPhoneNumber_1);
		final JLabel lblNote = new JLabel("Note:");
		lblNote.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNote.setBounds(30, 510, 56, 16);
		lblNote.setVisible(false);
		panel_3.add(lblNote);
		final JLabel lblNomber = new JLabel("Number of offers:");
		lblNomber.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblNomber.setBounds(24, 582, 124, 16);
		lblNomber.setVisible(false);
		panel_3.add(lblNomber);

		final JButton btnNewButton_2 = new JButton("details notes");
		btnNewButton_2.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/Eye-icon.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NoteInterface frame = new NoteInterface(ide);
				frame.setVisible(true);
			}
		});
		btnNewButton_2.setVisible(false);

		btnNewButton_2.setBounds(257, 549, 133, 25);
		panel_3.add(btnNewButton_2);

		final JLabel labelFirstName = new JLabel("");
		labelFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		labelFirstName.setBounds(167, 285, 92, 16);
		panel_3.add(labelFirstName);

		final JLabel LabelSecondName = new JLabel("");
		LabelSecondName.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		LabelSecondName.setBounds(167, 324, 92, 16);
		panel_3.add(LabelSecondName);

		final JLabel LabelNIC = new JLabel("");
		LabelNIC.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		LabelNIC.setBounds(169, 363, 90, 16);
		panel_3.add(LabelNIC);

		final JLabel LabelAddress = new JLabel("");
		LabelAddress.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		LabelAddress.setBounds(322, 387, 83, 16);
		panel_3.add(LabelAddress);

		final JLabel labelMail = new JLabel("");
		labelMail.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		labelMail.setBounds(322, 435, 92, 16);
		panel_3.add(labelMail);

		final JLabel labelPhone = new JLabel("");
		labelPhone.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		labelPhone.setBounds(322, 493, 92, 16);
		panel_3.add(labelPhone);

		final JLabel labelNote = new JLabel("");
		labelNote.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		labelNote.setBounds(102, 520, 227, 16);
		labelNote.setVisible(false);
		panel_3.add(labelNote);

		final JLabel labelOffers = new JLabel("");
		labelOffers.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		labelOffers.setBounds(178, 582, 212, 16);
		labelOffers.setVisible(false);
		panel_3.add(labelOffers);
		
		final JPanel panel_6 = new JPanel();
		panel_6.setBounds(32, 58, 170, 184);
		panel_3.add(panel_6);
		panel_6.setLayout(null);
		
		final JLabel label = new JLabel("");
		label.setBounds(12, 13, 146, 158);
		panel_6.add(label);

		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Blocked swappers ",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBackground(Color.WHITE);
		panel_4.setBounds(35, 340, 414, 269);
		panel.add(panel_4);
		panel_4.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBounds(27, 118, 353, 124);
		panel_4.add(scrollPane);
		modele1 = new SwapperblockModel();
		tableswapperblocked = new JTable(modele1);
		tableswapperblocked.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnNewButton_2.setVisible(true);
		
				lblNomber.setVisible(true);
				lblNote.setVisible(true);
				labelNote.setVisible(true);
				labelOffers.setVisible(true);
				ide = tableswapperblocked.getValueAt(
						tableswapperblocked.getSelectedRow(), 0).hashCode();
				Swapper swap = SwapperDelegate.findSwapperById(ide);
				labelFirstName.setText(swap.getFirstName());
				labelMail.setText(swap.getMail());
				labelPhone.setText(Integer.toString(swap.getPhoneNumber()));
				LabelNIC.setText(Integer.toString(swap.getNic()));
				LabelSecondName.setText(swap.getSecondName());
				labelMail.setText(swap.getMail());
				int off = OfferDelegate.nbrOfferOfSwapper(ide);
				if (off == 0) {

					labelOffers.setText("this swapper hasn't of offers");

				} else {

					labelOffers.setText(Integer.toString(off));
				}
				List<Note> notes = NoteDelegate.findNoteBySwapper(ide);
				if (notes.isEmpty()) {
					labelNote.setForeground(Color.black);
					labelNote.setText("this swapper wasn't noted");
				} else {
					float a = 0;
					int i = 0;
					for (Note note : notes) {

						a = note.getNote() + a;
						i++;
					}
					if (a / i >= 10) {
						labelNote.setForeground(Color.GREEN);
					} else {
						labelNote.setForeground(Color.RED);
					}
					labelNote.setText((Double.toString(a / i)));
				}

			}
		});
		scrollPane.setViewportView(tableswapperblocked);
		final JComboBox comboBox = new JComboBox();
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				if (comboBox.getSelectedIndex() == 1) {
					List<Swapper> s;
					s = SwapperDelegate.findSwapperBlockByFirstName(textField
							.getText());
					tableswapperblocked.setModel(new RechercheSwapperModel(s));

				} else if (comboBox.getSelectedIndex() == 2) {
					List<Swapper> s;
					s = SwapperDelegate.findSwapperBlockBySecondName(textField
							.getText());
					tableswapperblocked.setModel(new RechercheSwapperModel(s));

				} else if (comboBox.getSelectedIndex() == 3) {
					List<Swapper> s;
					s = SwapperDelegate.findSwapperBlockByMail(textField
							.getText());
					tableswapperblocked.setModel(new RechercheSwapperModel(s));

				}

				else {
					modele1 = new SwapperblockModel();
					tableswapperblocked.setModel(modele1);

				}
			}
		});

		textField.setBounds(51, 54, 141, 22);
		panel_4.add(textField);
		textField.setColumns(10);

		comboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textField.setText("");

				modele1 = new SwapperblockModel();
				tableswapperblocked.setModel(modele1);
			}
		});

		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Search:",
				"By First name", "By second name", "By mail" }));
		comboBox.setBounds(255, 54, 117, 22);
		panel_4.add(comboBox);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/search-icon.png")));
		btnNewButton.setBounds(204, 51, 38, 25);
		panel_4.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Unblock");
		btnNewButton_1.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/view24.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ide = tableswapperblocked.getValueAt(
						tableswapperblocked.getSelectedRow(), 0).hashCode();
				Swapper S = SwapperDelegate.findSwapperById(ide);
				SwapperDelegate.unblockSwapper(S);
				modele = new SwapperAllModel();
				modele1 = new SwapperblockModel();
				table_1.setModel(modele);
				tableswapperblocked.setModel(modele1);
				modele.fireTableDataChanged();
				modele1.fireTableDataChanged();
			}
		});
		btnNewButton_1.setBounds(340, 622, 97, 25);
		panel.add(btnNewButton_1);

		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(null, "Swappers not verified",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBackground(Color.WHITE);
		panel_5.setBounds(928, 492, 388, 117);
		panel.add(panel_5);
		panel_5.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBackground(Color.WHITE);
		scrollPane_1.setBounds(12, 23, 364, 66);
		panel_5.add(scrollPane_1);
		modele2 = new SwappersPassifsModel();
		tableswappersnotverified = new JTable(modele2);
		tableswappersnotverified.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnNewButton_2.setVisible(false);
	
				lblNomber.setVisible(false);
				lblNote.setVisible(false);
				labelNote.setVisible(false);
				labelOffers.setVisible(false);
				ide = tableswappersnotverified.getValueAt(
						tableswappersnotverified.getSelectedRow(), 0)
						.hashCode();
				Swapper swap = SwapperDelegate.findSwapperById(ide);
				labelFirstName.setText(swap.getFirstName());
				labelMail.setText(swap.getMail());
				labelPhone.setText(Integer.toString(swap.getPhoneNumber()));
				LabelNIC.setText(Integer.toString(swap.getNic()));
				LabelSecondName.setText(swap.getSecondName());
				labelMail.setText(swap.getMail());
			}
		});
		scrollPane_1.setViewportView(tableswappersnotverified);

		JPanel panel_1 = new JPanel();
		panel_1.setToolTipText("ss\r\nssss");
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new TitledBorder(null, "Swappers",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(23, 58, 414, 224);
		panel.add(panel_1);
		panel_1.setLayout(null);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(12, 118, 380, 87);
		panel_1.add(scrollPane_2);

		table_1 = new JTable(new SwapperAllModel());
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnNewButton_2.setVisible(true);
				
				lblNomber.setVisible(true);
				lblNote.setVisible(true);
				labelNote.setVisible(true);
				labelOffers.setVisible(true);
				ide = table_1.getValueAt(table_1.getSelectedRow(), 0)
						.hashCode();
				Swapper swap = SwapperDelegate.findSwapperById(ide);
				labelFirstName.setText(swap.getFirstName());
				labelMail.setText(swap.getMail());
				labelPhone.setText(Integer.toString(swap.getPhoneNumber()));
				LabelNIC.setText(Integer.toString(swap.getNic()));
				LabelSecondName.setText(swap.getSecondName());
				labelMail.setText(swap.getMail());
				byte[] b = swap.getImage();
				File file = new File(swap.getFirstName());
				try {
					FileUtils.writeByteArrayToFile(file, b);
					 BufferedImage image = ImageIO.read(file);
					 ImageIcon imageIcon = new ImageIcon(image);
					 label.setIcon(imageIcon);
					 
					
			
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			
				int off = OfferDelegate.nbrOfferOfSwapper(ide);
				if (off == 0) {

					labelOffers.setText("this swapper hasn't of offers");

				} else {

					labelOffers.setText(Integer.toString(off));
				}
				List<Note> notes = NoteDelegate.findNoteBySwapper(ide);
				if (notes.isEmpty()) {
					labelNote.setForeground(Color.black);
					labelNote.setText("this swapper wasn't noted");
				} else {
					float a = 0;
					int i = 0;
					for (Note note : notes) {

						a = note.getNote() + a;
						i++;
					}
					if (a / i >= 10) {
						labelNote.setForeground(Color.GREEN);
					} else {
						labelNote.setForeground(Color.RED);
					}
					labelNote.setText((Double.toString(a / i)));
				}

			}
		});
		scrollPane_2.setViewportView(table_1);
		final JComboBox comboBoxSearch = new JComboBox();
		comboBoxSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtSearch.setText("");

				modele = new SwapperAllModel();
				table_1.setModel(modele);

			}
		});
		comboBoxSearch.setModel(new DefaultComboBoxModel(new String[] {
				"Search:", "By First name", "By second name", "By mail" }));
		comboBoxSearch.setToolTipText("Search\r\n");
		comboBoxSearch.setBounds(257, 54, 116, 22);
		panel_1.add(comboBoxSearch);
		txtSearch = new JTextField();
		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				if (comboBoxSearch.getSelectedIndex() == 1) {
					List<Swapper> s;
					s = SwapperDelegate.findSwapperByFirstName(txtSearch
							.getText());
					table_1.setModel(new RechercheSwapperModel(s));

				} else if (comboBoxSearch.getSelectedIndex() == 2) {
					List<Swapper> s;
					s = SwapperDelegate.findSwapperBySecondName(txtSearch
							.getText());
					table_1.setModel(new RechercheSwapperModel(s));

				} else if (comboBoxSearch.getSelectedIndex() == 3) {
					List<Swapper> s;
					s = SwapperDelegate.findSwapperByMail(txtSearch.getText());
					table_1.setModel(new RechercheSwapperModel(s));

				}

				else {
					modele = new SwapperAllModel();
					table_1.setModel(modele);
				}

			}
		});
		txtSearch.setBounds(44, 54, 138, 22);
		panel_1.add(txtSearch);
		txtSearch.setColumns(10);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/search-icon.png")));
		button.setBounds(195, 54, 38, 22);
		panel_1.add(button);

		JButton btnValide = new JButton("Validate");
		btnValide.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/pencil-icon.png")));
		btnValide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ide = tableswappersnotverified.getValueAt(
						tableswappersnotverified.getSelectedRow(), 0)
						.hashCode();
				Swapper S = SwapperDelegate.findSwapperById(ide);
				SwapperDelegate.verificationSwapper(S);
				modele = new SwapperAllModel();
				modele2 = new SwappersPassifsModel();
				table_1.setModel(modele);
				tableswappersnotverified.setModel(modele2);
				modele.fireTableDataChanged();
				modele2.fireTableDataChanged();
				LabelVerification.setText(String.valueOf(SwapperDelegate
						.nbrSwapperNotVerified()));
			}
		});
		btnValide.setBounds(1062, 622, 119, 25);
		panel.add(btnValide);

		JButton btnRe = new JButton("Reject");
		btnRe.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnRe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ide = tableswappersnotverified.getValueAt(
						tableswappersnotverified.getSelectedRow(), 0)
						.hashCode();
				Swapper S = SwapperDelegate.findSwapperById(ide);
				SwapperDelegate.removeSwapper(S);
				modele = new SwapperAllModel();
				modele2 = new SwappersPassifsModel();
				table_1.setModel(modele);
				tableswappersnotverified.setModel(modele2);
				modele.fireTableDataChanged();
				modele2.fireTableDataChanged();
				LabelVerification.setText(String.valueOf(SwapperDelegate
						.nbrSwapperNotVerified()));
			}
		});
		btnRe.setBounds(1215, 622, 107, 25);
		panel.add(btnRe);

		JButton btnADD = new JButton("Add");
		btnADD.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/pencil-icon.png")));
		btnADD.setBounds(1226, 454, 97, 25);
		panel.add(btnADD);
		
		JLabel lblSwappersManagement = new JLabel("Swappers management");
		lblSwappersManagement.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblSwappersManagement.setBounds(549, 4, 356, 44);
		panel.add(lblSwappersManagement);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBounds(12, 699, 1321, 81);
		panel.add(panel_7);
		panel_7.setLayout(null);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/left224.png")));
		btnNewButton_3.setBounds(12, 13, 86, 55);
		panel_7.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		btnNewButton_4.setBounds(110, 13, 97, 55);
		panel_7.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		btnNewButton_5.setBounds(219, 13, 97, 55);
		panel_7.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		btnNewButton_6.setBounds(328, 13, 97, 55);
		panel_7.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		btnNewButton_7.setBounds(437, 13, 97, 55);
		panel_7.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		btnNewButton_8.setBounds(546, 0, 97, 81);
		panel_7.add(btnNewButton_8);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		button_1.setBounds(657, 13, 97, 55);
		panel_7.add(button_1);
		
		JButton btnNewButton_9 = new JButton("");
		btnNewButton_9.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		btnNewButton_9.setBounds(766, 13, 97, 55);
		panel_7.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("");
		btnNewButton_10.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		btnNewButton_10.setBounds(875, 13, 86, 55);
		panel_7.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_11.setIcon(new ImageIcon(SwapperInterface.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		btnNewButton_11.setBounds(990, 13, 97, 55);
		panel_7.add(btnNewButton_11);
		
		JButton button_2 = new JButton("");
		button_2.setBounds(1126, 13, 97, 55);
		panel_7.add(button_2);
		btnADD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Swapper s = new Swapper();
				s.setFirstName(firstname.getText());
				s.setSecondName(secondname.getText());
				s.setNic(Integer.parseInt(nic.getText()));
				// byte[] fileContent =
				// Files.readAllBytes(picture.getText().toPath());
				s.setVerification(true);
				s.setBlackList(false);
				s.setLogin(Login.getText());
				s.setPassword(passwordField.getText());
				s.setPhoneNumber(Integer.parseInt(phoneNumber.getText()));

				s.setAddress(address.getText());
				s.setMail(mail.getText());
				File f = chooser.getSelectedFile();
				try {

					s.setImage(FileUtils.readFileToByteArray(f));
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}

				SwapperDelegate.addSwapper(s);
				modele = new SwapperAllModel();
				modele = new SwapperAllModel();
				table_1.setModel(modele);
				modele.fireTableDataChanged();
				address.setText(null);
				firstname.setText(null);
				secondname.setText(null);
				mail.setText(null);
				nic.setText(null);
				phoneNumber.setText(null);

				phoneNumber.setText(null);
				passwordField.setText(null);
				Login.setText(null);
				picture.setText(null);

				// s.setDateOfBrith(dateofbirth.getText().);

			}
		});

	}
}
