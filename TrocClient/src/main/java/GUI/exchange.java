package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.alee.laf.WebLookAndFeel;
import com.alee.utils.TextUtils;

import delegate.ExchangeDelegate;
import delegate.OfferDelegate;
import delegate.RequestDelegate;
import delegate.SwapperDelegate;
import domain.Exchange;
import domain.Offer;
import domain.Request;
import domain.Swapper;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.JScrollPane;

import model.ExchangeModelTable;
import model.OfferTableModel;
import model.OfferTableRech;
import model.RequestTableModel;

import java.awt.Frame;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.w3c.dom.ls.LSInput;

import java.awt.Component;
import java.awt.ComponentOrientation;

import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.JDesktopPane;
import javax.swing.ImageIcon;
import javax.swing.JSplitPane;
import javax.swing.JSeparator;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import java.awt.Font;

import javax.swing.border.MatteBorder;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.Dimension;
import javax.swing.JProgressBar;

public class exchange extends JFrame  {

	private JPanel contentPane;
	private OfferTableModel modelOffer;
	private RequestTableModel modelRequest;
	private JTable table_1;
	private JButton btnNewButton_12;
	private JButton btnNewButton_13;
	private JButton btnNewButton_14;
	private JPanel panel_2;
	private JLabel lblOffersManagement;
	private JButton btnNewButton_15;
	private JButton btnNewButton_16;
	private JLabel lblNewLabel_6;
	JCheckBox jCheckBox;
	private JTable table;
	private JTable table_2;
	private JTable table_3;
	private ExchangeModelTable model1;
	private ExchangeModelTable model2;
	private ExchangeModelTable model3;
	List<Exchange>exchanges;
	List<Exchange>exchanges2;
	List<Exchange>exchanges3;
	private JProgressBar progressBar;
	private JProgressBar progressBar_1;
	private JProgressBar progressBar_2;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install();
					exchange frame = new exchange();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	
	public exchange() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		
		setBackground(Color.WHITE);
		setTitle("Offers Management");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		contentPane.setLayout(null);
	
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 598, 1354, 96);
		panel_1.setBackground(SystemColor.control);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		btnNewButton_12 = new JButton("");
		btnNewButton_12.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNewButton_12.setBounds(626, 11, 79, 74);
		panel_1.add(btnNewButton_12);
		btnNewButton_12.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setBounds(750, 26, 70, 59);
		panel_1.add(btnNewButton_8);
		btnNewButton_8.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		
		JButton btnNewButton_11 = new JButton("");
		btnNewButton_11.setBounds(870, 26, 70, 59);
		panel_1.add(btnNewButton_11);
		btnNewButton_11.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setBounds(991, 26, 70, 59);
		panel_1.add(btnNewButton_6);
		btnNewButton_6.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setBounds(1120, 26, 70, 59);
		panel_1.add(btnNewButton_4);
		btnNewButton_4.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		btnNewButton_4.setBackground(Color.WHITE);
		
		btnNewButton_14 = new JButton("");
		btnNewButton_14.setBounds(1257, 33, 62, 52);
		panel_1.add(btnNewButton_14);
		btnNewButton_14.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/right208.png")));
		
		JButton btnNewButton_9 = new JButton("");
		btnNewButton_9.setBounds(497, 26, 70, 59);
		panel_1.add(btnNewButton_9);
		btnNewButton_9.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		
		JButton btnNewButton_10 = new JButton("");
		btnNewButton_10.setBounds(372, 26, 70, 59);
		panel_1.add(btnNewButton_10);
		btnNewButton_10.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setBounds(257, 26, 70, 59);
		panel_1.add(btnNewButton_7);
		btnNewButton_7.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setBounds(118, 26, 70, 59);
		panel_1.add(btnNewButton_5);
		btnNewButton_5.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		
		btnNewButton_13 = new JButton("");
		btnNewButton_13.setBounds(10, 33, 62, 52);
		panel_1.add(btnNewButton_13);
		btnNewButton_13.setBorder(null);
		btnNewButton_13.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/left224.png")));
	
			   
			   lblOffersManagement = new JLabel("Exchanges Management");
			   lblOffersManagement.setBounds(377, 22, 583, 53);
			   lblOffersManagement.setHorizontalAlignment(SwingConstants.CENTER);
			   lblOffersManagement.setHorizontalTextPosition(SwingConstants.CENTER);
			   lblOffersManagement.setFont(new Font("Tahoma", Font.BOLD, 46));
			   contentPane.add(lblOffersManagement);
			   
			   JPanel panel = new JPanel();
			   panel.setBackground(Color.WHITE);
			   panel.setBounds(0, 89, 1372, 515);
			   contentPane.add(panel);
			   panel.setLayout(null);
			   
			   JScrollPane scrollPane = new JScrollPane();
			   scrollPane.setBounds(648, 78, 699, 159);
			   panel.add(scrollPane);
			   exchanges=ExchangeDelegate.findAllExchange();
			      model1=new ExchangeModelTable(exchanges);
			   table = new JTable(model1);
			   scrollPane.setViewportView(table);
			   
			   JScrollPane scrollPane_1 = new JScrollPane();
			   scrollPane_1.setBounds(10, 329, 466, 159);
			   panel.add(scrollPane_1);
			   exchanges2=ExchangeDelegate.findAllExchangeV();
			      model2=new ExchangeModelTable(exchanges2);
			   table_2 = new JTable(model2);
			   scrollPane_1.setViewportView(table_2);
			   
			   JScrollPane scrollPane_3 = new JScrollPane();
			   scrollPane_3.setBounds(758, 329, 553, 159);
			   panel.add(scrollPane_3);
			   exchanges3=ExchangeDelegate.findAllExchangeF();
			    model3=new ExchangeModelTable(exchanges3);
			   table_3 = new JTable(model3);
			   scrollPane_3.setViewportView(table_3);
			   
			   JButton btnRemove = new JButton("disable");
			   btnRemove.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/Actions-dialog-cancel-icon.png")));
			   btnRemove.addActionListener(new ActionListener() {
			   	public void actionPerformed(ActionEvent e) {
			   		
			   		Exchange ex =new Exchange();
			   		int ide=table.getValueAt(table.getSelectedRow(), 0).hashCode();
			   		System.out.println(ide);
			   	
			   		 ex=ExchangeDelegate.findEchangeById(ide);
			   		ex.setFraud(true);
			   		ExchangeDelegate.updateExchange(ex);
			   	List<Exchange>	exchangesU=ExchangeDelegate.findAllExchangeF();
			   		ExchangeModelTable modelU=new ExchangeModelTable(exchangesU);
			   		table_3.setModel(modelU);
			   		modelU.fireTableDataChanged();
			   		List<Exchange>	exchangesA=ExchangeDelegate.findAllExchangeV();
			   		ExchangeModelTable modelA=new ExchangeModelTable(exchangesA);
			   		table.setModel(modelA);
			   		modelA.fireTableDataChanged();
			   	 progressBar.setValue(table_3.getRowCount());
				 progressBar_2.setValue(table_2.getRowCount());
				 progressBar_1.setValue(table.getRowCount());
			   		
			   	}
			   });
			   btnRemove.setBounds(765, 248, 109, 31);
			   panel.add(btnRemove);
			   
			    progressBar = new JProgressBar();
			   progressBar.setStringPainted(true);
			   progressBar.setBackground(Color.BLUE);
			   progressBar.setBounds(273, 77, 146, 23);
			   panel.add(progressBar);
			   progressBar.setMaximum(table.getRowCount()+table_2.getRowCount()+table_3.getRowCount());
			   
			 progressBar.setValue(table_3.getRowCount());
			 
			 progressBar_1 = new JProgressBar();
			 progressBar_1.setStringPainted(true);
			 progressBar_1.setBounds(273, 140, 146, 23);
			 panel.add(progressBar_1);
			 progressBar_1.setMaximum(table.getRowCount()+table_2.getRowCount()+table_3.getRowCount());
			 progressBar_1.setValue(table.getRowCount());
			 
			 progressBar_2 = new JProgressBar();
			 progressBar_2.setStringPainted(true);
			 progressBar_2.setBounds(273, 213, 146, 23);
			 panel.add(progressBar_2);
			 progressBar_2.setMaximum(table.getRowCount()+table_2.getRowCount()+table_3.getRowCount());
			 progressBar_2.setValue(table_2.getRowCount());
			
			 JLabel lblNewLabel = new JLabel("New label");
			 lblNewLabel.setBounds(10, 216, 229, 14);
			 panel.add(lblNewLabel);
			 lblNewLabel.setText("Number of exchanges validate:0");
			 
			 JLabel lblNewLabel_1 = new JLabel("New label");
			 lblNewLabel_1.setBounds(10, 143, 229, 14);
			 panel.add(lblNewLabel_1);
			 lblNewLabel_1.setText("Number of exchanges failed :0");
			 
			 JLabel lblNewLabel_2 = new JLabel("New label");
			 lblNewLabel_2.setBounds(10, 80, 229, 14);
			 panel.add(lblNewLabel_2);
			 lblNewLabel_2.setText("Number des exchanges in progress :0");
			 
			 JButton btnView = new JButton("view");
			 btnView.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/Eye-icon.png")));
			 btnView.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		int ide=table.getValueAt(table.getSelectedRow(), 0).hashCode();
			 		Exchange exchange=ExchangeDelegate.findEchangeById(ide);
			 		Profile profile=new Profile(exchange);
			 		profile.setVisible(true);
			 		
			 		
			 		
			 		
			 	}
			 });
			 btnView.setBounds(648, 248, 99, 31);
			 panel.add(btnView);
			 
			 JLabel lblNewLabel_3 = new JLabel("Exchanges validate");
			 lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 20));
			 lblNewLabel_3.setBounds(104, 273, 199, 61);
			 panel.add(lblNewLabel_3);
			 
			 JLabel lblNewLabel_4 = new JLabel("Exchanges failed");
			 lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 20));
			 lblNewLabel_4.setBounds(922, 287, 180, 31);
			 panel.add(lblNewLabel_4);
			 
			 JLabel lblNewLabel_5 = new JLabel("Exchanges in progress");
			 lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 20));
			 lblNewLabel_5.setBounds(860, 26, 274, 41);
			 panel.add(lblNewLabel_5);
			 
			 JLabel label = new JLabel("Rafik Riahi");
			 label.setFont(new Font("Tahoma", Font.BOLD, 12));
			 label.setBounds(1126, 74, 78, 14);
			 contentPane.add(label);
			 
			 JButton button = new JButton("");
			 button.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/locked59.png")));
			 button.setBounds(1201, 53, 46, 35);
			 contentPane.add(button);
			 
			 JButton button_1 = new JButton("");
			 button_1.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/repair14.png")));
			 button_1.setBounds(1257, 53, 45, 35);
			 contentPane.add(button_1);
			 
			 JButton button_2 = new JButton("");
			 button_2.setIcon(new ImageIcon(exchange.class.getResource("/Ressources/ImgFromMaster/logout13.png")));
			 button_2.setBackground(Color.WHITE);
			 button_2.setBounds(1309, 53, 45, 35);
			 contentPane.add(button_2);
		
		
		
		
		
		
		
		//pane=new JScrollPane(table);
	}
}
