package GUI;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.TableColumn;

import model.CommentModel;
import model.EventModel;
import model.ParticipantModel;

import com.alee.laf.WebLookAndFeel;
import com.toedter.calendar.JDateChooser;

import delegate.CommentDelegate;
import delegate.EventDelegate;
import delegate.SwapperDelegate;
import domain.Comment;
import domain.Event;
import domain.Swapper;

import javax.swing.SwingConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.SystemColor;
import javax.swing.ImageIcon;

public class EventGUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JTextField textFieldName;
	private JTextField textFieldTheme;
	private JTextField textFieldPlace;
	private JDateChooser calendar;
	private JTextField txName;
	private JTextField txTheme;
	private JTextField txPlace;
	JLabel firstn = new JLabel("");
	JLabel lastn = new JLabel("");
	JLabel mailn = new JLabel("");
	JEditorPane contenttx = new JEditorPane();
	JLabel datetx = new JLabel("");
	JLabel participanttx = new JLabel("");
	JDateChooser dateChooser;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WebLookAndFeel.install ();
					EventGUI frame = new EventGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public Event ev;
	private JTextField txtName;
	private JTextField txtPla;
	/**
	 * Create the frame.
	 */
	public EventGUI() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 742, 476);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(0, 60, 1352, 548);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new LineBorder(Color.GRAY));
		panel_1.setBounds(65, 11, 360, 243);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 44, 304, 146);
		panel_1.add(scrollPane);
		
		final JEditorPane editorPane = new JEditorPane();
		editorPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		editorPane.setEnabled(false);
		editorPane.setBounds(119, 175, 225, 57);
		
		
		
		table = new JTable(new EventModel());
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()!=0){
					String name = (String)table.getValueAt(table.getSelectedRow(),0);
					ev= EventDelegate.findEventByName(name);
					txName.setEnabled(true);
					txTheme.setEnabled(true);
					txPlace.setEnabled(true);
					editorPane.setEnabled(true);
					dateChooser.setEnabled(true);
					txName.setEditable(true);
					txTheme.setEditable(true);
					txPlace.setEditable(true);
					editorPane.setEditable(true);
					txName.setText(ev.getNameEvent());
					txTheme.setText(ev.getThemeEvent());
					txPlace.setText(ev.getPlaceEvent());
					editorPane.setText(ev.getDescriptionEvent());
					dateChooser.setDate(ev.getDateEvent());
					
					
					
					
					table_1.setModel(new ParticipantModel(ev));
					table_1.repaint();
					table_2.setModel(new CommentModel(ev));
					TableColumn column1 = null;
					column1 =table_2.getColumnModel().getColumn(3);
					column1.setPreferredWidth(0);
					column1.setMinWidth(0);
					column1.setWidth(0);
					column1.setMaxWidth(0);
					table_2.repaint();
					
					participanttx.setText("");
					datetx.setText("");
					contenttx.setText("");
					firstn.setText("");
					lastn.setText("");
					mailn.setText("");
					
					
					
					
					
					
				
					
					
					
				}
			}
		});
		
		scrollPane.setViewportView(table);
		
		JButton btnDelete = new JButton("delete");
		btnDelete.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EventDelegate.removeEvent(ev);
				table.setModel(new EventModel());
				table.repaint();
				txName.setText("");
				txTheme.setText("");
				txPlace.setText("");
				editorPane.setText("");
				
			}
		});
		btnDelete.setBounds(220, 206, 105, 23);
		panel_1.add(btnDelete);
		
		txtName = new JTextField();
		txtName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String name=txtName.getText();
				List<Event> eve=EventDelegate.findEventsByName(name);
				table.setModel(new EventModel(eve));
				table.repaint();
				
				
				
			}
		});
		txtName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				txtName.setText("name");
				txtName.setFont(new Font("Sylfaen", Font.ITALIC, 13));
				txtName.setForeground(Color.LIGHT_GRAY);
			}
			@Override
			public void focusGained(FocusEvent e) {
				txtName.setText("");
				txtName.setForeground(Color.BLACK);
			}
		});
		txtName.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtName.setText("name");
		txtName.setBounds(59, 11, 105, 22);
		panel_1.add(txtName);
		txtName.setColumns(10);
		
		txtPla = new JTextField();
		
		
		txtPla.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				txtPla.setText("Place");
				txtPla.setFont(new Font("Sylfaen", Font.ITALIC, 13));
				txtPla.setForeground(Color.LIGHT_GRAY);
			}
			@Override
			public void focusGained(FocusEvent e) {
				txtPla.setText("");
				txtPla.setForeground(Color.BLACK);
			}
			
		});
		txtPla.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				List<Event> events=EventDelegate.findThemsByName(txtPla.getText());
				
				
				EventModel mod=new EventModel(events);
				table.setModel(mod);
				table.repaint();
			}
		});
		
		txtPla.setHorizontalAlignment(SwingConstants.CENTER);
		txtPla.setForeground(Color.LIGHT_GRAY);
		txtPla.setText("Place");
		txtPla.setFont(new Font("Sylfaen", Font.ITALIC, 13));
		txtPla.setColumns(10);
		txtPla.setBounds(174, 11, 105, 22);
		panel_1.add(txtPla);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBorder(new LineBorder(Color.GRAY));
		panel_2.setBounds(65, 265, 360, 118);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		scrollPane_1.setBounds(22, 11, 307, 93);
		panel_2.add(scrollPane_1);
		
		table_1 = new JTable();
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount()!=0){
					String fname = (String)table_1.getValueAt(table_1.getSelectedRow(),0);
					String lname = (String)table_1.getValueAt(table_1.getSelectedRow(),1);
					String mail = (String)table_1.getValueAt(table_1.getSelectedRow(),2);
					
					firstn.setText(fname);
					lastn.setText(lname);
					mailn.setText(mail);}
			}
		});
		
		scrollPane_1.setViewportView(table_1);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBorder(new LineBorder(Color.GRAY));
		panel_3.setBounds(65, 394, 360, 146);
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(23, 11, 309, 102);
		panel_3.add(scrollPane_2);
		
		table_2 = new JTable();
		table_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				if(e.getClickCount()!=0){
					String fname = (String)table_2.getValueAt(table_2.getSelectedRow(),0);
					Date lname = (Date)table_2.getValueAt(table_2.getSelectedRow(),2);
					String mail = (String)table_2.getValueAt(table_2.getSelectedRow(),1);
					DateFormat df = new SimpleDateFormat("yyy/MM/dd HH:mm:ss");
					String dt=df.format(lname);
					participanttx.setText(fname);
					datetx.setText(dt.substring(0, 10));
					contenttx.setText(mail);}
			}
				
				
			
		});
		
		
		scrollPane_2.setViewportView(table_2);
		
		JButton btnDelete_1 = new JButton("delete");
		btnDelete_1.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/Actions-edit-delete-icon.png")));
		btnDelete_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = (int)table_2.getValueAt(table_2.getSelectedRow(),3);
				
				Comment c=CommentDelegate.findCommentById(id);
				c.setEvent(null);
				c.setSwapper(null);
				CommentDelegate.updateComment(c);
				CommentDelegate.removeComment(c);
				ev=EventDelegate.findEventById(ev.getIdEventPk());
				table_2.setModel(new CommentModel(ev));
				table_2.repaint();
				TableColumn column1 = null;
				
				column1 =table_2.getColumnModel().getColumn(3);
				column1.setPreferredWidth(0);
				column1.setMinWidth(0);
				column1.setWidth(0);
				column1.setMaxWidth(0);
				
				
				participanttx.setText("");
				datetx.setText("");
				contenttx.setText("");
				
			}
		});
		btnDelete_1.setBounds(233, 118, 102, 23);
		panel_3.add(btnDelete_1);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		panel_6.setBorder(new LineBorder(Color.GRAY));
		panel_6.setBounds(489, 394, 354, 146);
		panel.add(panel_6);
		panel_6.setLayout(null);
		
		JLabel lblComment = new JLabel("Comment");
		lblComment.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblComment.setBounds(140, 10, 71, 14);
		panel_6.add(lblComment);
		
		JLabel lblParticipant = new JLabel("Participant");
		lblParticipant.setBounds(10, 35, 77, 14);
		panel_6.add(lblParticipant);
		
		
		participanttx.setBounds(128, 35, 178, 14);
		panel_6.add(participanttx);
		
		JLabel lblNewLabel_1 = new JLabel("Date");
		lblNewLabel_1.setBounds(10, 58, 46, 14);
		panel_6.add(lblNewLabel_1);
		
		
		datetx.setBounds(128, 58, 178, 14);
		panel_6.add(datetx);
		
		JLabel lblContent = new JLabel("Content");
		lblContent.setBounds(10, 83, 46, 14);
		panel_6.add(lblContent);
		contenttx.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		
		contenttx.setEditable(false);
		contenttx.setBounds(128, 83, 216, 52);
		panel_6.add(contenttx);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setBorder(new LineBorder(Color.GRAY));
		panel_5.setBounds(489, 265, 354, 118);
		panel.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblUser = new JLabel("Participant");
		lblUser.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUser.setBounds(144, 6, 87, 14);
		panel_5.add(lblUser);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(10, 31, 87, 14);
		panel_5.add(lblFirstName);
		
		JLabel lblNewLabel = new JLabel("Last Name");
		lblNewLabel.setBounds(10, 56, 87, 14);
		panel_5.add(lblNewLabel);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(10, 81, 87, 14);
		panel_5.add(lblMail);
		
		
		firstn.setBounds(133, 31, 179, 14);
		panel_5.add(firstn);
		
		
		lastn.setBounds(133, 56, 179, 14);
		panel_5.add(lastn);
		
		
		mailn.setBounds(133, 81, 211, 14);
		panel_5.add(mailn);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		panel_7.setBorder(new LineBorder(Color.GRAY));
		panel_7.setBounds(919, 69, 327, 378);
		panel.add(panel_7);
		panel_7.setLayout(null);
		
		JLabel lblCreateEvent = new JLabel("Create Event");
		lblCreateEvent.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCreateEvent.setBounds(127, 11, 89, 14);
		panel_7.add(lblCreateEvent);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 45, 46, 14);
		panel_7.add(lblName);
		
		textFieldName = new JTextField();
		textFieldName.setBounds(104, 42, 130, 20);
		panel_7.add(textFieldName);
		textFieldName.setColumns(10);
		
		JLabel lblTheme = new JLabel("Theme");
		lblTheme.setBounds(10, 83, 46, 14);
		panel_7.add(lblTheme);
		
		textFieldTheme = new JTextField();
		textFieldTheme.setColumns(10);
		textFieldTheme.setBounds(104, 73, 130, 20);
		panel_7.add(textFieldTheme);
		
		JLabel lblPlace = new JLabel("Place");
		lblPlace.setBounds(10, 116, 46, 14);
		panel_7.add(lblPlace);
		
		textFieldPlace = new JTextField();
		textFieldPlace.setColumns(10);
		textFieldPlace.setBounds(104, 111, 130, 20);
		panel_7.add(textFieldPlace);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(10, 150, 46, 14);
		panel_7.add(lblDate);
		
		calendar = new JDateChooser();
		
		calendar.setBounds(104, 147, 164, 20);
		panel_7.add(calendar);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(10, 190, 84, 14);
		panel_7.add(lblDescription);
		
		final JEditorPane editorDescription = new JEditorPane();
		editorDescription.setBorder(new LineBorder(new Color(0, 0, 0)));
		editorDescription.setBounds(104, 190, 213, 132);
		panel_7.add(editorDescription);
		
		JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textFieldName.getText().equals("")){
					JOptionPane.showMessageDialog(null, "You must type a name");
				}
				else{
				Event ev =new Event();
				ev.setNameEvent(textFieldName.getText());
				ev.setThemeEvent(textFieldTheme.getText());
				ev.setDescriptionEvent(editorDescription.getText());
				ev.setPlaceEvent(textFieldPlace.getText());
				ev.setDateEvent(calendar.getDate());
				EventDelegate.addEvent(ev);
				table.setModel(new EventModel());
				table.repaint();
				textFieldName.setText("");
				textFieldPlace.setText("");
				textFieldTheme.setText("");
				editorDescription.setText("");
				calendar.setDate(null);
				List<Swapper> sw=SwapperDelegate.findAllSwapper();
				EventDelegate.sendMail(sw, ev);
				
				}
				
				
			}
		});
		btnAdd.setBounds(228, 333, 89, 23);
		panel_7.add(btnAdd);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		panel_4.setLayout(null);
		panel_4.setBorder(new LineBorder(Color.GRAY));
		panel_4.setBounds(489, 11, 354, 243);
		panel.add(panel_4);
		
		JLabel lblUpdateEvent = new JLabel("Update Event");
		lblUpdateEvent.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUpdateEvent.setBounds(136, 11, 95, 20);
		panel_4.add(lblUpdateEvent);
		
		JLabel label_2 = new JLabel("Name");
		label_2.setBounds(10, 45, 46, 14);
		panel_4.add(label_2);
		
		txName = new JTextField();
		txName.setEditable(false);
		txName.setColumns(10);
		txName.setBounds(119, 42, 130, 20);
		panel_4.add(txName);
		
		JLabel label_3 = new JLabel("Theme");
		label_3.setBounds(10, 83, 46, 14);
		panel_4.add(label_3);
		
		txTheme = new JTextField();
		txTheme.setEditable(false);
		txTheme.setColumns(10);
		txTheme.setBounds(119, 80, 130, 20);
		panel_4.add(txTheme);
		
		JLabel label_4 = new JLabel("Place");
		label_4.setBounds(10, 116, 46, 14);
		panel_4.add(label_4);
		
		txPlace = new JTextField();
		txPlace.setEditable(false);
		txPlace.setColumns(10);
		txPlace.setBounds(119, 111, 130, 20);
		panel_4.add(txPlace);
		
		JLabel label_5 = new JLabel("Date");
		label_5.setBounds(10, 150, 46, 14);
		panel_4.add(label_5);
		
		JLabel label_6 = new JLabel("Description");
		label_6.setBounds(10, 182, 89, 14);
		panel_4.add(label_6);
		
		JButton button = new JButton("add");
		button.setBounds(181, 344, 89, 23);
		panel_4.add(button);
		
		JButton btnNewButton = new JButton("Save");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(txName.getText().equals("")){
					JOptionPane.showMessageDialog(null, "You must type a name");
				}
				else{
				ev.setNameEvent(txName.getText());
				ev.setThemeEvent(txTheme.getText());
				ev.setPlaceEvent(txPlace.getText());
				ev.setDescriptionEvent(editorPane.getText());
				ev.setDateEvent(dateChooser.getDate());
				EventDelegate.updateEvent(ev);
				table.setModel(new EventModel());
				table.repaint();
				
				}
				
			}
		});
		btnNewButton.setBounds(255, 255, 89, 23);
		panel_4.add(btnNewButton);
		panel_4.add(editorPane);
		
		dateChooser = new JDateChooser();
		dateChooser.setEnabled(false);
		dateChooser.setBounds(119, 144, 162, 20);
		panel_4.add(dateChooser);
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBackground(SystemColor.menu);
		panel_8.setBounds(10, 615, 1342, 90);
		contentPane.add(panel_8);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/home63.png")));
		button_1.setHorizontalTextPosition(SwingConstants.CENTER);
		button_1.setBounds(628, 6, 79, 67);
		panel_8.add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/ascendant6.png")));
		button_2.setBounds(750, 6, 70, 59);
		panel_8.add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/calendar146.png")));
		button_3.setBounds(870, 6, 70, 59);
		panel_8.add(button_3);
		
		JButton button_4 = new JButton("");
		button_4.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/directory.png")));
		button_4.setBounds(991, 6, 70, 59);
		panel_8.add(button_4);
		
		JButton button_5 = new JButton("");
		button_5.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/users6.png")));
		button_5.setBackground(Color.WHITE);
		button_5.setBounds(1119, 6, 70, 59);
		panel_8.add(button_5);
		
		JButton button_6 = new JButton("");
		button_6.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/right208.png")));
		button_6.setBounds(1257, 11, 62, 52);
		panel_8.add(button_6);
		
		JButton button_7 = new JButton("");
		button_7.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/sim2.png")));
		button_7.setBounds(497, 6, 70, 59);
		panel_8.add(button_7);
		
		JButton button_8 = new JButton("");
		button_8.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/video189.png")));
		button_8.setBounds(372, 6, 70, 59);
		panel_8.add(button_8);
		
		JButton button_9 = new JButton("");
		button_9.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/present3.png")));
		button_9.setBounds(257, 6, 70, 59);
		panel_8.add(button_9);
		
		JButton button_10 = new JButton("");
		button_10.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/handshake1 (1).png")));
		button_10.setBounds(118, 6, 70, 59);
		panel_8.add(button_10);
		
		JButton button_11 = new JButton("");
		button_11.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/left224.png")));
		button_11.setBorder(null);
		button_11.setBounds(10, 9, 62, 52);
		panel_8.add(button_11);
		
		JLabel lblEventManagement = new JLabel("Event Management");
		lblEventManagement.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEventManagement.setHorizontalAlignment(SwingConstants.CENTER);
		lblEventManagement.setFont(new Font("Tahoma", Font.BOLD, 46));
		lblEventManagement.setBounds(377, 11, 548, 53);
		contentPane.add(lblEventManagement);
		
		JButton button_12 = new JButton("");
		button_12.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/repair14.png")));
		button_12.setBounds(1255, 21, 45, 35);
		contentPane.add(button_12);
		
		JButton button_13 = new JButton("");
		button_13.setIcon(new ImageIcon(EventGUI.class.getResource("/Ressources/ImgFromMaster/logout13.png")));
		button_13.setBackground(Color.WHITE);
		button_13.setBounds(1307, 21, 45, 35);
		contentPane.add(button_13);
		
		
	}
}
