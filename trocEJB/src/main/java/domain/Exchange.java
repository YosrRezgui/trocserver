package domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Exchange
 *
 */
@Entity

public class Exchange implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idExchangePk;
	private Date dateExchange;
	private boolean confirmationSwapper1;
	private boolean confirmationSwapper2;
	private boolean fraud;
	@OneToOne
	private Request  request;
	private static final long serialVersionUID = 1L;

	public Exchange() {
		super();
	}

	/**
	 * @param idExchangePk
	 * @param dateExchange
	 * @param confirmationSwapper1
	 * @param confirmationSwapper2
	 * @param request
	 */
	public Exchange(int idExchangePk, Date dateExchange,
			boolean confirmationSwapper1, boolean confirmationSwapper2,
			Request request) {
		super();
		this.idExchangePk = idExchangePk;
		this.dateExchange = dateExchange;
		this.confirmationSwapper1 = confirmationSwapper1;
		this.confirmationSwapper2 = confirmationSwapper2;
		this.request = request;
	}

	/**
	 * @return the idExchangePk
	 */
	public int getIdExchangePk() {
		return idExchangePk;
	}

	/**
	 * @param idExchangePk the idExchangePk to set
	 */
	public void setIdExchangePk(int idExchangePk) {
		this.idExchangePk = idExchangePk;
	}

	/**
	 * @return the dateExchange
	 */
	public Date getDateExchange() {
		return dateExchange;
	}

	/**
	 * @param dateExchange the dateExchange to set
	 */
	public void setDateExchange(Date dateExchange) {
		this.dateExchange = dateExchange;
	}

	/**
	 * @return the confirmationSwapper1
	 */
	public boolean isConfirmationSwapper1() {
		return confirmationSwapper1;
	}

	/**
	 * @param confirmationSwapper1 the confirmationSwapper1 to set
	 */
	public void setConfirmationSwapper1(boolean confirmationSwapper1) {
		this.confirmationSwapper1 = confirmationSwapper1;
	}

	/**
	 * @return the confirmationSwapper2
	 */
	public boolean isConfirmationSwapper2() {
		return confirmationSwapper2;
	}

	/**
	 * @param confirmationSwapper2 the confirmationSwapper2 to set
	 */
	public void setConfirmationSwapper2(boolean confirmationSwapper2) {
		this.confirmationSwapper2 = confirmationSwapper2;
	}

	/**
	 * @return the request
	 */
	public Request getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(Request request) {
		this.request = request;
	}

	public boolean isFraud() {
		return fraud;
	}

	public void setFraud(boolean fraud) {
		this.fraud = fraud;
	}

   
	
	
}
