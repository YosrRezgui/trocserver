package domain;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Complaint
 *
 */
@Entity

public class Complaint implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idComplaintPk;
	private String subject;
	private String contents;
	@ManyToOne
	private Swapper swapperAj;
	@ManyToOne
	private Swapper swapperRe;
	@ManyToOne
	private Offer offer;
	
	private static final long serialVersionUID = 1L;

	public Complaint() {
		super();
	}

	/**
	 * @param idComplaintPk
	 * @param subject
	 * @param contents
	 * @param swapperAj
	 * @param swapperRe
	 * @param offer
	 */
	public Complaint(int idComplaintPk, String subject, String contents,
			Swapper swapperAj, Swapper swapperRe, Offer offer) {
		super();
		this.idComplaintPk = idComplaintPk;
		this.subject = subject;
		this.contents = contents;
		this.swapperAj = swapperAj;
		this.swapperRe = swapperRe;
		this.offer = offer;
	}

	/**
	 * @return the idComplaintPk
	 */
	public int getIdComplaintPk() {
		return idComplaintPk;
	}

	/**
	 * @param idComplaintPk the idComplaintPk to set
	 */
	public void setIdComplaintPk(int idComplaintPk) {
		this.idComplaintPk = idComplaintPk;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the contents
	 */
	public String getContents() {
		return contents;
	}

	/**
	 * @param contents the contents to set
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}

	/**
	 * @return the swapperAj
	 */
	public Swapper getSwapperAj() {
		return swapperAj;
	}

	/**
	 * @param swapperAj the swapperAj to set
	 */
	public void setSwapperAj(Swapper swapperAj) {
		this.swapperAj = swapperAj;
	}

	/**
	 * @return the swapperRe
	 */
	public Swapper getSwapperRe() {
		return swapperRe;
	}

	/**
	 * @param swapperRe the swapperRe to set
	 */
	public void setSwapperRe(Swapper swapperRe) {
		this.swapperRe = swapperRe;
	}

	/**
	 * @return the offer
	 */
	public Offer getOffer() {
		return offer;
	}

	/**
	 * @param offer the offer to set
	 */
	public void setOffer(Offer offer) {
		this.offer = offer;
	}   
	
   
}
