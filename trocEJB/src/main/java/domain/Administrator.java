package domain;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrator
 *
 */
@Entity

public class Administrator implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAdministratorPk;
	private String firstName;
	private String secondName;
	private String login;
	private String password;
	private String mail;
	private static final long serialVersionUID = 1L;

	public Administrator() {
		super();
	}   
	
	public Administrator(int idAdministratorPk, String firstName,
			String secondName, String login, String password, String mail) {
		super();
		this.idAdministratorPk = idAdministratorPk;
		this.firstName = firstName;
		this.secondName = secondName;
		this.login = login;
		this.password = password;
		this.mail = mail;
	}

	public int getIdAdministratorPk() {
		return this.idAdministratorPk;
	}

	public void setIdAdministratorPk(int idAdministratorPk) {
		this.idAdministratorPk = idAdministratorPk;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getSecondName() {
		return this.secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}   
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
   
}
