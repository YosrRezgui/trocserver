package domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Participant
 *
 */
@Entity

public class Participant implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idParticipantPk;
	
	
	@ManyToOne
	private Event event;
	@ManyToOne
	private Swapper swapper;
	
	
	
	private static final long serialVersionUID = 1L;

	public Participant() {
		super();
	}

	/**
	 * @param idParticipantPk
	 * @param event
	 * @param swapper
	 */
	public Participant(int idParticipantPk, Event event, Swapper swapper) {
		super();
		this.idParticipantPk = idParticipantPk;
		this.event = event;
		this.swapper = swapper;
	}

	/**
	 * @return the idParticipantPk
	 */
	public int getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(int idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * @return the event
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * @return the swapper
	 */
	public Swapper getSwapper() {
		return swapper;
	}

	/**
	 * @param swapper the swapper to set
	 */
	public void setSwapper(Swapper swapper) {
		this.swapper = swapper;
	}   
	
	
}
