package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity

public class Category implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCategory;
	private String nameCategory;
	@OneToMany(mappedBy="category",cascade=CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval = true)
	private List<Offer> offers;
	@OneToMany(mappedBy="category",cascade=CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval = true)
	private List<SubCategory> subCategories;
	private static final long serialVersionUID = 1L;

	public Category() {
		super();
	}

	/**
	 * @param idCategory
	 * @param nameCategory
	 * @param offers
	 * @param subCategories
	 */
	public Category(int idCategory, String nameCategory, List<Offer> offers,
			List<SubCategory> subCategories) {
		super();
		this.idCategory = idCategory;
		this.nameCategory = nameCategory;
		this.offers = offers;
		this.subCategories = subCategories;
	}

	/**
	 * @return the idCategory
	 */
	public int getIdCategory() {
		return idCategory;
	}

	/**
	 * @param idCategory the idCategory to set
	 */
	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	/**
	 * @return the nameCategory
	 */
	public String getNameCategory() {
		return nameCategory;
	}

	/**
	 * @param nameCategory the nameCategory to set
	 */
	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	/**
	 * @return the offers
	 */
	public List<Offer> getOffers() {
		return offers;
	}

	/**
	 * @param offers the offers to set
	 */
	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}

	/**
	 * @return the subCategories
	 */
	public List<SubCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * @param subCategories the subCategories to set
	 */
	public void setSubCategories(List<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	
}
