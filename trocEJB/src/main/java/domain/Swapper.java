package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Swapper
 *
 */
@Entity

public class Swapper implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idSwapperPk;
	private String firstName;
	private String secondName;
	private String login;
	private String password;
	private int nic;
	private String mail;
	private Date dateOfBrith;
	private int phoneNumber;
	private String address;
	@Column(columnDefinition="LONGBLOB")
	private byte[] image;
	private boolean verification;
	private boolean blackList;
	@OneToMany(mappedBy="swapperAj", cascade=CascadeType.ALL)
	private List<Complaint> complaintsAj;
	
	@OneToMany(mappedBy="swapperRe", cascade=CascadeType.ALL)
	private List<Complaint> complaintsRe;
	
	@OneToMany(mappedBy="swapper" ,fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	
	private List<Offer> offers;
	@OneToMany(mappedBy="sender", cascade=CascadeType.ALL)
	private List<Discussion> discussionsSendt;
	@OneToMany(mappedBy="receiver", cascade=CascadeType.ALL)
	private List<Discussion> discussionsReceived;
	
	@OneToMany(mappedBy="swapper", cascade=CascadeType.ALL)
	private List<Comment> comments;
	@OneToMany(mappedBy="swapper", cascade=CascadeType.ALL)
	private List<Participant> participants;
	@OneToMany(mappedBy="noteSender", cascade=CascadeType.ALL)
	private List<Note> notesSendt;
	@OneToMany(mappedBy="noteReceiver", cascade=CascadeType.ALL)
	private List<Note> notesReceived;
	
	
	
	
	private static final long serialVersionUID = 1L;

	public Swapper() {
		
	}

	/**
	 * @param idSwapperPk
	 * @param firstName
	 * @param secondName
	 * @param login
	 * @param password
	 * @param nic
	 * @param mail
	 * @param dateOfBrith
	 * @param phoneNumber
	 * @param address
	 * @param image
	 * @param verification
	 * @param blackList
	 * @param complaintsAj
	 * @param complaintsRe
	 * @param offers
	 * @param discussionsSendt
	 * @param discussionsReceived
	 * @param comments
	 * @param participants
	 * @param notesSendt
	 * @param notesReceived
	 */
	public Swapper(int idSwapperPk, String firstName, String secondName,
			String login, String password, int nic, String mail,
			Date dateOfBrith, int phoneNumber, String address, byte[] image,
			boolean verification, boolean blackList,
			List<Complaint> complaintsAj, List<Complaint> complaintsRe,
			List<Offer> offers, List<Discussion> discussionsSendt,
			List<Discussion> discussionsReceived, List<Comment> comments,
			List<Participant> participants, List<Note> notesSendt,
			List<Note> notesReceived) {
	
		this.idSwapperPk = idSwapperPk;
		this.firstName = firstName;
		this.secondName = secondName;
		this.login = login;
		this.password = password;
		this.nic = nic;
		this.mail = mail;
		this.dateOfBrith = dateOfBrith;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.image = image;
		this.verification = verification;
		this.blackList = blackList;
		this.complaintsAj = complaintsAj;
		this.complaintsRe = complaintsRe;
		this.offers = offers;
		this.discussionsSendt = discussionsSendt;
		this.discussionsReceived = discussionsReceived;
		this.comments = comments;
		this.participants = participants;
		this.notesSendt = notesSendt;
		this.notesReceived = notesReceived;
	}

	public Boolean getVerification() {
		return verification;
	}

	public void setVerification(Boolean verification) {
		this.verification = verification;
	}

	public Boolean getBlackList() {
		return blackList;
	}

	public void setBlackList(Boolean blackList) {
		this.blackList = blackList;
	}

	/**
	 * @return the idSwapperPk
	 */
	public int getIdSwapperPk() {
		return idSwapperPk;
	}

	/**
	 * @param idSwapperPk the idSwapperPk to set
	 */
	public void setIdSwapperPk(int idSwapperPk) {
		this.idSwapperPk = idSwapperPk;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the secondName
	 */
	public String getSecondName() {
		return secondName;
	}

	/**
	 * @param secondName the secondName to set
	 */
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the nic
	 */
	public int getNic() {
		return nic;
	}

	/**
	 * @param nic the nic to set
	 */
	public void setNic(int nic) {
		this.nic = nic;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the dateOfBrith
	 */
	public Date getDateOfBrith() {
		return dateOfBrith;
	}

	/**
	 * @param dateOfBrith the dateOfBrith to set
	 */
	public void setDateOfBrith(Date dateOfBrith) {
		this.dateOfBrith = dateOfBrith;
	}

	/**
	 * @return the phoneNumber
	 */
	public int getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + (blackList ? 1231 : 1237);
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((complaintsAj == null) ? 0 : complaintsAj.hashCode());
		result = prime * result
				+ ((complaintsRe == null) ? 0 : complaintsRe.hashCode());
		result = prime * result
				+ ((dateOfBrith == null) ? 0 : dateOfBrith.hashCode());
		result = prime
				* result
				+ ((discussionsReceived == null) ? 0 : discussionsReceived
						.hashCode());
		result = prime
				* result
				+ ((discussionsSendt == null) ? 0 : discussionsSendt.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + idSwapperPk;
		result = prime * result + Arrays.hashCode(image);
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result + nic;
		result = prime * result
				+ ((notesReceived == null) ? 0 : notesReceived.hashCode());
		result = prime * result
				+ ((notesSendt == null) ? 0 : notesSendt.hashCode());
		result = prime * result + ((offers == null) ? 0 : offers.hashCode());
		result = prime * result
				+ ((participants == null) ? 0 : participants.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + phoneNumber;
		result = prime * result
				+ ((secondName == null) ? 0 : secondName.hashCode());
		result = prime * result + (verification ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Swapper other = (Swapper) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (blackList != other.blackList)
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (complaintsAj == null) {
			if (other.complaintsAj != null)
				return false;
		} else if (!complaintsAj.equals(other.complaintsAj))
			return false;
		if (complaintsRe == null) {
			if (other.complaintsRe != null)
				return false;
		} else if (!complaintsRe.equals(other.complaintsRe))
			return false;
		if (dateOfBrith == null) {
			if (other.dateOfBrith != null)
				return false;
		} else if (!dateOfBrith.equals(other.dateOfBrith))
			return false;
		if (discussionsReceived == null) {
			if (other.discussionsReceived != null)
				return false;
		} else if (!discussionsReceived.equals(other.discussionsReceived))
			return false;
		if (discussionsSendt == null) {
			if (other.discussionsSendt != null)
				return false;
		} else if (!discussionsSendt.equals(other.discussionsSendt))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (idSwapperPk != other.idSwapperPk)
			return false;
		if (!Arrays.equals(image, other.image))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (nic != other.nic)
			return false;
		if (notesReceived == null) {
			if (other.notesReceived != null)
				return false;
		} else if (!notesReceived.equals(other.notesReceived))
			return false;
		if (notesSendt == null) {
			if (other.notesSendt != null)
				return false;
		} else if (!notesSendt.equals(other.notesSendt))
			return false;
		if (offers == null) {
			if (other.offers != null)
				return false;
		} else if (!offers.equals(other.offers))
			return false;
		if (participants == null) {
			if (other.participants != null)
				return false;
		} else if (!participants.equals(other.participants))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phoneNumber != other.phoneNumber)
			return false;
		if (secondName == null) {
			if (other.secondName != null)
				return false;
		} else if (!secondName.equals(other.secondName))
			return false;
		if (verification != other.verification)
			return false;
		return true;
	}

	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * @return the verification
	 */
	public boolean isVerification() {
		return verification;
	}

	/**
	 * @param verification the verification to set
	 */
	public void setVerification(boolean verification) {
		this.verification = verification;
	}

	/**
	 * @return the blackList
	 */
	public boolean isBlackList() {
		return blackList;
	}

	/**
	 * @param blackList the blackList to set
	 */
	public void setBlackList(boolean blackList) {
		this.blackList = blackList;
	}

	/**
	 * @return the complaintsAj
	 */
	public List<Complaint> getComplaintsAj() {
		return complaintsAj;
	}

	/**
	 * @param complaintsAj the complaintsAj to set
	 */
	public void setComplaintsAj(List<Complaint> complaintsAj) {
		this.complaintsAj = complaintsAj;
	}

	/**
	 * @return the complaintsRe
	 */
	public List<Complaint> getComplaintsRe() {
		return complaintsRe;
	}

	/**
	 * @param complaintsRe the complaintsRe to set
	 */
	public void setComplaintsRe(List<Complaint> complaintsRe) {
		this.complaintsRe = complaintsRe;
	}

	/**
	 * @return the offers
	 */
	public List<Offer> getOffers() {
		return offers;
	}

	/**
	 * @param offers the offers to set
	 */
	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}

	/**
	 * @return the discussionsSendt
	 */
	public List<Discussion> getDiscussionsSendt() {
		return discussionsSendt;
	}

	/**
	 * @param discussionsSendt the discussionsSendt to set
	 */
	public void setDiscussionsSendt(List<Discussion> discussionsSendt) {
		this.discussionsSendt = discussionsSendt;
	}

	/**
	 * @return the discussionsReceived
	 */
	public List<Discussion> getDiscussionsReceived() {
		return discussionsReceived;
	}

	/**
	 * @param discussionsReceived the discussionsReceived to set
	 */
	public void setDiscussionsReceived(List<Discussion> discussionsReceived) {
		this.discussionsReceived = discussionsReceived;
	}

	/**
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * @return the participants
	 */
	public List<Participant> getParticipants() {
		return participants;
	}

	/**
	 * @param participants the participants to set
	 */
	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	/**
	 * @return the notesSendt
	 */
	public List<Note> getNotesSendt() {
		return notesSendt;
	}

	/**
	 * @param notesSendt the notesSendt to set
	 */
	public void setNotesSendt(List<Note> notesSendt) {
		this.notesSendt = notesSendt;
	}

	/**
	 * @return the notesReceived
	 */
	public List<Note> getNotesReceived() {
		return notesReceived;
	}

	/**
	 * @param notesReceived the notesReceived to set
	 */
	public void setNotesReceived(List<Note> notesReceived) {
		this.notesReceived = notesReceived;
	}   
	
}
	

	
