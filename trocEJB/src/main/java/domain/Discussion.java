package domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Discussion
 *
 */
@Entity

public class Discussion implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idDiscussionPk;
	@ManyToOne
	private Swapper sender;
	@ManyToOne
	private Swapper receiver;
	
	@OneToMany(mappedBy="discussion",cascade=CascadeType.ALL)
	private List<Message> messages;
	private Date dateDiscussion;
	
	
	
	
	
	
	
	private static final long serialVersionUID = 1L;

	public Discussion() {
		super();
	}

	/**
	 * @param idDiscussionPk
	 * @param sender
	 * @param receiver
	 * @param messages
	 * @param dateDiscussion
	 */
	public Discussion(int idDiscussionPk, Swapper sender, Swapper receiver,
			List<Message> messages, Date dateDiscussion) {
		super();
		this.idDiscussionPk = idDiscussionPk;
		this.sender = sender;
		this.receiver = receiver;
		this.messages = messages;
		this.dateDiscussion = dateDiscussion;
	}

	/**
	 * @return the idDiscussionPk
	 */
	public int getIdDiscussionPk() {
		return idDiscussionPk;
	}

	/**
	 * @param idDiscussionPk the idDiscussionPk to set
	 */
	public void setIdDiscussionPk(int idDiscussionPk) {
		this.idDiscussionPk = idDiscussionPk;
	}

	/**
	 * @return the sender
	 */
	public Swapper getSender() {
		return sender;
	}

	/**
	 * @param sender the sender to set
	 */
	public void setSender(Swapper sender) {
		this.sender = sender;
	}

	/**
	 * @return the receiver
	 */
	public Swapper getReceiver() {
		return receiver;
	}

	/**
	 * @param receiver the receiver to set
	 */
	public void setReceiver(Swapper receiver) {
		this.receiver = receiver;
	}

	/**
	 * @return the messages
	 */
	public List<Message> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	/**
	 * @return the dateDiscussion
	 */
	public Date getDateDiscussion() {
		return dateDiscussion;
	}

	/**
	 * @param dateDiscussion the dateDiscussion to set
	 */
	public void setDateDiscussion(Date dateDiscussion) {
		this.dateDiscussion = dateDiscussion;
	}   
	
	
}
