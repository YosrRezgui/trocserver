package domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Request
 *
 */
@Entity

public class Request implements Serializable {

	   
	@EmbeddedId
	private ObjectId objectId;

	private Date dateRequest;
	private boolean confirmation;
	
	@ManyToOne
	@JoinColumn(name="idOffer1", insertable=false ,updatable=false)
	private Offer offer1;
	@ManyToOne
	@JoinColumn(name="idoffer2", insertable=false ,updatable=false)
	private Offer offer2;
	
	@OneToOne(mappedBy="request")
	private Exchange  exchange;
	
	
	private static final long serialVersionUID = 1L;

	public Request() {
		super();
	}

	/**
	 * @param objectId
	 * @param dateRequest
	 * @param confirmation
	 * @param offer1
	 * @param offer2
	 * @param exchange
	 */
	public Request(ObjectId objectId, Date dateRequest, boolean confirmation,
			Offer offer1, Offer offer2, Exchange exchange) {
		super();
		this.objectId = objectId;
		this.dateRequest = dateRequest;
		this.confirmation = confirmation;
		this.offer1 = offer1;
		this.offer2 = offer2;
		this.exchange = exchange;
	}

	/**
	 * @return the objectId
	 */
	public ObjectId getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(ObjectId objectId) {
		this.objectId = objectId;
	}

	/**
	 * @return the dateRequest
	 */
	public Date getDateRequest() {
		return dateRequest;
	}

	/**
	 * @param dateRequest the dateRequest to set
	 */
	public void setDateRequest(Date dateRequest) {
		this.dateRequest = dateRequest;
	}

	/**
	 * @return the confirmation
	 */
	public boolean isConfirmation() {
		return confirmation;
	}

	/**
	 * @param confirmation the confirmation to set
	 */
	public void setConfirmation(boolean confirmation) {
		this.confirmation = confirmation;
	}

	/**
	 * @return the offer1
	 */
	public Offer getOffer1() {
		return offer1;
	}

	/**
	 * @param offer1 the offer1 to set
	 */
	public void setOffer1(Offer offer1) {
		this.offer1 = offer1;
	}

	/**
	 * @return the offer2
	 */
	public Offer getOffer2() {
		return offer2;
	}

	/**
	 * @param offer2 the offer2 to set
	 */
	public void setOffer2(Offer offer2) {
		this.offer2 = offer2;
	}

	/**
	 * @return the exchange
	 */
	public Exchange getExchange() {
		return exchange;
	}

	/**
	 * @param exchange the exchange to set
	 */
	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}   
	
	
}
