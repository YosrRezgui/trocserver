package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Comment
 *
 */
@Entity

public class Comment implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCommentPk;
	private Date dateComment;
	private String contents;
	@ManyToOne(optional = true )
	@JoinColumn(nullable=true)
	private Event event;
	@ManyToOne(optional = true )
	@JoinColumn(nullable=true)
	private Swapper swapper;
	
	private static final long serialVersionUID = 1L;

	public Comment() {
		super();
	}

	/**
	 * @param idCommentPk
	 * @param dateComment
	 * @param contents
	 * @param event
	 * @param swapper
	 */
	public Comment(int idCommentPk, Date dateComment, String contents,
			Event event, Swapper swapper) {
		super();
		this.idCommentPk = idCommentPk;
		this.dateComment = dateComment;
		this.contents = contents;
		this.event = event;
		this.swapper = swapper;
	}

	/**
	 * @return the idCommentPk
	 */
	public int getIdCommentPk() {
		return idCommentPk;
	}

	/**
	 * @param idCommentPk the idCommentPk to set
	 */
	public void setIdCommentPk(int idCommentPk) {
		this.idCommentPk = idCommentPk;
	}

	/**
	 * @return the dateComment
	 */
	public Date getDateComment() {
		return dateComment;
	}

	/**
	 * @param dateComment the dateComment to set
	 */
	public void setDateComment(Date dateComment) {
		this.dateComment = dateComment;
	}

	/**
	 * @return the contents
	 */
	public String getContents() {
		return contents;
	}

	/**
	 * @param contents the contents to set
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}

	/**
	 * @return the event
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * @return the swapper
	 */
	public Swapper getSwapper() {
		return swapper;
	}

	/**
	 * @param swapper the swapper to set
	 */
	public void setSwapper(Swapper swapper) {
		this.swapper = swapper;
	}

	
	
	
}
