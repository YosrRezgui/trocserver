package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity

public class Event implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idEventPk;
	private String nameEvent;
	private Date dateEvent;
	private String placeEvent;
	private String descriptionEvent;
	private String themeEvent;
	
	@OneToMany(mappedBy="event", cascade=CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval = true)
	private List<Comment> comments;
	@OneToMany(mappedBy="event", cascade=CascadeType.ALL,fetch=FetchType.EAGER,orphanRemoval = true)

	private List<Participant> participants;
	
	
	private static final long serialVersionUID = 1L;

	public Event() {
		super();
	}

	/**
	 * @param idEventPk
	 * @param nameEvent
	 * @param dateEvent
	 * @param placeEvent
	 * @param descriptionEvent
	 * @param themeEvent
	 * @param comments
	 * @param participants
	 */
	public Event(int idEventPk, String nameEvent, Date dateEvent,
			String placeEvent, String descriptionEvent, String themeEvent,
			List<Comment> comments, List<Participant> participants) {
		super();
		this.idEventPk = idEventPk;
		this.nameEvent = nameEvent;
		this.dateEvent = dateEvent;
		this.placeEvent = placeEvent;
		this.descriptionEvent = descriptionEvent;
		this.themeEvent = themeEvent;
		this.comments = comments;
		this.participants = participants;
	}

	/**
	 * @return the idEventPk
	 */
	public int getIdEventPk() {
		return idEventPk;
	}

	/**
	 * @param idEventPk the idEventPk to set
	 */
	public void setIdEventPk(int idEventPk) {
		this.idEventPk = idEventPk;
	}

	/**
	 * @return the nameEvent
	 */
	public String getNameEvent() {
		return nameEvent;
	}

	/**
	 * @param nameEvent the nameEvent to set
	 */
	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}

	/**
	 * @return the dateEvent
	 */
	public Date getDateEvent() {
		return dateEvent;
	}

	/**
	 * @param dateEvent the dateEvent to set
	 */
	public void setDateEvent(Date dateEvent) {
		this.dateEvent = dateEvent;
	}

	/**
	 * @return the placeEvent
	 */
	public String getPlaceEvent() {
		return placeEvent;
	}

	/**
	 * @param placeEvent the placeEvent to set
	 */
	public void setPlaceEvent(String placeEvent) {
		this.placeEvent = placeEvent;
	}

	/**
	 * @return the descriptionEvent
	 */
	public String getDescriptionEvent() {
		return descriptionEvent;
	}

	/**
	 * @param descriptionEvent the descriptionEvent to set
	 */
	public void setDescriptionEvent(String descriptionEvent) {
		this.descriptionEvent = descriptionEvent;
	}

	/**
	 * @return the themeEvent
	 */
	public String getThemeEvent() {
		return themeEvent;
	}

	/**
	 * @param themeEvent the themeEvent to set
	 */
	public void setThemeEvent(String themeEvent) {
		this.themeEvent = themeEvent;
	}

	/**
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * @return the participants
	 */
	public List<Participant> getParticipants() {
		return participants;
	}

	/**
	 * @param participants the participants to set
	 */
	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}   
	
	
}
