package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Message
 *
 */
@Entity

public class Message implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idMessagePk;
	private String contents;
	private Date dateMessage;
	
	@ManyToOne
	private Discussion discussion;
	private static final long serialVersionUID = 1L;

	public Message() {
		super();
	}

	/**
	 * @param idMessagePk
	 * @param contents
	 * @param dateMessage
	 * @param discussion
	 */
	public Message(int idMessagePk, String contents, Date dateMessage,
			Discussion discussion) {
		super();
		this.idMessagePk = idMessagePk;
		this.contents = contents;
		this.dateMessage = dateMessage;
		this.discussion = discussion;
	}

	/**
	 * @return the idMessagePk
	 */
	public int getIdMessagePk() {
		return idMessagePk;
	}

	/**
	 * @param idMessagePk the idMessagePk to set
	 */
	public void setIdMessagePk(int idMessagePk) {
		this.idMessagePk = idMessagePk;
	}

	/**
	 * @return the contents
	 */
	public String getContents() {
		return contents;
	}

	/**
	 * @param contents the contents to set
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}

	/**
	 * @return the dateMessage
	 */
	public Date getDateMessage() {
		return dateMessage;
	}

	/**
	 * @param dateMessage the dateMessage to set
	 */
	public void setDateMessage(Date dateMessage) {
		this.dateMessage = dateMessage;
	}

	/**
	 * @return the discussion
	 */
	public Discussion getDiscussion() {
		return discussion;
	}

	/**
	 * @param discussion the discussion to set
	 */
	public void setDiscussion(Discussion discussion) {
		this.discussion = discussion;
	}   
	
}
