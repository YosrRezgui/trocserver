package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: SubCategory
 *
 */
@Entity

public class SubCategory implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idSubCat;
	private String nameSubCat;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Category category;
	@OneToMany(mappedBy="subcategory",cascade=CascadeType.ALL)
	private List<Offer> offers;

	public SubCategory() {
		super();
	}
	/**
	 * @param idSubCat
	 * @param nameSubCat
	 * @param category
	 * @param offers
	 */
	public SubCategory(int idSubCat, String nameSubCat, Category category,
			List<Offer> offers) {
		super();
		this.idSubCat = idSubCat;
		this.nameSubCat = nameSubCat;
		this.category = category;
		this.offers = offers;
	}   
	/**
	 * @return the idSubCat
	 */
	public int getIdSubCat() {
		return idSubCat;
	}

	/**
	 * @param idSubCat the idSubCat to set
	 */
	public void setIdSubCat(int idSubCat) {
		this.idSubCat = idSubCat;
	}

	/**
	 * @return the nameSubCat
	 */
	public String getNameSubCat() {
		return nameSubCat;
	}

	/**
	 * @param nameSubCat the nameSubCat to set
	 */
	public void setNameSubCat(String nameSubCat) {
		this.nameSubCat = nameSubCat;
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @return the offers
	 */
	public List<Offer> getOffers() {
		return offers;
	}

	/**
	 * @param offers the offers to set
	 */
	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}

	
	
	
}
