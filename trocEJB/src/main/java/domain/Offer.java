package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.ws.rs.DefaultValue;

/**
 * Entity implementation class for Entity: Offer
 *
 */
@Entity
/**
 * 
 * @author rafik
 *
 */

public class Offer implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idOfferPk;
	private String nameOffer;
	private String DescriptionOffer;
	private Date dateOffer;
	private String interestedBy;
	@Column(columnDefinition="LONGBLOB")
	private byte [] image;
	private String type;
	@ManyToOne
	private Swapper swapper;
	@ManyToOne
	private Category category;
	@ManyToOne
	private SubCategory subcategory;
	@OneToMany(mappedBy="offer1" , cascade=CascadeType.ALL)
	private List<Request> requests1;
	@OneToMany(mappedBy="offer2" , cascade=CascadeType.ALL)
	private List<Request> requests2;
	
	private static final long serialVersionUID = 1L;

	public Offer() {
		super();
	}


	public Offer(int idOfferPk, String nameOffer, String descriptionOffer,
			Date dateOffer, String interestedBy, byte[] image, Swapper swapper,
			Category category, SubCategory subcategory,
			List<Request> requests1, List<Request> requests2) {
		super();
		this.idOfferPk = idOfferPk;
		this.nameOffer = nameOffer;
		DescriptionOffer = descriptionOffer;
		this.dateOffer = dateOffer;
		this.interestedBy = interestedBy;
		this.image = image;
		this.swapper = swapper;
		this.category = category;
		this.subcategory = subcategory;
		this.requests1 = requests1;
		this.requests2 = requests2;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((DescriptionOffer == null) ? 0 : DescriptionOffer.hashCode());
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result
				+ ((dateOffer == null) ? 0 : dateOffer.hashCode());
		result = prime * result + idOfferPk;
		result = prime * result
				+ ((interestedBy == null) ? 0 : interestedBy.hashCode());
		result = prime * result
				+ ((nameOffer == null) ? 0 : nameOffer.hashCode());
		result = prime * result
				+ ((requests1 == null) ? 0 : requests1.hashCode());
		result = prime * result
				+ ((requests2 == null) ? 0 : requests2.hashCode());
		result = prime * result
				+ ((subcategory == null) ? 0 : subcategory.hashCode());
		result = prime * result + ((swapper == null) ? 0 : swapper.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Offer other = (Offer) obj;
		if (DescriptionOffer == null) {
			if (other.DescriptionOffer != null)
				return false;
		} else if (!DescriptionOffer.equals(other.DescriptionOffer))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (dateOffer == null) {
			if (other.dateOffer != null)
				return false;
		} else if (!dateOffer.equals(other.dateOffer))
			return false;
		if (idOfferPk != other.idOfferPk)
			return false;
		if (interestedBy == null) {
			if (other.interestedBy != null)
				return false;
		} else if (!interestedBy.equals(other.interestedBy))
			return false;
		if (nameOffer == null) {
			if (other.nameOffer != null)
				return false;
		} else if (!nameOffer.equals(other.nameOffer))
			return false;
		if (requests1 == null) {
			if (other.requests1 != null)
				return false;
		} else if (!requests1.equals(other.requests1))
			return false;
		if (requests2 == null) {
			if (other.requests2 != null)
				return false;
		} else if (!requests2.equals(other.requests2))
			return false;
		if (subcategory == null) {
			if (other.subcategory != null)
				return false;
		} else if (!subcategory.equals(other.subcategory))
			return false;
		if (swapper == null) {
			if (other.swapper != null)
				return false;
		} else if (!swapper.equals(other.swapper))
			return false;
		return true;
	}

	/**
	 * @return the idOfferPk
	 */
	public int getIdOfferPk() {
		return idOfferPk;
	}

	/**
	 * @param idOfferPk the idOfferPk to set
	 */
	public void setIdOfferPk(int idOfferPk) {
		this.idOfferPk = idOfferPk;
	}

	/**
	 * @return the nameOffer
	 */
	public String getNameOffer() {
		return nameOffer;
	}

	/**
	 * @param nameOffer the nameOffer to set
	 */
	public void setNameOffer(String nameOffer) {
		this.nameOffer = nameOffer;
	}

	/**
	 * @return the descriptionOffer
	 */
	public String getDescriptionOffer() {
		return DescriptionOffer;
	}

	/**
	 * @param descriptionOffer the descriptionOffer to set
	 */
	public void setDescriptionOffer(String descriptionOffer) {
		DescriptionOffer = descriptionOffer;
	}

	/**
	 * @return the dateOffer
	 */
	public Date getDateOffer() {
		return dateOffer;
	}

	/**
	 * @param dateOffer the dateOffer to set
	 */
	public void setDateOffer(Date dateOffer) {
		this.dateOffer = dateOffer;
	}

	/**
	 * @return the interestedBy
	 */
	public String getInterestedBy() {
		return interestedBy;
	}

	/**
	 * @param interestedBy the interestedBy to set
	 */
	public void setInterestedBy(String interestedBy) {
		this.interestedBy = interestedBy;
	}

	/**
	 * @return the swapper
	 */
	public Swapper getSwapper() {
		return swapper;
	}

	/**
	 * @param swapper the swapper to set
	 */
	public void setSwapper(Swapper swapper) {
		this.swapper = swapper;
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @return the subcategory
	 */
	public SubCategory getsubcategory() {
		return subcategory;
	}
	/**
	 * @param subcategory the subcategory to set
	 */

	public void setsubcategory(SubCategory subcategory) {
		this.subcategory = subcategory;
	}

	/**
	 * @return the requests1
	 */
	public List<Request> getRequests1() {
		return requests1;
	}

	/**
	 * @param requests1 the requests1 to set
	 */
	public void setRequests1(List<Request> requests1) {
		this.requests1 = requests1;
	}

	/**
	 * @return the requests2
	 */
	public List<Request> getRequests2() {
		return requests2;
	}

	/**
	 * @param requests2 the requests2 to set
	 */
	public void setRequests2(List<Request> requests2) {
		this.requests2 = requests2;
	}   
	
	 public String toString() {
		    return "Professor id: " + getDescriptionOffer();}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte [] getImage() {
		return image;
	}

	public void setImage(byte [] image) {
		this.image = image;
	}
   
}
