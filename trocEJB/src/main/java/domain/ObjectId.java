package domain;

import java.io.Serializable;

import javax.persistence.Embeddable;
@Embeddable
public class ObjectId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idOffer1;
	private int idoffer2;
	/**
	 * @return the idOffer1
	 */
	public ObjectId() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param idOffer1
	 * @param idoffer2
	 */
	public ObjectId(int idOffer1, int idoffer2) {
		super();
		this.idOffer1 = idOffer1;
		this.idoffer2 = idoffer2;
	}

	public int getIdOffer1() {
		return idOffer1;
	}
	/**
	 * @param idOffer1 the idOffer1 to set
	 */
	public void setIdOffer1(int idOffer1) {
		this.idOffer1 = idOffer1;
	}
	/**
	 * @return the idoffer2
	 */
	public int getIdoffer2() {
		return idoffer2;
	}
	/**
	 * @param idoffer2 the idoffer2 to set
	 */
	public void setIdoffer2(int idoffer2) {
		this.idoffer2 = idoffer2;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idOffer1;
		result = prime * result + idoffer2;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjectId other = (ObjectId) obj;
		if (idOffer1 != other.idOffer1)
			return false;
		if (idoffer2 != other.idoffer2)
			return false;
		return true;
	}
	

}
