package domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Note
 *
 */
@Entity

public class Note implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idNote;
	private int note;
	@ManyToOne
	private Swapper noteSender;
	@ManyToOne
	private Swapper noteReceiver;
	
	private static final long serialVersionUID = 1L;

	public Note() {
		super();
	}

	/**
	 * @param idNote
	 * @param note
	 * @param noteSender
	 * @param noteReceiver
	 */
	public Note(int idNote, int note, Swapper noteSender, Swapper noteReceiver) {
		super();
		this.idNote = idNote;
		this.note = note;
		this.noteSender = noteSender;
		this.noteReceiver = noteReceiver;
	}

	/**
	 * @return the idNote
	 */
	public int getIdNote() {
		return idNote;
	}

	/**
	 * @param idNote the idNote to set
	 */
	public void setIdNote(int idNote) {
		this.idNote = idNote;
	}

	/**
	 * @return the note
	 */
	public int getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(int note) {
		this.note = note;
	}

	/**
	 * @return the noteSender
	 */
	public Swapper getNoteSender() {
		return noteSender;
	}

	/**
	 * @param noteSender the noteSender to set
	 */
	public void setNoteSender(Swapper noteSender) {
		this.noteSender = noteSender;
	}

	/**
	 * @return the noteReceiver
	 */
	public Swapper getNoteReceiver() {
		return noteReceiver;
	}

	/**
	 * @param noteReceiver the noteReceiver to set
	 */
	public void setNoteReceiver(Swapper noteReceiver) {
		this.noteReceiver = noteReceiver;
	}   
	
	
}
