package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import domain.Participant;

/**
 * Session Bean implementation class ParticipantService
 */
/**
 * 
 * @author Houssem
 *
 */
@Stateless
@LocalBean
public class ParticipantService implements ParticipantServiceRemote, ParticipantServiceLocal {
	
	
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    public ParticipantService() {
    	
        
    }

	@Override
	public void addParticipant(Participant participant) {
		em.persist(participant);
		
	}

	@Override
	public void updateParticipant(Participant participant) {

		em.merge(participant);
	}

	@Override
	public void deleteParticipant(Participant participant) {

		em.remove(participant);
	}

	@Override
	public Participant findParticipantById(int id) {
		Participant p= em.find(Participant.class, id);
		return p;
	}

	@Override
	public List<Participant> findAllParticipant() {
		return em.createQuery("select p from Participant p", Participant.class).getResultList();

	}

}
