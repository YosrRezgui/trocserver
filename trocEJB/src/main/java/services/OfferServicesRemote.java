package services;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import domain.Offer;
import domain.Swapper;

@Remote
public interface OfferServicesRemote {
	public void addOffer(Offer offer);
	public void removeOffer(Offer offer);
	public void updateOffer(Offer offer);
	public Offer findOfferById(int id);
	public List<Offer> displayAllOffers();
	public List<Offer> displayAllOffersV();
	public List<Offer> displayAllOffersC();
	public List<Offer> displayAllOffersVC();
	public int nbrOfferOfSwapper(int id);
	public List<Offer> SearchByName(String name);
	public List<Offer> SearchBySwapper(String name);
	public List<Offer> SearchByDate(Date dat);

}
