package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import domain.Comment;


/**
 * Session Bean implementation class CommentServiceEJB
 */
@Stateless
@LocalBean
public class CommentServiceEJB implements CommentServiceEJBRemote, CommentServiceEJBLocal {
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public CommentServiceEJB() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addComment(Comment comment) {
		em.persist(comment);
		
	}

	@Override
	public void updateComment(Comment comment) {
		em.merge(comment);
		
	}

	@Override
	public Comment findCommentById(int id) {
		Comment p = em.find(Comment.class, id);
		return p;
	}

	@Override
	public void delete(Comment comment) {
		em.remove(em.merge(comment));
		
	}

	@Override
	public List<Comment> findAllComment() {

		return em.createQuery("select o from Comment o",Comment.class).getResultList();
	}

}
