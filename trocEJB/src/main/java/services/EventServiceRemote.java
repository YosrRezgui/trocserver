package services;

import java.util.List;

import javax.ejb.Remote;

import domain.Event;
import domain.Swapper;

@Remote
public interface EventServiceRemote {
	
	
	public void addEvent(Event event);
	public void updateEvent(Event event);
	public void deleteEvent(Event event);
	public Event findByIdEvent(int id);
	public List<Event> findAllEvent();
	public Event findEventByName(String name);
	public List<Event> findEventsByName(String name);
	public List<Event> findThemsByName(String theme);
	public void sendMail(List<Swapper> s,Event e);

	
	

}
