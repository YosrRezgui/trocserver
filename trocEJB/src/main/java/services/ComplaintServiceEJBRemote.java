package services;

import java.util.List;

import javax.ejb.Remote;

import domain.Complaint;


@Remote
public interface ComplaintServiceEJBRemote {
	/**
	 * This method add a Complaint
	 * @param Complaint
	 */
	public void addComplaint(Complaint complaint);
	/**
	 * this method update a Complaint
	 * @param Complaint
	 */
	public void updateComplaint(Complaint complaint);
	/**
	 * this method find a Complaint
	 * @param id
	 * @return
	 */
	public Complaint findComplaintById(int id);
	/**
	 * this method delete a Complaint
	 * @param Complaint
	 */
	public void delete(Complaint complaint);
	/**
	 * this method find all Complaint
	 * @param Complaint
	 */
	public List<Complaint> findAllComplaint();
}
