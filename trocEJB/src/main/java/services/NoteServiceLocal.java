package services;

import java.util.List;

import javax.ejb.Local;

import domain.Note;

@Local
public interface NoteServiceLocal {
	public void addNote(Note note);
	public void removeNote(Note note);
	public void updateNote(Note note);
	public Note findNoteById(int id);
	public List<Note> displayAllNotes();
	public List<Note> findNoteBySwapper(int id);
}
