package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import domain.Offer;
import domain.Request;

/**
 * Session Bean implementation class RequestService
 */
/**
 * 
 * @author Houssem
 *
 */
@Stateless
@LocalBean
public class RequestService implements RequestServiceRemote, RequestServiceLocal {

	
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public RequestService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addRequest(Request request) {
		em.persist(request);
		
	}

	@Override
	public void updateRequest(Request request) {
		em.merge(request);
		
	}

	@Override
	public void deleteRequest(Request request) {
		em.remove(request);
		
	}

	@Override
	public Request findRequestById(int id) {
		Request p= em.find(Request.class, id);
		return p;
	}

	@Override
	public List<Request> findAllRequest() {
		return em.createQuery("select p from Request p where", Request.class).getResultList();

	}

	@Override
	public List<Request> findAllRequestByOffer(int i) {
		return em.createQuery("select p from Request p where idOffer1=:x  and ( p.offer1.type LIKE 'encours' and p.confirmation=false))", Request.class).setParameter("x", i).getResultList();
	}
	public Request findAllRequestByOffer2(int i) {
		return em.createQuery("select p from Request p where idOffer2=:x ", Request.class).setParameter("x", i).getSingleResult();
	}
	public List<Request> SearchByName(String name) {
		return em.createQuery("select o from Request o where o.offer2.nameOffer LIKE:x ", Request.class).setParameter("x",name+"%").getResultList();
	}

	
	public List<Request> SearchBySwapper(String name) {
		return em.createQuery("select o from  Offer o where o.offer2.swapper.firstName LIKE:x ", Request.class).setParameter("x",name+"%").getResultList();

	}
}
