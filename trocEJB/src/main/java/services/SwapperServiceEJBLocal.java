package services;

import java.util.List;

import javax.ejb.Local;

import domain.Swapper;

@Local
public interface SwapperServiceEJBLocal {
	/**
	 * This method add a Swapper
	 * @param Swapper
	 */
	public void addSwapper(Swapper swapper);
	/**
	 * this method update a Swapper
	 * @param Swapper
	 */
	public void updateSwapper(Swapper swapper);
	/**
	 * this method find a Swapper
	 * @param id
	 * @return
	 */
	public Swapper findSwapperById(int id);
	/**
	 * this method delete a Swapper
	 * @param Swapper
	 */
	public void delete(Swapper swapper);
	/**
	 * this method find all Swapper
	 * @param Swapper
	 */
	public List<Swapper> findAllSwapper();
	public List<Swapper> findALLSwapperActifs();
	public List<Swapper> findALLSwapperPassifs();
	public List<Swapper> findALLSwapperblock();
	public void blockSwapper(Swapper swapper);
	public void unblockSwapper(Swapper swapper);
	public void verificationSwapper(Swapper swapper);
	public int nbrSwapperNotVerified();
	public  List<Swapper> findSwapperByFirstName(String firstName);
	public  List<Swapper> findSwapperBySecondName(String secondName);
	public  List<Swapper> findSwapperByCIN(int cin);
	public  List<Swapper> findSwapperByMail(String mail);
	public  List<Swapper> findSwapperBlockByFirstName(String firstName);
	public  List<Swapper> findSwapperBlockBySecondName(String secondName);
	public  List<Swapper> findSwapperBlockByMail(String mail);
}
