package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import domain.Note;

/**
 * Session Bean implementation class NoteService
 */
@Stateless
@LocalBean
public class NoteService implements NoteServiceRemote, NoteServiceLocal {
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public NoteService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addNote(Note note) {
		em.persist(note);
		
	}

	@Override
	public void removeNote(Note note) {
		em.remove(em.merge(note));
	}

	@Override
	public void updateNote(Note note) {
		em.merge(note);
		
	}

	@Override
	public Note findNoteById(int id) {
		// TODO Auto-generated method stub
		return em.find(Note.class, id);
	}

	@Override
	public List<Note> displayAllNotes() {
		// TODO Auto-generated method stub
		return em.createQuery("select o from Note o", Note.class).getResultList();
	}

	@Override
	public List<Note> findNoteBySwapper(int id) {
		// TODO Auto-generated method stub
		return em.createQuery("select o from Note o where noteReceiver_idSwapperPk=:x", Note.class).setParameter("x",id).getResultList();
	}


}
