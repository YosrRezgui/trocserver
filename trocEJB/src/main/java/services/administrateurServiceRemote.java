package services;

import java.util.List;

import javax.ejb.Remote;

import domain.Administrator;
import domain.Exchange;
import domain.Swapper;

@Remote
public interface administrateurServiceRemote {

	Administrator authentification(String login , String password);
	
	void modifierAdmin(Administrator administrator);
	Administrator afficherAdmin();
	
	public List<Exchange> NbrExchangeValide();
	public List<Exchange> NbrExchangeNonValide();
	public List<Exchange> NbrExchangeSwapper1NonValide();
	
	public List<Swapper> nbrSwapperConfirme();
	public List<Swapper> nbrSwapperNonConfirme();
}
