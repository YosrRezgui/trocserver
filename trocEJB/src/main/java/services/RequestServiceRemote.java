package services;

import java.util.List;

import javax.ejb.Remote;




import domain.Offer;
import domain.Request;

@Remote
public interface RequestServiceRemote {
	
	
	public void addRequest(Request request);
	public void updateRequest(Request request);
	public void deleteRequest(Request request);
	public Request findRequestById(int id);
	public List<Request> findAllRequest();
	public List<Request> findAllRequestByOffer(int i);
	public Request findAllRequestByOffer2(int i);
	public List<Request> SearchByName(String name);
	public List<Request> SearchBySwapper(String name);
	

}
