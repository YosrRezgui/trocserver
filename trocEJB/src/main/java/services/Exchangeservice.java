package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import domain.Exchange;
import domain.Offer;


/**
 * Session Bean implementation class Exchangeservice
 */
@Stateless
@LocalBean
public class Exchangeservice implements ExchangeserviceRemote, ExchangeserviceLocal {

	
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public Exchangeservice() {
        
    }

	@Override
	public void addExchange(Exchange exchange) {
		
		em.persist(exchange);
		
		
	}

	@Override
	public void updateExchange(Exchange exchange) {
		em.merge(exchange);
		
	}

	@Override
	public void deleteExchange(Exchange exchange) {
		em.remove(em.merge(exchange));
		
	}

	@Override
	public Exchange findExchangeById(int id) {
		
		Exchange p= em.find(Exchange.class, id);
		return p;
	}

	@Override
	public List<Exchange> findAllExchange() {
		return em.createQuery("select e from Exchange e where (e.confirmationSwapper1=false or e.confirmationSwapper2=false) and fraud=false", Exchange.class).getResultList();
	}
	public List<Exchange> findAllExchangeV() {
		return em.createQuery("select e from Exchange e where (e.confirmationSwapper1=true and e.confirmationSwapper2=true) and fraud=false", Exchange.class).getResultList();
	}
	public List<Exchange> findAllExchangeF() {
		return em.createQuery("select e from Exchange e where e.fraud=true", Exchange.class).getResultList();
	}

}
