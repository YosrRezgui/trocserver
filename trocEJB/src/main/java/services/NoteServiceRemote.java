package services;

import java.util.List;

import javax.ejb.Remote;

import domain.Note;


@Remote
public interface NoteServiceRemote {
	public void addNote(Note note);
	public void removeNote(Note note);
	public void updateNote(Note note);
	public Note findNoteById(int id);
	public List<Note> displayAllNotes();
	public List<Note> findNoteBySwapper(int id);
}
