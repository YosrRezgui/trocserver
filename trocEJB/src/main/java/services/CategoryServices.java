package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import domain.Category;


/**
 * Session Bean implementation class CategoryServices
 */
@Stateless
@LocalBean
public class CategoryServices implements CategoryServicesRemote, CategoryServicesLocal {
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public CategoryServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addCategory(Category category) {
		em.persist(category);
		
	}

	@Override
	public void removeCategory(Category category) {
		em.remove(em.merge(category));
	}

	@Override
	public void updateCategory(Category category) {
		em.merge(category);
		
	}

	@Override
	public Category findCategoryById(int id) {
		Category c = em.find(Category.class , id);
		return c;
	}

	@Override
	public List<Category> displayAllCategory() {
		return em.createQuery("select c from Category c ",Category.class).getResultList();
	}

}
