package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import domain.Swapper;

/**
 * Session Bean implementation class SwapperServiceEJB
 */
@Stateless
@LocalBean
public class SwapperServiceEJB implements SwapperServiceEJBRemote,
		SwapperServiceEJBLocal {
	@PersistenceContext(unitName = "trocEJB")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public SwapperServiceEJB() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addSwapper(Swapper swapper) {
		em.persist(swapper);

	}

	@Override
	public void updateSwapper(Swapper swapper) {
		em.merge(swapper);

	}

	@Override
	public Swapper findSwapperById(int id) {
		Swapper p = em.find(Swapper.class, id);
		return p;
	}

	@Override
	public void delete(Swapper swapper) {
		em.remove(em.merge(swapper));

	}

	@Override
	public List<Swapper> findAllSwapper() {

		return em.createQuery("Select o From Swapper o", Swapper.class)
				.getResultList();
	}

	@Override
	public List<Swapper> findALLSwapperActifs() {

		return em
				.createQuery(
						"Select o From Swapper o where o.verification =:x  and o.blackList =:y ",
						Swapper.class).setParameter("x", true)
				.setParameter("y", false).getResultList();

	}

	@Override
	public List<Swapper> findALLSwapperPassifs() {
		return em
				.createQuery(
						"Select o From Swapper o where o.verification =:t",
						Swapper.class).setParameter("t", false).getResultList();
	}

	@Override
	public List<Swapper> findALLSwapperblock() {

		return em
				.createQuery("Select o From Swapper o where o.blackList =:a",
						Swapper.class).setParameter("a", true).getResultList();
	}

	@Override
	public void blockSwapper(Swapper swapper) {
		swapper.setBlackList(true);
		em.merge(swapper);

	}

	@Override
	public void unblockSwapper(Swapper swapper) {
		swapper.setBlackList(false);
		em.merge(swapper);
	}

	@Override
	public void verificationSwapper(Swapper swapper) {
		swapper.setVerification(true);
		em.merge(swapper);

	}

	@Override
	public int nbrSwapperNotVerified() {
		Query query = em.createQuery("select COUNT(c) from Swapper c where verification=:x");
		query.setParameter("x", false);
		Long result=(Long) query.getSingleResult();
		int nbSwapper = ((Long) result).intValue();
		return nbSwapper;
		
	}

	@Override
	public  List<Swapper> findSwapperByFirstName(String firstName) {
		return em
				.createQuery(
						"Select o From Swapper o where o.firstName Like :t",
						Swapper.class).setParameter("t",firstName+"%" ).getResultList();
	}

	@Override
	public  List<Swapper> findSwapperBySecondName(String secondName) {
		return em
				.createQuery(
						"Select o From Swapper o where secondName Like :t",
						Swapper.class).setParameter("t",secondName+"%" ).getResultList();
	}

	@Override
	public  List<Swapper> findSwapperByCIN(int cin) {
		return em
				.createQuery(
						"Select o From Swapper o where nic =:t",
						Swapper.class).setParameter("t",cin ).getResultList();
	}

	@Override
	public List<Swapper> findSwapperByMail(String mail) {
		return em
				.createQuery(
						"Select o From Swapper o where mail Like :t",
						Swapper.class).setParameter("t",mail+"%" ).getResultList();
	}

	@Override
	public List<Swapper> findSwapperBlockByFirstName(String firstName) {
		return em
				.createQuery(
						"Select o From Swapper o where o.firstName Like :t and o.blackList =:a ",
						Swapper.class).setParameter("t",firstName+"%" ).setParameter("a", true).getResultList();
	}

	@Override
	public List<Swapper> findSwapperBlockBySecondName(String secondName) {
		return em
				.createQuery(
						"Select o From Swapper o where secondName Like :t and o.blackList =:a",
						Swapper.class).setParameter("t",secondName+"%" ).setParameter("a", true).getResultList();
	}

	@Override
	public List<Swapper> findSwapperBlockByMail(String mail) {
		return em
				.createQuery(
						"Select o From Swapper o where mail Like :t and o.blackList =:a",
						Swapper.class).setParameter("t",mail+"%" ).setParameter("a", true).getResultList();
	}

}
