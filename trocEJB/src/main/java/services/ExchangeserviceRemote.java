package services;

import java.util.List;

import javax.ejb.Remote;


import domain.Exchange;

@Remote
public interface ExchangeserviceRemote {
	
	
	public void addExchange(Exchange exchange);
	public void updateExchange(Exchange exchange);
	public void deleteExchange(Exchange exchange);
	public Exchange findExchangeById(int id);
	public List<Exchange> findAllExchange();
	public List<Exchange> findAllExchangeV();
	public List<Exchange> findAllExchangeF();

}
