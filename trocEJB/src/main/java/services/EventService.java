package services;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import domain.Event;
import domain.Swapper;



/**
 * Session Bean implementation class EventService
 */
/**
 * 
 * @author Houssem
 *
 */
@Stateless
@LocalBean
public class EventService implements EventServiceRemote, EventServiceLocal {
	@Resource(mappedName = "java:jboss/mail/gmail")
	private Session mailSession;
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    public EventService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addEvent(Event event) {
		
		em.persist(event);
	}

	@Override
	public void updateEvent(Event event) {
		
		em.merge(event);
		
		
	}

	@Override
	public void deleteEvent(Event event) {
		
		em.remove(em.merge(event));
		
	}

	@Override
	public Event findByIdEvent(int id) {
		Event e= em.find(Event.class, id);
		return e;
	}

	@Override
	public List<Event> findAllEvent() {
		// TODO Auto-generated method stub
		return em.createQuery("select e from Event e", Event.class).getResultList();
	}

	@Override
	public Event findEventByName(String name) {
		
		return em.createQuery("select e from Event e WHERE e.nameEvent LIKE '"+name+"'",Event.class).getSingleResult();
	}

	@Override
	public List<Event> findEventsByName(String name) {
		return em.createQuery("select e from Event e WHERE e.nameEvent LIKE '%"+name+"%'",Event.class).getResultList();
	}

	@Override
	public List<Event> findThemsByName(String theme) {
		return em.createQuery("select e from Event e WHERE e.placeEvent LIKE '%"+theme+"%'",Event.class).getResultList();

	}
	@Asynchronous
	@Override
	public void sendMail(List<Swapper> s, Event e) {
		
		MimeMessage m = new MimeMessage(mailSession);
		for(Swapper swap: s){
        try {
        	
        	
            m.setRecipients(Message.RecipientType.TO, swap.getMail());
            m.setContent("Welcome MR "+swap.getFirstName()+" "+swap.getSecondName()+" you're invited to our Event "+e.getNameEvent()+" that will take place in "+e.getPlaceEvent(),"text/plain");
            Transport.send(m);//throws exception
        } catch (MessagingException s1) {
            s1.printStackTrace();
        }
		}
        
		
	}

}
