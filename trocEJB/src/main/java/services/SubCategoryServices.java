package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import domain.SubCategory;

/**
 * Session Bean implementation class SubCategoryServices
 */
@Stateless
@LocalBean
public class SubCategoryServices implements SubCategoryServicesRemote, SubCategoryServicesLocal {
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public SubCategoryServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addSubCategory(SubCategory subCategory) {
		em.persist(subCategory);
		
	}

	@Override
	public void removeSubCategory(SubCategory subCategory) {
		em.remove(em.merge(subCategory));
		
	}

	@Override
	public void updateSubCategory(SubCategory subCategory) {
		em.merge(subCategory);
		
	}

	@Override
	public SubCategory findSubCategoryById(int id) {
		SubCategory sc=em.find(SubCategory.class , id);
		return sc;
	}

	@Override
	public List<SubCategory> displayAllSubCategory() {
		return em.createQuery("select sc from SubCategory sc ",SubCategory.class).getResultList();
	}
	public List<SubCategory> findSub(int id) {
		return em.createQuery("select sc from SubCategory sc WHERE sc.category.idCategory = "+id,SubCategory.class).getResultList();
	}

}
