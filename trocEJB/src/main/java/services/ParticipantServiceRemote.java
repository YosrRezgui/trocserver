package services;

import java.util.List;

import javax.ejb.Remote;

import domain.Participant;

@Remote
public interface ParticipantServiceRemote {
	
	
	
	
	public void addParticipant(Participant participant);
	public void updateParticipant(Participant participant);
	public void deleteParticipant(Participant participant);
	public Participant findParticipantById(int id);
	public List<Participant> findAllParticipant();

}
