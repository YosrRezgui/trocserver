package services;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import domain.Offer;
import domain.Request;
import domain.Swapper;

/**
 * Session Bean implementation class OfferServices
 */
@Stateless
@LocalBean
public class OfferServices implements OfferServicesRemote {
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public OfferServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addOffer(Offer offer) {
		em.persist(offer);
		
	}

	@Override
	public void removeOffer(Offer offer) {
		em.remove(em.merge(offer));
		
	}

	@Override
	public void updateOffer(Offer offer) {
		em.merge(offer);
		
	}

	@Override
	public Offer findOfferById(int id) {
		Offer o = em.find(Offer.class, id);
		return o;
	}

	@Override
	public List<Offer> displayAllOffers() {
		return em.createQuery("select o from Offer o", Offer.class).getResultList();
		
	}
	public List<Offer> displayAllOffersV() {
		return em.createQuery("select o from Offer o where o.type LIKE 'valide'", Offer.class).getResultList();
		
	}
	public List<Offer> displayAllOffersVC() {
		return em.createQuery("select o from Offer o where o.type LIKE 'valide' or o.type LIKE 'encours'", Offer.class).getResultList();
		
	}
	public List<Offer> displayAllOffersC() {
		return em.createQuery("select o from Offer o where o.type LIKE 'encours'", Offer.class).getResultList();
		
	}
	

	@Override
	public int nbrOfferOfSwapper(int id) {
		Query query = em.createQuery("select COUNT(c) from Offer c where swapper_idSwapperPk=:x");
		query.setParameter("x", id);
		Long result=(Long) query.getSingleResult();
		int nbOffer = ((Long) result).intValue();
		return nbOffer;
	}
	public List<Offer> SearchByName(String name) {
		return em.createQuery("select o from Offer o where NameOffer LIKE:x ", Offer.class).setParameter("x",name+"%").getResultList();
	}

	@Override
	public List<Offer> SearchBySwapper(String name) {
		return em.createQuery("select o from  Offer o where o.swapper.firstName LIKE:x ", Offer.class).setParameter("x",name+"%").getResultList();
	}

	public List<Offer> SearchByDate(Date dat) {
		return em.createQuery("select o from  Offer o where o.dateOffer =:x ", Offer.class).setParameter("x",dat).getResultList();
	}
}
