package services;

import java.util.List;

import javax.ejb.Remote;

import domain.Category;


@Remote
public interface CategoryServicesRemote {
	public void addCategory(Category category);
	public void removeCategory(Category category);
	public void updateCategory(Category category);
	public Category findCategoryById(int id);
	public List<Category> displayAllCategory();
}
