package services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import domain.Administrator;
import domain.Exchange;
import domain.Swapper;

/**
 * Session Bean implementation class administrateurService
 */
@Stateless
@LocalBean
public class administrateurService implements administrateurServiceRemote {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public administrateurService() {
        // TODO Auto-generated constructor stub
    	//  test
    }

	@Override
	
	public Administrator authentification(String login, String password) {
		Administrator found=null;
		try{
		found=em.createQuery("select A from Administrator A where A.login=:x and A.password=:y", Administrator.class).setParameter("x", login).setParameter("y", password).getSingleResult();
		}
		catch(Exception e){
			Logger.getLogger(getClass().getName()).log(Level.WARNING, "auth failed with email"+login+"and password="+password);
		}
		return found;
	}

	@Override
	public void modifierAdmin(Administrator administrator) {
		// TODO Auto-generated method stub
		em.merge(administrator);
		
	}

	@Override
	public Administrator afficherAdmin() {
		// TODO Auto-generated method stub
		return em.createQuery("select a from Administrator a",Administrator.class).getSingleResult();
	
		
	}


	public List<Exchange> NbrExchangeValide() {
	
		return em.createQuery("select e from Exchange e where e.confirmationSwapper1 = 1 and e.confirmationSwapper2 = 1",Exchange.class).getResultList();
	}

	public List<Exchange> NbrExchangeNonValide() {
		return em.createQuery("select e from Exchange e where e.confirmationSwapper1 = 0 and e.confirmationSwapper2 = 0",Exchange.class).getResultList();
	}

	@Override
	public List<Exchange> NbrExchangeSwapper1NonValide() {
		return em.createQuery("select e from Exchange e where e.confirmationSwapper1 = 0 or e.confirmationSwapper2= 0",Exchange.class).getResultList();
	}
	
	@Override
	public List<Swapper> nbrSwapperConfirme() {

		return em.createQuery("select s from Swapper s where s.blackList = 1 and s.verification = 1",Swapper.class).getResultList();
	}

	@Override
	public List<Swapper> nbrSwapperNonConfirme() {
		return em.createQuery("select s from Swapper s where s.blackList = 0 or s.verification = 0",Swapper.class).getResultList();
	}

	

}
