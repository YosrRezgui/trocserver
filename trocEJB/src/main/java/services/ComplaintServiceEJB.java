package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import domain.Complaint;

/**
 * Session Bean implementation class ComplaintServiceEJB
 */
@Stateless
@LocalBean
public class ComplaintServiceEJB implements ComplaintServiceEJBRemote, ComplaintServiceEJBLocal {
	@PersistenceContext(unitName="trocEJB")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ComplaintServiceEJB() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addComplaint(Complaint complaint) {
		em.persist(complaint);
		
	}

	@Override
	public void updateComplaint(Complaint complaint) {
		em.merge(complaint);
		
	}

	@Override
	public Complaint findComplaintById(int id) {
		Complaint p = em.find(Complaint.class, id);
		return p;
	}

	@Override
	public void delete(Complaint complaint) {
		em.remove(em.merge(complaint));
		
	}
	public List<Complaint> findAllOffer() {
		return em.createQuery("select o from Complaint o",Complaint.class).getResultList();
	}

	@Override
	public List<Complaint> findAllComplaint() {
		
			return em.createQuery("select o from Complaint o",Complaint.class).getResultList();
	
	}

}
