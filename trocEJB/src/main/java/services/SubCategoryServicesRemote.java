package services;

import java.util.List;

import javax.ejb.Remote;


import domain.SubCategory;

@Remote
public interface SubCategoryServicesRemote {
	public void addSubCategory(SubCategory subCategory);
	public void removeSubCategory(SubCategory subCategory);
	public void updateSubCategory(SubCategory subCategory);
	public SubCategory findSubCategoryById(int id);
	public List<SubCategory> displayAllSubCategory();
	public List<SubCategory> findSub(int id);

}
