package services;

import javax.ejb.Remote;

import domain.Game;

@Remote
public interface GameServiceRemote {
	 public void addGame(Game game);

}
