package services;

import java.util.List;

import javax.ejb.Local;

import domain.Comment;
import domain.Complaint;


@Local
public interface CommentServiceEJBLocal {
	/**
	 * This method add a Comment
	 * @param Comment
	 */
	public void addComment(Comment comment);
	/**
	 * this method update a Comment
	 * @param Comment
	 */
	public void updateComment(Comment comment);
	/**
	 * this method find a Comment
	 * @param id
	 * @return
	 */
	public Comment findCommentById(int id);
	/**
	 * this method delete a Comment
	 * @param Comment
	 */
	public void delete(Comment comment);
	/**
	 * this method find all Comment
	 * @param Comment
	 */
	public List<Comment> findAllComment();

}
